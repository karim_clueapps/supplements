package com.clueapps.supplements.API;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.clueapps.supplements.Holders.UserDataErrorViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Cart.ShippingActivity;
import com.clueapps.supplements.activities.HomeActivity;
import com.clueapps.supplements.activities.User.BlockActivity;
import com.clueapps.supplements.activities.User.EnterCodeActivity;
import com.clueapps.supplements.activities.User.ForceUpdateActivity;
import com.clueapps.supplements.activities.User.LoginActivity;
import com.clueapps.supplements.activities.User.ResetPasswordActivity;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.RefreshToken;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.helpers.Validations;
import com.clueapps.supplements.models.Data.Store;
import com.clueapps.supplements.models.Data.User;
import com.clueapps.supplements.models.requests.ChangePasswordRequest;
import com.clueapps.supplements.models.requests.ContactUsRequest;
import com.clueapps.supplements.models.requests.ForgotPasswordRequest;
import com.clueapps.supplements.models.responses.ErrorResponse;
import com.clueapps.supplements.models.responses.MessageResponse;
import com.clueapps.supplements.models.responses.DataUserResponse;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public class UserAPI {

    public static void enterCodeApi(final Activity activity ,final int code ,final String email) {

        Dialogs.customProgDialog(activity);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        UserAPI.ForgotPasswordApi apiInterface = retrofit.create(UserAPI.ForgotPasswordApi.class);

        ForgotPasswordRequest request = new ForgotPasswordRequest();
        request.setCode(code);
        request.setEmail(email);

        Call<MessageResponse> call = apiInterface.enterCode(Utility.version , new SharedPref(activity).getLang(),request);
        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {

                Dialogs.dismissDialog();

                switch (response.code()) {
                    case 200:
                        if (response.body() != null) {
                            Bundle bundle = new Bundle();
                            bundle.putString("code" , code+"");
                            IntentClass.goToActivityAndClear(activity,ResetPasswordActivity.class,bundle);

                        }
                        break;
                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body  = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 404:
                                    if (body.getErrorBody() != null) {
                                        Dialogs.showToast(body.getErrorBody().getMessage(), activity);
                                    }
                                    break;
                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong),activity);

                            }

                        } catch (Exception e) {
                         //   Log.d( "error_retrofit" , e.getMessage());
                        }



                }

            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Dialogs.showDialogOrToast(activity,activity.getResources().getString(R.string.error)
                        ,activity.getResources().getString(R.string.no_network)
                        ,activity.getResources().getString(R.string.ok_str)
                        ,1);

                Dialogs.dismissDialog();
            }
        });

    }

    public static void loginApi(final Activity activity , final User user, final boolean fromCart , final Store store) {
        Dialogs.customProgDialog(activity);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        UserAPI.LoginApi apiInterface = retrofit.create(UserAPI.LoginApi.class);
        final User request = new User();
        request.setMobile(user.getMobile());
        request.setPassword(user.getPassword());
        request.setRegisterId(FirebaseInstanceId.getInstance().getToken());
        request.setDeviceId(Utility.gettingDeviceId(activity));
        Call<DataUserResponse> call = apiInterface.login(Utility.version , new SharedPref(activity).getLang(),request);
        call.enqueue(new Callback<DataUserResponse>() {
            @Override
            public void onResponse(Call<DataUserResponse> call, Response<DataUserResponse> response) {

                Dialogs.dismissDialog();
                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class,null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class,null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken( response.body().getTokenResponse().getToken());
                                    loginApi(activity,user,fromCart,store);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if(activity!=null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;


                    case 200:
                        if (response.body() != null) {
                            String token = response.body().getUserResponse().getToken();
                            User user = response.body().getUserResponse().getUser();
                            SharedPref sharedPref = new SharedPref(activity);
                            sharedPref.setUserEmail(user.getEmail());
                            sharedPref.setToken(token);
                            sharedPref.setUserId(user.getId());
                            sharedPref.setFName(user.getfName());
                            sharedPref.setLName(user.getlName());
                            sharedPref.setUserMob(user.getMobile());
                            sharedPref.setBirth(user.getBirth());
                            sharedPref.setGender(user.getGender());
                            sharedPref.setLang(user.getLanguage());

                            if(user.getIsBlock()==1)
                            {
                                IntentClass.goToActivityAndClear(activity,BlockActivity.class,null);
                            }
                            else
                            {
                                if ( fromCart ) {
                                    Bundle bundle = new Bundle();
                                    bundle.putParcelable("store",store);
                                    IntentClass.goToActivityAndClear(activity, ShippingActivity.class, bundle);
                                }
                                else
                                    IntentClass.goToActivityAndClear(activity,HomeActivity.class,null);

                            }

                        }
                        break;

                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body  = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 401:
                                    Dialogs.showToast(activity.getString(R.string.wrong_login) , activity);
                                    break;

                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong),activity);
                            }

                        } catch (Exception e) {
                          //  Log.d( "error_retrofit" , e.getMessage());
                        }

                        break;


                }

            }

            @Override
            public void onFailure(Call<DataUserResponse> call, Throwable t) {
                Dialogs.showDialogOrToast(activity,activity.getResources().getString(R.string.error)
                        ,activity.getResources().getString(R.string.no_network)
                        ,activity.getResources().getString(R.string.ok_str)
                        ,1);

                Dialogs.dismissDialog();
            }
        });

    }

    public static void resetPasswordApi(final Activity activity , final int code , final  String password) {

        Dialogs.customProgDialog(activity);

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        UserAPI.ResetPasswordApi apiInterface = retrofit.create(UserAPI.ResetPasswordApi.class);

        ForgotPasswordRequest request = new ForgotPasswordRequest();
        request.setCode(code);
        request.setPassword(password);

        Call<MessageResponse> call = apiInterface.resetPassword(Utility.version , new SharedPref(activity).getLang(),request);
        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {

                Dialogs.dismissDialog();
                switch (response.code()) {
                    case 200:
                        if (response.body() != null) {
                           IntentClass.goToActivityAndClear(activity,LoginActivity.class,null);

                        }
                        break;

                    default:
                        Dialogs.showToast(activity.getString(R.string.something_wrong), activity);
                        break;


                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Dialogs.showDialogOrToast(activity,activity.getResources().getString(R.string.error)
                        ,activity.getResources().getString(R.string.no_network)
                        ,activity.getResources().getString(R.string.ok_str)
                        ,1);

                Dialogs.dismissDialog();
            }
        });

    }

    public static void contactUsApi(final Activity activity ,final String email , final String msg){

        Dialogs.customProgDialog(activity);

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        final ContactUsRequest request = new ContactUsRequest();
        request.setEmail(email);
        request.setMessage(msg);

        UserAPI.ContactUsApi apiInterface = retrofit.create(UserAPI.ContactUsApi.class);

        Call<MessageResponse> call = apiInterface.sendEmail(Utility.version,request);

        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                Dialogs.dismissDialog();
                switch (response.code()) {
                    case 201:
                        if (response.body() != null) {
                            activity.onBackPressed();
                        }
                        break;

                    default:
                        Dialogs.showToast(activity.getResources().getString(R.string.something_wrong),activity);
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Dialogs.dismissDialog();
                Dialogs.showToast(activity.getResources().getString(R.string.no_internet),activity);

            }
        });
    }

    public static void forgotPasswordApi(final Activity activity ,final String email){

        Dialogs.customProgDialog(activity);

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        final ForgotPasswordRequest request = new ForgotPasswordRequest();
        request.setEmail(email);
        UserAPI.ForgotPasswordApi apiInterface = retrofit.create(UserAPI.ForgotPasswordApi.class);
        Call<MessageResponse> call = apiInterface.forgotPassword(Utility.version,request);

        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                Dialogs.dismissDialog();
                switch (response.code()) {
                    case 200:
                        if (response.body() != null) {
                            Bundle bundle = new Bundle();
                            bundle.putString("email" , email);
                            IntentClass.goToActivity(activity,EnterCodeActivity.class,bundle);

                            // Dialogs.showForgetDialog(activity);
                        }
                        break;
                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body  = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 404:
                                    if (body.getErrorBody() != null) {
                                        Dialogs.showToast(body.getErrorBody().getMessage(), activity);
                                    }
                                    break;
                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong),activity);

                            }

                        } catch (Exception e) {
                           // Log.d( "error_retrofit" , e.getMessage());
                        }



                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Dialogs.dismissDialog();
                Dialogs.showToast(activity.getResources().getString(R.string.no_internet),activity);

            }
        });
    }

    public static void signUpApi(final Activity activity , final User signUpRequest , final boolean fromCart , final Store store,final UserDataErrorViewHolder holder){
        Dialogs.customProgDialog(activity);

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        UserAPI.RegisterApi apiInterface = retrofit.create(UserAPI.RegisterApi.class);
        signUpRequest.setDeviceId(Utility.gettingDeviceId(activity));
        signUpRequest.setRegisterId(FirebaseInstanceId.getInstance().getToken());
        Call<DataUserResponse> call = apiInterface.register(Utility.version , new SharedPref(activity).getLang(),signUpRequest);

        call.enqueue(new Callback<DataUserResponse>() {
            @Override
            public void onResponse(Call<DataUserResponse> call, Response<DataUserResponse> response) {
                Dialogs.dismissDialog();

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class,null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class,null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken( response.body().getTokenResponse().getToken());
                                    signUpApi(activity,signUpRequest,fromCart,store,holder);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if(activity!=null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;


                    case 201:
                        if (response.body() != null) {
                            String token = response.body().getUserResponse().getToken();
                            User user = response.body().getUserResponse().getUser();
                            SharedPref sharedPref = new SharedPref(activity);
                            sharedPref.setUserEmail(user.getEmail());
                            sharedPref.setToken(token);
                            sharedPref.setUserId(user.getId());
                            sharedPref.setFName(user.getfName());
                            sharedPref.setLName(user.getlName());
                            sharedPref.setUserMob(user.getMobile());
                            sharedPref.setBirth(user.getBirth());
                            sharedPref.setGender(user.getGender());
                            sharedPref.setLang(user.getLanguage());

                            if ( fromCart ) {
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("store",store);
                                IntentClass.goToActivityAndClear(activity, ShippingActivity.class, bundle);
                            }
                            else
                                IntentClass.goToActivityAndClear(activity,HomeActivity.class,null);
                             
                        }
                        break;
                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body  = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 400:
                                    Dialogs.showToast(body.getErrorBody().getMessage() , activity);

                                    if ( body.getErrorBody().getErrorMap().get("email_address") != null ) {
                                        holder.errorEmailTxv.setText(body.getErrorBody().getErrorMap().get("email_address"));
                                        Validations.animateView(holder.errorEmailTxv);
                                    }
                                    if ( body.getErrorBody().getErrorMap().get("phone") != null ) {
                                        holder.errorMobTxv.setText(body.getErrorBody().getErrorMap().get("phone"));
                                        Validations.animateView(holder.errorMobTxv);
                                    }


                                    break;

                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong),activity);
                            }

                        } catch (Exception e) {

                        }


                }
            }

            @Override
            public void onFailure(Call<DataUserResponse> call, Throwable t) {
                Dialogs.dismissDialog();

                Dialogs.showDialogOrToast(activity,activity.getResources().getString(R.string.error)
                        ,activity.getResources().getString(R.string.no_network)
                        ,activity.getResources().getString(R.string.ok_str)
                        ,1);
            }
        });
    }

    public static void editAccountApi(final Activity activity ,final User updateRequest){
        Dialogs.customProgDialog(activity);

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        SharedPref sharedPref = new SharedPref(activity);
        String token = sharedPref.getToken();
        UserAPI.UpdateUserApi apiInterface = retrofit.create(UserAPI.UpdateUserApi.class);
        updateRequest.setDeviceId(Utility.gettingDeviceId(activity));
        updateRequest.setRegisterId(FirebaseInstanceId.getInstance().getToken());
        Call<MessageResponse> call = apiInterface.user_update(token, Utility.version,sharedPref.getLang(),updateRequest);

        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                Dialogs.dismissDialog();

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class,null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class,null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken( response.body().getTokenResponse().getToken());
                                    editAccountApi(activity,updateRequest);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if(activity!=null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;

                    case 200:
                        if (response.body() != null) {
                            SharedPref sharedPref = new SharedPref(activity);
                            sharedPref.setUserEmail(updateRequest.getEmail());
                            sharedPref.setUserId(updateRequest.getId());
                            sharedPref.setFName(updateRequest.getfName());
                            sharedPref.setLName(updateRequest.getlName());
                            sharedPref.setBirth(updateRequest.getBirth());
                            sharedPref.setGender(updateRequest.getMobile());
                            Dialogs.showToast(activity.getString(R.string.edit_success),activity);
                        }
                        break;
                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body  = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 400:
                                    Dialogs.showToast(body.getErrorBody().getMessage() , activity);
                                    break;

                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong),activity);
                            }

                        } catch (Exception e) {
                            Log.d( "error_retrofit" , "");
                        }


                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Dialogs.dismissDialog();

                Dialogs.showDialogOrToast(activity,activity.getResources().getString(R.string.error)
                        ,activity.getResources().getString(R.string.no_network)
                        ,activity.getResources().getString(R.string.ok_str)
                        ,1);
            }
        });
    }


    public static void changePassword(final Activity activity,final ChangePasswordRequest request){

        Dialogs.customProgDialog(activity);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        SharedPref sharedPref = new SharedPref(activity);
        String token = sharedPref.getToken();

        UserAPI.ChangePasswordApi apiInterface = retrofit.create(UserAPI.ChangePasswordApi.class);
        Call<MessageResponse> call = apiInterface.changePassword(token, Utility.version ,sharedPref.getLang(),request);

        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                Dialogs.dismissDialog();

                switch (response.code()) {

                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class,null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class,null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken( response.body().getTokenResponse().getToken());
                                    changePassword(activity,request);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if(activity!=null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;

                    case 200:
                        Dialogs.showToast( activity.getResources().getString(R.string.password_changed),activity);
                        activity.onBackPressed();
                        break;
                    case 421:
                        Dialogs.showToast( activity.getResources().getString(R.string.something_wrong),activity);
                        break;
                    default:
                        Log.d("error","error");
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {

                Dialogs.showDialogOrToast(activity,activity.getResources().getString(R.string.error)
                        ,activity.getResources().getString(R.string.no_network)
                        ,activity.getResources().getString(R.string.ok_str)
                        ,1);
            }
        });

    }
 
    public static void logout (final Activity activity){
        Dialogs.customProgDialog(activity);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();
        String token = new SharedPref(activity).getToken();

        UserAPI.LogoutApi apiInterface = retrofit.create(UserAPI.LogoutApi.class);
        Call<MessageResponse> call = apiInterface.logout(Utility.version , new SharedPref(activity).getLang(),token);

        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                Dialogs.dismissDialog();
                switch (response.code()) {

                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class,null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class,null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken( response.body().getTokenResponse().getToken());
                                    logout(activity);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if(activity!=null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;

                    case 200:
                        if (response.body() != null) {
                            new SharedPref(activity).clearUserData();
                            Intent broadcastIntent = new Intent();
                            broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                            activity.sendBroadcast(broadcastIntent);
                            IntentClass.goToActivityAndClear(activity, LoginActivity.class,null);
                            activity.finish();
                        }
                        break;

                    default:
                        Log.d("error","error");
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                if(activity!=null)
                    Toast.makeText(activity, activity.getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();

            }
        });

    }

    public interface ContactUsApi{
        @POST("contact")
        Call<MessageResponse> sendEmail(@Header("Version") int version,
                                             @Body ContactUsRequest request);


    }

    public interface ForgotPasswordApi{
        @POST("forget-password")
        Call<MessageResponse> forgotPassword(@Header("Version") int version,
                                             @Body ForgotPasswordRequest request);

        @POST("confirm-code")
        Call<MessageResponse> enterCode(@Header("Version") int version,
                                         @Header("locale") String locale ,
                                         @Body ForgotPasswordRequest request);

    }

    public interface LoginApi {
        @POST("sign-in")
        Call<DataUserResponse> login( @Header("Version") int version,
                                      @Header("locale") String locale ,
                                      @Body User request);
    }


    public interface ResetPasswordApi {

        @POST("reset-password")
        Call<MessageResponse> resetPassword( @Header("Version") int version,
                                        @Header("locale") String locale ,
                                         @Body ForgotPasswordRequest request);
    }


    public interface ChangePasswordApi {
        @PUT("users")
        Call<MessageResponse> changePassword(@Header("Authorization") String token,
                                             @Header("Version") int version,
                                             @Header("locale") String locale ,
                                             @Body ChangePasswordRequest request);
    }

    public interface LogoutApi {

        @GET("logout")
        Call<MessageResponse> logout( @Header("Version") int version,
                                        @Header("locale") String locale ,
                                      @Header("Authorization") String token);
    }

    public interface UpdateUserApi {
        @PUT("users")
        Call<MessageResponse> user_update(@Header("Authorization") String token,
                                           @Header("Version") int version,
                                           @Header("locale") String locale ,
                                           @Body User user);
    }


    public interface RegisterApi {
        @POST("sign-up")
        Call<DataUserResponse> register( @Header("Version") int version,
                                         @Header("locale") String locale ,
                                         @Body User user);
    }


}