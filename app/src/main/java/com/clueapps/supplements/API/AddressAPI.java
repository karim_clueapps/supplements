package com.clueapps.supplements.API;

import android.app.Activity;
import android.graphics.Paint;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.clueapps.supplements.Holders.OrderDetailsViewHolder;
import com.clueapps.supplements.Holders.ProductDetailsViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.HomeActivity;
import com.clueapps.supplements.activities.User.BlockActivity;
import com.clueapps.supplements.activities.User.ForceUpdateActivity;
import com.clueapps.supplements.adapters.AddressesAdapter;
import com.clueapps.supplements.adapters.AdsAdapter;
import com.clueapps.supplements.adapters.CategoryAdapter;
import com.clueapps.supplements.adapters.OrderItemsAdapter;
import com.clueapps.supplements.adapters.OrdersAdapter;
import com.clueapps.supplements.adapters.ProductsAdapter;
import com.clueapps.supplements.adapters.SpinnersAdapter;
import com.clueapps.supplements.adapters.StoreAdapter;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.RefreshToken;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Ad;
import com.clueapps.supplements.models.Data.Address;
import com.clueapps.supplements.models.Data.Category;
import com.clueapps.supplements.models.Data.Order;
import com.clueapps.supplements.models.Data.PaginatorData;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.models.Data.SearchProduct;
import com.clueapps.supplements.models.Data.Store;
import com.clueapps.supplements.models.Data.User;
import com.clueapps.supplements.models.responses.DataAddressesResponse;
import com.clueapps.supplements.models.responses.DataCategoriesResponse;
import com.clueapps.supplements.models.responses.DataCitiesResponse;
import com.clueapps.supplements.models.responses.DataCountriesResponse;
import com.clueapps.supplements.models.responses.DataOrderResponse;
import com.clueapps.supplements.models.responses.DataOrdersResponse;
import com.clueapps.supplements.models.responses.DataProductResponse;
import com.clueapps.supplements.models.responses.DataProductsResponse;
import com.clueapps.supplements.models.responses.DataStoresResponse;
import com.clueapps.supplements.models.responses.ErrorResponse;
import com.clueapps.supplements.models.responses.MessageResponse;
import com.clueapps.supplements.realm.RealmController;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class AddressAPI {
 
    public static void getAddresses(final Activity activity , final AddressesAdapter addressesAdapter , final ArrayList<Address> addresses , final PaginatorData paginator ){
        paginator.loading = true ;

        if ( paginator.page == 1 )
            paginator.refreshLayout.setRefreshing(true);
        else
            paginator.progressMoreDataRel.setVisibility(View.VISIBLE);

        paginator.noResultLayout.setVisibility(View.GONE);
        paginator.recyclerView.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        String token = new SharedPref(activity).getToken();

        AddressesApi apiInterface = retrofit.create(AddressesApi.class);

        Call<DataAddressesResponse> call = apiInterface.getAddresses(token,Utility.version , new SharedPref(activity).getLang(),paginator.page);


        call.enqueue(new Callback<DataAddressesResponse>() {
            @Override
            public void onResponse(Call<DataAddressesResponse> call, Response<DataAddressesResponse> response) {

                paginator.refreshLayout.setRefreshing(false);
                paginator.progressMoreDataRel.setVisibility(View.GONE);

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class,null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class,null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken( response.body().getTokenResponse().getToken());
                                    getAddresses(activity,addressesAdapter,addresses,paginator);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if(activity!=null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;
                    case 401:
                        Dialogs.showLoginDialog(activity);
                        break;
                    case 200:
                        if (response.body().getAddressesResponse() != null) {
                            addressesAdapter.notifyDataSetChanged();
                            addresses.addAll(response.body().getAddressesResponse().getAddressArrayList());
                            paginator.loading = false;
                            paginator.page =  response.body().getPaginator().getPage() +1 ;
                            if ( response.body().getPaginator().getNext() == null || response.body().getAddressesResponse().getAddressArrayList().size()<Utility.take)
                                paginator.empty = true;

                        }
                        if ( addresses.size() == 0 ) {
                            paginator.noResultLayout.setVisibility(View.VISIBLE);
                            paginator.recyclerView.setVisibility(View.GONE);

                        }
                        break;

                    default:
                        paginator.loading=false;
                        paginator.empty=true;

                        if ( addresses.size() == 0 ) {
                            paginator.noResultLayout.setVisibility(View.VISIBLE);
                            paginator.recyclerView.setVisibility(View.GONE);

                        }
                }
            }

            @Override
            public void onFailure(Call<DataAddressesResponse> call, Throwable t) {
                if ( addresses.size() == 0 ) {
                    paginator.noResultLayout.setVisibility(View.VISIBLE);
                    paginator.recyclerView.setVisibility(View.GONE);
                }

                paginator.progressMoreDataRel.setVisibility(View.GONE);
                paginator.refreshLayout.setRefreshing(false);

                Dialogs.showToast(activity.getResources().getString(R.string.no_network),activity);

            }
        });

    }

    public static void addAddress(final Activity activity ,final Address address){

        Dialogs.customProgDialog(activity);

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        AddressesApi apiInterface = retrofit.create(AddressesApi.class);
        SharedPref sharedPref = new SharedPref(activity);
        String token = sharedPref.getToken();
        Call<MessageResponse> call = apiInterface.addAddress(token,Utility.version , sharedPref.getLang(),address);

        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                Dialogs.dismissDialog();

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class,null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class,null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken( response.body().getTokenResponse().getToken());
                                    addAddress(activity,address);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if(activity!=null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;
                    case 401:
                        Dialogs.showLoginDialog(activity);
                        break;
                    case 201:
                        if (response.body() != null) {
                            activity.finish();
                        }
                        break;
                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body  = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 400:
                                    Dialogs.showToast(body.getErrorBody().getMessage() , activity);
                                    break;

                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong),activity);
                            }

                        } catch (Exception e) {
                            Log.d( "error_retrofit" , "");
                        }


                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Dialogs.dismissDialog();

                Dialogs.showDialogOrToast(activity,activity.getResources().getString(R.string.error)
                        ,activity.getResources().getString(R.string.no_network)
                        ,activity.getResources().getString(R.string.ok_str)
                        ,2);
            }
        });
    }

    public static void editAddress(final Activity activity ,final Address address){

        Dialogs.customProgDialog(activity);

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        AddressesApi apiInterface = retrofit.create(AddressesApi.class);
        SharedPref sharedPref = new SharedPref(activity);
        String token = sharedPref.getToken();
        Call<MessageResponse> call = apiInterface.editAddress(token,Utility.version , sharedPref.getLang(),address.getId(),address);

        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                Dialogs.dismissDialog();

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class,null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class,null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken( response.body().getTokenResponse().getToken());
                                    editAddress(activity,address);
                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if(activity!=null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;
                    case 401:
                        Dialogs.showLoginDialog(activity);
                        break;
                    case 201:
                        if (response.body() != null) {
                            activity.finish();
                        }
                        break;
                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body  = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 400:
                                    Dialogs.showToast(body.getErrorBody().getMessage() , activity);
                                    break;

                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong),activity);
                            }

                        } catch (Exception e) {
                            Log.d( "error_retrofit" , "");
                        }


                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Dialogs.dismissDialog();

                Dialogs.showDialogOrToast(activity,activity.getResources().getString(R.string.error)
                        ,activity.getResources().getString(R.string.no_network)
                        ,activity.getResources().getString(R.string.ok_str)
                        ,1);
            }
        });
    }

    public static void removeAddress(final Activity activity ,final String addressId){

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        SharedPref sharedPref = new SharedPref(activity);
        String token = sharedPref.getToken();

        AddressesApi apiInterface = retrofit.create(AddressesApi.class);
        Address address = new Address();
        address.setAddressId(addressId);

        Call<MessageResponse> call = apiInterface.removeAddress(token,Utility.version , sharedPref.getLang(),address);

        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class,null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class,null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken( response.body().getTokenResponse().getToken());
                                    removeAddress(activity,addressId);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if(activity!=null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;
                    case 401:
                        Dialogs.showLoginDialog(activity);
                        break;
                    case 201:
                        if (response.body() != null) {
                            activity.finish();
                        }
                        break;


                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body  = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 400:
                                    Dialogs.showToast(body.getErrorBody().getMessage() , activity);
                                    break;


                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong),activity);
                            }

                        } catch (Exception e) {
                            //   Log.d( "error_retrofit" , e.getMessage());
                        }


                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Dialogs.showDialogOrToast(activity,activity.getResources().getString(R.string.error)
                        ,activity.getResources().getString(R.string.no_network)
                        ,activity.getResources().getString(R.string.ok_str)
                        ,1);
            }
        });
    }

    public interface AddressesApi {

        @GET("users/addresses")
        Call<DataAddressesResponse> getAddresses(@Header("Authorization") String token,
                                                 @Header("Version") int version,
                                                 @Header("locale") String locale,
                                                 @Query("page") int page);

        @POST("users/addresses")
        Call<MessageResponse> addAddress(@Header("Authorization") String token,
                                         @Header("Version") int version,
                                         @Header("locale") String locale,
                                         @Body Address address);

        @PUT("users/addresses/{address_id}")
        Call<MessageResponse> editAddress(@Header("Authorization") String token,
                                          @Header("Version") int version,
                                          @Header("locale") String locale,
                                          @Path("address_id") String addressId,
                                          @Body Address address);

        @HTTP(method = "DELETE", path = "users/addresses", hasBody = true)
        Call<MessageResponse> removeAddress(@Header("Authorization") String token,
                                            @Header("Version") int version,
                                            @Header("locale") String locale,
                                            @Body Address address);


    }

}