package com.clueapps.supplements.API;

import android.app.Activity;
import android.widget.Toast;

import com.clueapps.supplements.activities.User.BlockActivity;
import com.clueapps.supplements.activities.User.ForceUpdateActivity;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.RefreshToken;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.responses.DataCitiesResponse;
import com.clueapps.supplements.models.responses.DataCountriesResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public class API {

    public static int take = 10;
    public static String BASE_URL = "http://supplement-app.net/supplements/public/api/";
//"http://supplement-app.net/supplements/public/api/";

    public static int version  = 1 ;

    // when app version is old
    public final static int FORCE_UPDATE = 451;

    // when user blocked
    public final static int BLOCK = 456;

    // when token expired
    public final static int REFRESH_TOKEN = 452;

    public static void userResponses(final Activity activity ,int code ,final Void func) {


        switch (code) {

            case API.FORCE_UPDATE:

                IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                break;
            case API.BLOCK:
                IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                break;
            case API.REFRESH_TOKEN:
                RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                    @Override
                    public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                        if (response.code() == 200 && response.body() != null) {
                            SharedPref sharedPref = new SharedPref(activity);
                            sharedPref.setToken(response.body().getTokenResponse().getToken());
                            func.notify();


                        }
                    }

                    @Override
                    public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                        if (activity != null)
                            Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                    }
                });
                break;

        }
    }


    public static Call<DataCountriesResponse> getCountries(Activity activity) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final API.GetCountriesApi retroInterface = retrofit.create(API.GetCountriesApi.class);
        Call<DataCountriesResponse> call = retroInterface.getCountries(Utility.version, new SharedPref(activity).getLang());

        return call;
    }
     public static Call<DataCitiesResponse> getCities(Activity activity,String countryCode) {

         Retrofit retrofit = new Retrofit.Builder()
                 .baseUrl(API.BASE_URL)
                 .addConverterFactory(GsonConverterFactory.create())
                 .build();

         final API.GetCitiesApi retroInterface = retrofit.create(API.GetCitiesApi.class);
         Call<DataCitiesResponse> call = retroInterface.getCities(Utility.version , new SharedPref(activity).getLang(),countryCode);

        return call;
    }


    public interface GetCountriesApi {

        @GET("countries")
        Call<DataCountriesResponse> getCountries(   @Header("Version") int version,
                                                    @Header("locale") String locale);
    }

    public interface GetCitiesApi {

        @GET("cities/{code}")
        Call<DataCitiesResponse> getCities(@Header("Version") int version,
                                           @Header("locale") String locale ,
                                           @Path("code") String countryCode );

    }

}