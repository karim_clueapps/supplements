package com.clueapps.supplements.API;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.clueapps.supplements.Holders.OrderDetailsViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Cart.ShippingActivity;
import com.clueapps.supplements.activities.HomeActivity;
import com.clueapps.supplements.activities.User.BlockActivity;
import com.clueapps.supplements.activities.User.ForceUpdateActivity;
import com.clueapps.supplements.adapters.OrderItemsAdapter;
import com.clueapps.supplements.adapters.OrdersAdapter;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.RefreshToken;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Address;
import com.clueapps.supplements.models.Data.Order;
import com.clueapps.supplements.models.Data.PaginatorData;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.models.Data.PromocodeResponse;
import com.clueapps.supplements.models.Data.User;
import com.clueapps.supplements.models.responses.DataOrderResponse;
import com.clueapps.supplements.models.responses.DataOrdersResponse;
import com.clueapps.supplements.models.responses.ErrorResponse;
import com.clueapps.supplements.models.responses.MessageResponse;
import com.clueapps.supplements.realm.RealmController;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class OrderAPI {

    public interface ApiResponseListener {
        void onSuccess(String response);
    }

    public static void validatePromoCode(final Activity activity, final String code, final ApiResponseListener listener) {

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        OrdersApi apiInterface = retrofit.create(OrdersApi.class);
        SharedPref sharedPref = new SharedPref(activity);
        String token = sharedPref.getToken();


        Call<PromocodeResponse> call = apiInterface.validatePromocode(token, Utility.version, sharedPref.getLang(), code);

        call.enqueue(new Callback<PromocodeResponse>() {
            @Override
            public void onResponse(Call<PromocodeResponse> call, Response<PromocodeResponse> response) {
                Dialogs.dismissDialog();

                switch (response.code()) {
                    case 401:
                        Dialogs.showLoginDialog(activity);
                        break;
                    case 200:
                        if (response.body() != null) {
                            listener.onSuccess(new Gson().toJson(response.body()));
                        }
                        break;
                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 400:
                                    Dialogs.showToast(body.getErrorBody().getMessage(), activity);
                                    break;

                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong), activity);
                            }

                        } catch (Exception e) {
                        }


                }
            }

            @Override
            public void onFailure(Call<PromocodeResponse> call, Throwable t) {
                Dialogs.dismissDialog();

                Dialogs.showDialogOrToast(activity, activity.getResources().getString(R.string.error)
                        , activity.getResources().getString(R.string.no_network)
                        , activity.getResources().getString(R.string.ok_str)
                        , 2);
            }
        });
    }

    public static void getOrders(final Activity activity, final OrdersAdapter ordersAdapter, final ArrayList<Order> orders, final PaginatorData paginator) {
        paginator.loading = true;


        if (paginator.page == 1)
            paginator.refreshLayout.setRefreshing(true);
        else
            paginator.progressMoreDataRel.setVisibility(View.VISIBLE);

        paginator.noResultLayout.setVisibility(View.GONE);
        paginator.recyclerView.setVisibility(View.VISIBLE);


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        String token = new SharedPref(activity).getToken();

        OrdersApi apiInterface = retrofit.create(OrdersApi.class);

        Call<DataOrdersResponse> call = apiInterface.getOrders(token, Utility.version, new SharedPref(activity).getLang(), paginator.page);


        call.enqueue(new Callback<DataOrdersResponse>() {
            @Override
            public void onResponse(Call<DataOrdersResponse> call, Response<DataOrdersResponse> response) {

                paginator.refreshLayout.setRefreshing(false);
                paginator.progressMoreDataRel.setVisibility(View.GONE);

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    getOrders(activity, ordersAdapter, orders, paginator);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;

                    case 200:
                        if (response.body().getOrdersResponse() != null) {
                            ordersAdapter.notifyDataSetChanged();
                            orders.addAll(response.body().getOrdersResponse().getOrderArrayList());
                            paginator.loading = false;
                            paginator.page = response.body().getPaginator().getPage() + 1;
                            if (response.body().getPaginator().getNext() == null || response.body().getOrdersResponse().getOrderArrayList().size() < Utility.take)
                                paginator.empty = true;

                        }
                        if (orders.size() == 0) {
                            paginator.noResultLayout.setVisibility(View.VISIBLE);
                            paginator.recyclerView.setVisibility(View.GONE);

                        }
                        break;

                    default:
                        paginator.loading = false;
                        paginator.empty = true;
                        if (orders.size() == 0) {
                            paginator.noResultLayout.setVisibility(View.VISIBLE);
                            paginator.recyclerView.setVisibility(View.GONE);

                        }
                }
            }

            @Override
            public void onFailure(Call<DataOrdersResponse> call, Throwable t) {
                if (orders.size() == 0) {
                    paginator.noResultLayout.setVisibility(View.VISIBLE);
                    paginator.recyclerView.setVisibility(View.GONE);

                }

                paginator.refreshLayout.setRefreshing(false);
                paginator.progressMoreDataRel.setVisibility(View.GONE);

                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

    }


    public static void getOrder(final Activity activity, final String orderId, final OrderDetailsViewHolder holder) {

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();
        String token = new SharedPref(activity).getToken();

        OrdersApi apiInterface = retrofit.create(OrdersApi.class);

        Call<DataOrderResponse> call = apiInterface.getOrder(token, Utility.version, new SharedPref(activity).getLang(), orderId);


        call.enqueue(new Callback<DataOrderResponse>() {
            @Override
            public void onResponse(Call<DataOrderResponse> call, Response<DataOrderResponse> response) {

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    getOrder(activity, orderId, holder);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;
                    case 401:
                        Dialogs.showLoginDialog(activity);
                        break;
                    case 200:
                        if (response.body().getOrderResponse() != null) {
                            Order order = response.body().getOrderResponse().getOrder();
                            User user = response.body().getOrderResponse().getUser();
                            ArrayList<Product> productArrayList = response.body().getOrderResponse().getProductArrayList();

                            holder.fNameEdt.setText(user.getfName());
                            holder.lNameEdt.setText(user.getlName());
                            holder.mobileEdt.setText(user.getMobile());
                            Address address = new Address();
                            address.setCountry(order.getCountry());
                            address.setCity(order.getCity());
                            address.setStreet(order.getStreet());
                            address.setBlock(order.getBlock());
                            address.setBuildingNum(order.getBuildingNum());

                            holder.addressEdt.setText(Utility.setAddress(address));

                            holder.paymentEdt.setText(order.getPayment());
                            holder.productsPriceEdt.setText(Utility.setPrice(activity, order.getTotalCost()));
                            holder.delFeesEdt.setText(Utility.setPrice(activity, order.getStore().getFees()));
                            holder.totalCostEdt.setText(Utility.setPrice(activity, order.getTotalCost()));

                            holder.productsRecycler.setHasFixedSize(true);
                            holder.productsRecycler.setAdapter(new OrderItemsAdapter(activity, productArrayList));

                            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            holder.productsRecycler.setLayoutManager(linearLayoutManager);

                            Utility.setOrderStatusView(activity, holder, order.getStatus());


                        }
                        break;

                    default:
                        Log.d("error", "error");
                }
            }

            @Override
            public void onFailure(Call<DataOrderResponse> call, Throwable t) {

                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

    }


    public static void addOrder(final Activity activity, final Order order) {

        Dialogs.customProgDialog(activity);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        OrdersApi apiInterface = retrofit.create(OrdersApi.class);
        SharedPref sharedPref = new SharedPref(activity);
        String token = sharedPref.getToken();


        Call<MessageResponse> call = apiInterface.addOrder(token, Utility.version, sharedPref.getLang(), order);

        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                Dialogs.dismissDialog();

                switch (response.code()) {
                    case 401:
                        Dialogs.showLoginDialog(activity);
                        break;
                    case 201:
                        if (response.body() != null) {
                            RealmController.removeFromStore(order.getStoreId());
                            IntentClass.goToActivityAndClear(activity, HomeActivity.class, null);
                            ShippingActivity.address = new Address();
                            Dialogs.showToast(activity.getString(R.string.order_sent), activity);
                        }
                        break;
                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 400:
                                    Dialogs.showToast(body.getErrorBody().getMessage(), activity);
                                    break;

                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong), activity);
                            }

                        } catch (Exception e) {
                        }


                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Dialogs.dismissDialog();

                Dialogs.showDialogOrToast(activity, activity.getResources().getString(R.string.error)
                        , activity.getResources().getString(R.string.no_network)
                        , activity.getResources().getString(R.string.ok_str)
                        , 2);
            }
        });
    }

    public interface OrdersApi {

        @GET("users/orders")
        Call<DataOrdersResponse> getOrders(@Header("Authorization") String token,
                                           @Header("Version") int version,
                                           @Header("locale") String locale,
                                           @Query("page") int page);

        @GET("users/orders/{orderId}")
        Call<DataOrderResponse> getOrder(@Header("Authorization") String token,
                                         @Header("Version") int version,
                                         @Header("locale") String locale,
                                         @Path("orderId") String orderId);

        @POST("users/orders")
        Call<MessageResponse> addOrder(@Header("Authorization") String token,
                                       @Header("Version") int version,
                                       @Header("locale") String locale,
                                       @Body Order order);

        @GET("promocodes")
        Call<PromocodeResponse> validatePromocode(@Header("Authorization") String token,
                                                  @Header("Version") int version,
                                                  @Header("locale") String locale,
                                                  @Query("code") String code);


    }
}