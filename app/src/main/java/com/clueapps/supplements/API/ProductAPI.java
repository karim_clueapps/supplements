package com.clueapps.supplements.API;

import android.app.Activity;
import android.graphics.Paint;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.clueapps.supplements.Holders.ProductDetailsViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.User.BlockActivity;
import com.clueapps.supplements.activities.User.ForceUpdateActivity;
import com.clueapps.supplements.adapters.AdsAdapter;
import com.clueapps.supplements.adapters.CategoryAdapter;
import com.clueapps.supplements.adapters.ProductsAdapter;
import com.clueapps.supplements.adapters.SpinnersAdapter;
import com.clueapps.supplements.adapters.StoreAdapter;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.RefreshToken;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Ad;
import com.clueapps.supplements.models.Data.Category;
import com.clueapps.supplements.models.Data.ImagePath;
import com.clueapps.supplements.models.Data.PaginatorData;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.models.Data.SearchProduct;
import com.clueapps.supplements.models.Data.Store;
import com.clueapps.supplements.models.responses.DataCategoriesResponse;
import com.clueapps.supplements.models.responses.DataProductResponse;
import com.clueapps.supplements.models.responses.DataProductsResponse;
import com.clueapps.supplements.models.responses.DataStoresResponse;
import com.clueapps.supplements.models.responses.ErrorResponse;
import com.clueapps.supplements.models.responses.MessageResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ProductAPI {

    public static Void searchProducts(final Activity activity, final ProductsAdapter productsAdapter, final SearchProduct searchProduct,
                                      final ArrayList<Product> products, final PaginatorData paginator) {


        paginator.loading = true;

        if (paginator.page == 1)
            paginator.refreshLayout.setRefreshing(true);
        else
            paginator.progressMoreDataRel.setVisibility(View.VISIBLE);

        paginator.noResultLayout.setVisibility(View.GONE);


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();
        String token = new SharedPref(activity).getToken();

        ProductsApi apiInterface = retrofit.create(ProductsApi.class);

        Call<DataProductsResponse> call = apiInterface.searchProduct(token, Utility.version, new SharedPref(activity).getLang(),
                searchProduct.getKeyword(), searchProduct.getCategoryId(), searchProduct.getStoreId(), searchProduct.getMinPrice(),
                searchProduct.getMaxPrice(), searchProduct.getOrderBy(), paginator.page);


        call.enqueue(new Callback<DataProductsResponse>() {
            @Override
            public void onResponse(Call<DataProductsResponse> call, Response<DataProductsResponse> response) {

                paginator.refreshLayout.setRefreshing(false);
                paginator.progressMoreDataRel.setVisibility(View.GONE);

                switch (response.code()) {
                    case API.FORCE_UPDATE:
                    case API.BLOCK:
                    case API.REFRESH_TOKEN:

                        API.userResponses(activity, response.code(), searchProducts(activity, productsAdapter, searchProduct, products, paginator));
                        break;

                    case 401:
                        Dialogs.showLoginDialog(activity);
                        break;
                    case 200:
                        if (response.body().getProductsResponse() != null) {
                            productsAdapter.notifyDataSetChanged();
                            products.addAll(response.body().getProductsResponse().getProductArrayList());
                            paginator.loading = false;
                            paginator.page = response.body().getPaginator().getPage() + 1;
                            if (response.body().getPaginator().getNext() == null || response.body().getProductsResponse().getProductArrayList().size() < Utility.take)
                                paginator.empty = true;

                        }
                        if (products.size() == 0) {
                            paginator.noResultLayout.setVisibility(View.VISIBLE);
                            paginator.recyclerView.setVisibility(View.GONE);

                        } else {
                            paginator.recyclerView.setVisibility(View.VISIBLE);
                            paginator.noResultLayout.setVisibility(View.GONE);
                        }
                        break;

                    default:
                        if (products.size() == 0) {
                            paginator.noResultLayout.setVisibility(View.VISIBLE);
                            paginator.refreshLayout.setVisibility(View.GONE);

                        }
                        paginator.loading = false;
                        paginator.empty = true;
                        Log.d("error", "error");
                }
            }

            @Override
            public void onFailure(Call<DataProductsResponse> call, Throwable t) {
                if (products.size() == 0) {
                    paginator.noResultLayout.setVisibility(View.VISIBLE);
                    paginator.recyclerView.setVisibility(View.GONE);

                }
                paginator.refreshLayout.setRefreshing(false);
                paginator.progressMoreDataRel.setVisibility(View.GONE);

                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

        return null;
    }


    public static void getProducts(final Activity activity, final ProductsAdapter productsAdapter, final String storeId,
                                   final ArrayList<Product> products, final PaginatorData paginator) {

        paginator.loading = true;


        paginator.refreshLayout.setRefreshing(true);


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();
        String token = new SharedPref(activity).getToken();

        ProductsApi apiInterface = retrofit.create(ProductsApi.class);

        Call<DataProductsResponse> call = apiInterface.getProducts(token, Utility.version, new SharedPref(activity).getLang(), storeId, paginator.page);


        call.enqueue(new Callback<DataProductsResponse>() {
            @Override
            public void onResponse(Call<DataProductsResponse> call, Response<DataProductsResponse> response) {

                paginator.refreshLayout.setRefreshing(false);

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    getProducts(activity, productsAdapter, storeId, products, paginator);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;
                    case 401:
                        Dialogs.showLoginDialog(activity);
                        break;
                    case 200:
                        if (response.body().getProductsResponse() != null) {
                            productsAdapter.notifyDataSetChanged();
                            products.addAll(response.body().getProductsResponse().getProductArrayList());
                            paginator.loading = false;
                            paginator.page = response.body().getPaginator().getPage() + 1;
                            if (response.body().getPaginator().getNext() == null || response.body().getProductsResponse().getProductArrayList().size() < Utility.take)
                                paginator.empty = true;

                        }
                        break;

                    default:
                        paginator.loading = false;
                        paginator.empty = true;
                        Log.d("error", "error");
                }
            }

            @Override
            public void onFailure(Call<DataProductsResponse> call, Throwable t) {

                paginator.refreshLayout.setRefreshing(false);

                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

    }

    public static void getProduct(final Activity activity, final ProductDetailsViewHolder holder) {

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();
        String token = new SharedPref(activity).getToken();

        ProductsApi apiInterface = retrofit.create(ProductsApi.class);

        Call<DataProductResponse> call = apiInterface.getProduct(token, Utility.version, new SharedPref(activity).getLang(), holder.product.getId());


        call.enqueue(new Callback<DataProductResponse>() {
            @Override
            public void onResponse(Call<DataProductResponse> call, Response<DataProductResponse> response) {

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    getProduct(activity, holder);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;
                    case 401:
                        Dialogs.showLoginDialog(activity);
                        break;
                    case 200:
                        if (response.body().getProductResponse() != null) {
                            int quantityInCart = holder.product.getQuantityCart();
                            holder.product = response.body().getProductResponse().getProduct();
                            holder.product.setQuantityCart(quantityInCart);

                            if (holder.product != null) {
                                holder.nameTxv.setText(holder.product.getName());
                                holder.descTxv.setText(holder.product.getDescription());
                                holder.weightTxv.setText(Utility.setText(activity, holder.product.getWeight(), activity.getString(R.string.kg)));
                                holder.priceTxv.setText(Utility.setPrice(activity, holder.product.getPrice()));
                                holder.brandTxv.setText(holder.product.getBrand());
                                holder.quantityTxv.setText(Utility.setText(activity, holder.product.getQuantity() + "", activity.getString(R.string.pieces)));
                                holder.usageAgeTxv.setText(holder.product.getUsageAge());
                                holder.storeTxv.setText(holder.product.getStore().getName());
                                holder.titleTxv.setText(holder.product.getName());
                                holder.favImv.setImageResource(holder.product.isWishlisted() ? R.drawable.heartwhitefill : R.drawable.hearticonwhitenofill);

                                holder.imagePaths = holder.product.getImages();

                                if (holder.product.getImages() == null || holder.product.getImages().size() == 0)
                                    holder.imagePaths.add(new ImagePath(holder.product.getIcon()));

                                holder.imagesAdapter.notifyDataSetChanged();
                                holder.dotsTab.setupWithViewPager(holder.imagesPager, true);

                                if (holder.product.getPercentage() > 0) {
                                    holder.oldPriceTxv.setVisibility(View.VISIBLE);
                                    holder.oldPriceTxv.setPaintFlags(holder.oldPriceTxv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                                }

                                if (holder.product.getQuantity() <= 0) {
                                    holder.addCartBtn.setEnabled(false);
                                    holder.soldOutImv.setVisibility(View.VISIBLE);
                                } else {
                                    holder.addCartBtn.setEnabled(true);
                                    holder.soldOutImv.setVisibility(View.GONE);
                                }
                            }

                        }
                        break;

                    default:
                        Log.d("error", "error");
                }
            }

            @Override
            public void onFailure(Call<DataProductResponse> call, Throwable t) {

                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

    }

    public static Call<MessageResponse> addToWishlist(final Activity activity, final String productId) {

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        WishlistApi apiInterface = retrofit.create(WishlistApi.class);
        SharedPref sharedPref = new SharedPref(activity);
        String token = sharedPref.getToken();

        RequestBody _productId = RequestBody.create(MediaType.parse("text/plain"), productId);

        Call<MessageResponse> call = apiInterface.addToWishlist(token, Utility.version, sharedPref.getLang(), _productId);

        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    addToWishlist(activity, productId);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;
                    case 401:
                        Dialogs.showLoginDialog(activity);
                        break;
                    case 201:
                        if (response.body() != null) {

                        }
                        break;
                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 400:
                                    Dialogs.showToast(body.getErrorBody().getMessage(), activity);
                                    break;

                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong), activity);
                            }

                        } catch (Exception e) {
                            //  Log.d( "error_retrofit" , e.getMessage());
                        }


                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {

                Dialogs.showDialogOrToast(activity, activity.getResources().getString(R.string.error)
                        , activity.getResources().getString(R.string.no_network)
                        , activity.getResources().getString(R.string.ok_str)
                        , 1);
            }
        });

        return call;
    }

    public static void removeFromWishlist(final Activity activity, final String productId) {

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        WishlistApi apiInterface = retrofit.create(WishlistApi.class);
        SharedPref sharedPref = new SharedPref(activity);
        String token = sharedPref.getToken();
        Call<MessageResponse> call = apiInterface.removeFromWishlist(token, Utility.version, sharedPref.getLang(), productId);

        call.enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    removeFromWishlist(activity, productId);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;

                    case 201:
                        if (response.body() != null) {

                        }
                        break;
                    default:

                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            ErrorResponse body = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);


                            switch (body.getErrorBody().getCode()) {
                                case 400:
                                    Dialogs.showToast(body.getErrorBody().getMessage(), activity);
                                    break;

                                default:
                                    Dialogs.showToast(activity.getString(R.string.something_wrong), activity);
                            }

                        } catch (Exception e) {
                            //   Log.d( "error_retrofit" , e.getMessage());
                        }


                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Dialogs.showDialogOrToast(activity, activity.getResources().getString(R.string.error)
                        , activity.getResources().getString(R.string.no_network)
                        , activity.getResources().getString(R.string.ok_str)
                        , 1);
            }
        });
    }

    public static void getCategories(final Activity activity, final CategoryAdapter categoryAdapter, final ArrayList<Category> categories,
                                     final AdsAdapter adsAdapter, final ArrayList<Ad> ads, final PaginatorData paginator) {

        paginator.loading = true;

        if (paginator.page == 1)
            paginator.refreshLayout.setRefreshing(true);
        else
            paginator.progressMoreDataRel.setVisibility(View.VISIBLE);


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();
        String token = new SharedPref(activity).getToken();

        CategoriesApi apiInterface = retrofit.create(CategoriesApi.class);

        Call<DataCategoriesResponse> call = apiInterface.getCategories(token, Utility.version, new SharedPref(activity).getLang(), paginator.page);


        call.enqueue(new Callback<DataCategoriesResponse>() {
            @Override
            public void onResponse(Call<DataCategoriesResponse> call, Response<DataCategoriesResponse> response) {
                paginator.refreshLayout.setRefreshing(false);
                paginator.progressMoreDataRel.setVisibility(View.GONE);


                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    getCategories(activity, categoryAdapter, categories, adsAdapter, ads, paginator);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;

                    case 200:
                        if (response.body().getCategoriesResponse() != null) {
                            categoryAdapter.notifyDataSetChanged();
                            categories.addAll(response.body().getCategoriesResponse().getCategoryArrayList());
                            paginator.page = response.body().getPaginator().getPage() + 1;

                            if (ads.size() == 0) {
                                ads.addAll(response.body().getCategoriesResponse().getAdArrayList());
                                adsAdapter.notifyDataSetChanged();
                            }
                        }

                        paginator.loading = false;

                        if (response.body().getPaginator().getNext() == null || response.body().getCategoriesResponse().getCategoryArrayList().size() < Utility.take)
                            paginator.empty = true;

                        break;

                    default:
                        paginator.loading = false;
                        paginator.empty = true;
                        Log.d("error", "error");
                }
            }

            @Override
            public void onFailure(Call<DataCategoriesResponse> call, Throwable t) {
                paginator.refreshLayout.setRefreshing(false);
                paginator.progressMoreDataRel.setVisibility(View.GONE);

                paginator.loading = false;
                paginator.empty = true;
                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

    }

    public static void getCategoriesSpinner(final Activity activity, final Spinner categorySpinner, final ArrayList<Category> categoryArray) {

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();
        String token = new SharedPref(activity).getToken();

        CategoriesApi apiInterface = retrofit.create(CategoriesApi.class);

        Call<DataCategoriesResponse> call = apiInterface.getCategoriesUnlimited(token, Utility.version, new SharedPref(activity).getLang(), Utility.take_100);


        call.enqueue(new Callback<DataCategoriesResponse>() {
            @Override
            public void onResponse(Call<DataCategoriesResponse> call, Response<DataCategoriesResponse> response) {

                switch (response.code()) {

                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    getCategoriesSpinner(activity, categorySpinner, categoryArray);
                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;

                    case 200:
                        if (response.body().getCategoriesResponse() != null) {
                            categoryArray.clear();
                            categoryArray.addAll(response.body().getCategoriesResponse().getCategoryArrayList());

                            List<String> categoryList = new ArrayList();
                            categoryList.add(activity.getString(R.string.select_category));

                            for (Category category : response.body().getCategoriesResponse().getCategoryArrayList())
                                categoryList.add(category.getName());

                            SpinnersAdapter categoryAdapter = new SpinnersAdapter(categoryList, activity, R.color.charcoal_grey_85);
                            categorySpinner.setAdapter(categoryAdapter);
                            categorySpinner.setEnabled(true);

                        }

                        break;

                    default:
                        Log.d("error", "error");
                }
            }

            @Override
            public void onFailure(Call<DataCategoriesResponse> call, Throwable t) {
                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

    }

    public static void getStoresSpinner(final Activity activity, final String categoryId, final Spinner storeSpinner, final ArrayList<Store> storeArray) {
        Dialogs.customProgDialog(activity);

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();
        String token = new SharedPref(activity).getToken();

        CategoriesApi apiInterface = retrofit.create(CategoriesApi.class);

        Call<DataStoresResponse> call = apiInterface.getStoresUnlimited(token, Utility.version, new SharedPref(activity).getLang(), categoryId, Utility.take_100);


        call.enqueue(new Callback<DataStoresResponse>() {
            @Override
            public void onResponse(Call<DataStoresResponse> call, Response<DataStoresResponse> response) {
                Dialogs.dismissDialog();

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    getStoresSpinner(activity, categoryId, storeSpinner, storeArray);
                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;

                    case 200:
                        if (response.body().getStoresResponse() != null) {
                            storeArray.clear();
                            storeArray.addAll(response.body().getStoresResponse().getStores());

                            List<String> storeList = new ArrayList();
                            storeList.add(activity.getString(R.string.select_store));

                            for (Store store : response.body().getStoresResponse().getStores())
                                storeList.add(store.getName());

                            SpinnersAdapter storeAdapter = new SpinnersAdapter(storeList, activity, R.color.charcoal_grey_85);
                            storeSpinner.setAdapter(storeAdapter);
                            storeSpinner.setEnabled(true);

                        }

                        break;

                    default:
                        Log.d("error", "error");
                }
            }

            @Override
            public void onFailure(Call<DataStoresResponse> call, Throwable t) {
                Dialogs.dismissDialog();
                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

    }


    public static void getStores(final Activity activity, final StoreAdapter storeAdapter, final String categoryId,
                                 final ArrayList<Store> stores, final PaginatorData paginator) {

        paginator.loading = true;


        paginator.refreshLayout.setRefreshing(true);


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();
        String token = new SharedPref(activity).getToken();

        CategoriesApi apiInterface = retrofit.create(CategoriesApi.class);

        Call<DataStoresResponse> call = apiInterface.getStores(token, Utility.version, new SharedPref(activity).getLang(), categoryId, paginator.page);


        call.enqueue(new Callback<DataStoresResponse>() {
            @Override
            public void onResponse(Call<DataStoresResponse> call, Response<DataStoresResponse> response) {

                paginator.refreshLayout.setRefreshing(false);

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    getStores(activity, storeAdapter, categoryId, stores, paginator);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();
                            }
                        });
                        break;

                    case 200:
                        if (response.body().getStoresResponse() != null) {
                            storeAdapter.notifyDataSetChanged();
                            stores.addAll(response.body().getStoresResponse().getStores());
                            paginator.loading = false;
                            paginator.page = response.body().getPaginator().getPage() + 1;
                            if (response.body().getPaginator().getNext() == null || response.body().getStoresResponse().getStores().size() < Utility.take)
                                paginator.empty = true;

                        }
                        break;

                    default:
                        paginator.loading = false;
                        paginator.empty = true;
                        Log.d("error", "error");
                }
            }

            @Override
            public void onFailure(Call<DataStoresResponse> call, Throwable t) {
                paginator.refreshLayout.setRefreshing(false);

                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

    }


    public static void getStoresTabs(final Activity activity, final TabLayout storeTabLayout, final String categoryId, final Store store,
                                     final ArrayList<Store> stores, final PaginatorData paginator) {

        paginator.loading = true;

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();
        String token = new SharedPref(activity).getToken();

        CategoriesApi apiInterface = retrofit.create(CategoriesApi.class);

        Call<DataStoresResponse> call = apiInterface.getStoresUnlimited(token, Utility.version, new SharedPref(activity).getLang(), categoryId, Utility.take_100);


        call.enqueue(new Callback<DataStoresResponse>() {
            @Override
            public void onResponse(Call<DataStoresResponse> call, Response<DataStoresResponse> response) {

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    getStoresTabs(activity, storeTabLayout, categoryId, store, stores, paginator);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;

                    case 200:
                        if (response.body().getStoresResponse() != null) {

                            Utility.setTabs(response.body().getStoresResponse().getStores(), storeTabLayout);

                            stores.addAll(response.body().getStoresResponse().getStores());
                            paginator.loading = false;
                            paginator.page = response.body().getPaginator().getPage() + 1;
                            if (response.body().getPaginator().getNext() == null || response.body().getStoresResponse().getStores().size() < Utility.take)
                                paginator.empty = true;

                            if (store != null && store.getId() != null) {
                                //     storeTabLayout.setScrollPosition(Utility.searchStore(stores, store.getId()), 0, true);
                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        storeTabLayout.setScrollPosition(Utility.searchStore(stores, store.getId()), 0, true);
                                      //  store.setId(null);
                                    }
                                },  100);
                            } else {
                                storeTabLayout.setScrollPosition(0, 0, true);
                            }
                        }
                        break;

                    default:
                        paginator.loading = false;
                        paginator.empty = true;
                        Log.d("error", "error");
                }
            }

            @Override
            public void onFailure(Call<DataStoresResponse> call, Throwable t) {

                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

    }


    public static void getProductsByCategory(final Activity activity, final ProductsAdapter productsAdapter, final String categoryId,
                                             final ArrayList<Product> products, final PaginatorData paginator) {

        paginator.loading = true;

        paginator.refreshLayout.setRefreshing(true);


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();
        String token = new SharedPref(activity).getToken();

        CategoriesApi apiInterface = retrofit.create(CategoriesApi.class);

        Call<DataProductsResponse> call = apiInterface.getProducts(token, Utility.version, new SharedPref(activity).getLang(), categoryId, paginator.page);


        call.enqueue(new Callback<DataProductsResponse>() {
            @Override
            public void onResponse(Call<DataProductsResponse> call, Response<DataProductsResponse> response) {

                paginator.refreshLayout.setRefreshing(false);

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    getProducts(activity, productsAdapter, categoryId, products, paginator);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;

                    case 200:
                        if (response.body().getProductsResponse() != null) {
                            productsAdapter.notifyDataSetChanged();
                            products.addAll(response.body().getProductsResponse().getProductArrayList());
                            paginator.loading = false;
                            paginator.page = response.body().getPaginator().getPage() + 1;
                            if (response.body().getPaginator().getNext() == null || response.body().getProductsResponse().getProductArrayList().size() < Utility.take)
                                paginator.empty = true;

                        }
                        break;

                    default:
                        paginator.loading = false;
                        paginator.empty = true;
                        Log.d("error", "error");
                }
            }

            @Override
            public void onFailure(Call<DataProductsResponse> call, Throwable t) {
                paginator.refreshLayout.setRefreshing(false);

                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

    }

    public static void getWishlist(final Activity activity, final ProductsAdapter productsAdapter, final ArrayList<Product> products, final PaginatorData paginator) {
        paginator.loading = true;

        paginator.refreshLayout.setRefreshing(true);


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        String token = new SharedPref(activity).getToken();

        WishlistApi apiInterface = retrofit.create(WishlistApi.class);

        Call<DataProductsResponse> call = apiInterface.getWishlist(token, Utility.version, new SharedPref(activity).getLang(), paginator.page);


        call.enqueue(new Callback<DataProductsResponse>() {
            @Override
            public void onResponse(Call<DataProductsResponse> call, Response<DataProductsResponse> response) {

                paginator.refreshLayout.setRefreshing(false);

                switch (response.code()) {
                    case 451:
                        IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                        break;
                    case 456:
                        IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                        break;
                    case 452:
                        RefreshToken.refreshToken(activity).enqueue(new Callback<RefreshToken.DataTokenResponse>() {
                            @Override
                            public void onResponse(Call<RefreshToken.DataTokenResponse> call, Response<RefreshToken.DataTokenResponse> response) {
                                if (response.code() == 200 && response.body() != null) {
                                    SharedPref sharedPref = new SharedPref(activity);
                                    sharedPref.setToken(response.body().getTokenResponse().getToken());
                                    getWishlist(activity, productsAdapter, products, paginator);


                                }
                            }

                            @Override
                            public void onFailure(Call<RefreshToken.DataTokenResponse> call, Throwable t) {
                                if (activity != null)
                                    Toast.makeText(activity, "wrong !!ا", Toast.LENGTH_LONG).show();

                            }
                        });
                        break;

                    case 200:
                        if (response.body().getProductsResponse() != null) {
                            productsAdapter.notifyDataSetChanged();
                            products.addAll(response.body().getProductsResponse().getProductArrayList());
                            paginator.loading = false;
                            paginator.page = response.body().getPaginator().getPage() + 1;
                            if (response.body().getPaginator().getNext() == null || response.body().getProductsResponse().getProductArrayList().size() < Utility.take)
                                paginator.empty = true;

                        }
                        if (products.size() == 0) {
                            paginator.noResultLayout.setVisibility(View.VISIBLE);
                            paginator.recyclerView.setVisibility(View.GONE);
                        }

                        break;

                    default:
                        paginator.loading = false;
                        paginator.empty = true;
                        if (products.size() == 0) {
                            paginator.noResultLayout.setVisibility(View.VISIBLE);
                            paginator.recyclerView.setVisibility(View.GONE);

                        }

                        Log.d("error", "error");
                }
            }

            @Override
            public void onFailure(Call<DataProductsResponse> call, Throwable t) {
                if (products.size() == 0) {
                    paginator.noResultLayout.setVisibility(View.VISIBLE);
                    paginator.recyclerView.setVisibility(View.GONE);

                }
                paginator.refreshLayout.setRefreshing(false);

                Dialogs.showToast(activity.getResources().getString(R.string.no_network), activity);

            }
        });

    }

    public interface CategoriesApi {

        @GET("categories")
        Call<DataCategoriesResponse> getCategories(@Header("Authorization") String token,
                                                   @Header("Version") int version,
                                                   @Header("locale") String locale,
                                                   @Query("page") int page);

        @GET("categories")
        Call<DataCategoriesResponse> getCategoriesUnlimited(@Header("Authorization") String token,
                                                            @Header("Version") int version,
                                                            @Header("locale") String locale,
                                                            @Query("limit") int limit);

        @GET("categories/{categoryId}/stores")
        Call<DataStoresResponse> getStoresUnlimited(@Header("Authorization") String token,
                                                    @Header("Version") int version,
                                                    @Header("locale") String locale,
                                                    @Path("categoryId") String categoryId,
                                                    @Query("limit") int limit);


        @GET("categories/{categoryId}/stores")
        Call<DataStoresResponse> getStores(@Header("Authorization") String token,
                                           @Header("Version") int version,
                                           @Header("locale") String locale,
                                           @Path("categoryId") String categoryId,
                                           @Query("page") int page);


        @GET("categories/{categoryId}/products")
        Call<DataProductsResponse> getProducts(@Header("Authorization") String token,
                                               @Header("Version") int version,
                                               @Header("locale") String locale,
                                               @Path("categoryId") String categoryId,
                                               @Query("page") int page);
    }

    public interface ProductsApi {


        @GET("stores/{storeId}/products")
        Call<DataProductsResponse> getProducts(@Header("Authorization") String token,
                                               @Header("Version") int version,
                                               @Header("locale") String locale,
                                               @Path("storeId") String storeId,
                                               @Query("page") int page);

        @GET("products/{productId}")
        Call<DataProductResponse> getProduct(@Header("Authorization") String token,
                                             @Header("Version") int version,
                                             @Header("locale") String locale,
                                             @Path("productId") String productId);

        @GET("search")
        Call<DataProductsResponse> searchProduct(@Header("Authorization") String token,
                                                 @Header("Version") int version,
                                                 @Header("locale") String locale,
                                                 @Query("keyword") String name,
                                                 @Query("category") String categoryId,
                                                 @Query("store") String storeId,
                                                 @Query("budget_range_min") double minPrice,
                                                 @Query("budget_range_max") double maxPrice,
                                                 @Query("order") String order,
                                                 @Query("page") int page);

    }

    public interface WishlistApi {

        @GET("users/wishlist")
        Call<DataProductsResponse> getWishlist(@Header("Authorization") String token,
                                               @Header("Version") int version,
                                               @Header("locale") String locale,
                                               @Query("page") int page);

        @Multipart
        @POST("users/wishlist")
        Call<MessageResponse> addToWishlist(@Header("Authorization") String token,
                                            @Header("Version") int version,
                                            @Header("locale") String locale,
                                            @Part("product_id") RequestBody productId);

        @FormUrlEncoded
        @HTTP(method = "DELETE", path = "users/wishlist", hasBody = true)
        Call<MessageResponse> removeFromWishlist(@Header("Authorization") String token,
                                                 @Header("Version") int version,
                                                 @Header("locale") String locale,
                                                 @Field("product_id") String productId);

    }
}