package com.clueapps.supplements.helpers;

import android.content.Context;
import android.graphics.Typeface;

import com.clueapps.supplements.R;

/**
 * Created by as on 8/1/2017.
 */

public abstract class Fonts {
    public static Typeface regTypeface(Context context)
    {
        return Typeface.createFromAsset(context.getAssets(), context.getString(R.string.font_reg));
    }
    public static Typeface boldTypeface(Context context)
    {
        return Typeface.createFromAsset(context.getAssets(), context.getString(R.string.font_bold));
    }
    public static Typeface lightTypeface(Context context)
    {
        return Typeface.createFromAsset(context.getAssets(), context.getString(R.string.font_light));
    }
}
