package com.clueapps.supplements.helpers;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.clueapps.supplements.Holders.OrderDetailsViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.models.Data.Address;
import com.clueapps.supplements.models.Data.Order;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.models.Data.Store;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * <h1>Implement reusable methods</h1>
 * Utility class implement methods that used in the whole app
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-08-9
 */
public abstract class Utility {

    public static final String ORDER_PENDING = "pending" ;
    public static final String ORDER_CONFIRMED = "confirmed" ;
    public static final String ORDER_DELIVERED = "delivered" ;
    public static final String ORDER_CANCELED = "cancelled" ;


    public static final String LOW_TO_HIGH = "low_to_high" ;
    public static final String HIGH_TO_LOW = "high_to_low" ;


    public static int take = 10 ;
    public static int take_100 = 100 ;
    public static int version  = 1 ;

    public static Activity currentActivity = null;



    /**
     * This method is used to change order view depend on order status.
     * called when get order details.
     * @param activity for current activity
     * @param holder view golder of order view
     * @param orderStatus
     */
    public static void setOrderStatusView(Activity activity ,OrderDetailsViewHolder holder , String orderStatus){

        switch (orderStatus){

            case Utility.ORDER_PENDING :
                holder.pendingImv.setImageResource(R.drawable.subcartscreenprogressicon);
                holder.confirmedImv.setImageResource(R.drawable.subcartscreenfreycircle);
                holder.deliveredImv.setImageResource(R.drawable.subcartscreenfreycircle);
                holder.line1View.setBackgroundColor(activity.getResources().getColor(R.color.gray_light));
                holder.line2View.setBackgroundColor(activity.getResources().getColor(R.color.gray_light));
                holder.statusLayout.setVisibility(View.VISIBLE);
                holder.canceledTxv.setVisibility(View.GONE);

                break;

            case Utility.ORDER_CONFIRMED :
                holder.pendingImv.setImageResource(R.drawable.red_right);
                holder.confirmedImv.setImageResource(R.drawable.subcartscreenprogressicon);
                holder.deliveredImv.setImageResource(R.drawable.subcartscreenfreycircle);
                holder.line1View.setBackgroundColor(activity.getResources().getColor(R.color.red));
                holder.line2View.setBackgroundColor(activity.getResources().getColor(R.color.gray_light));
                holder.statusLayout.setVisibility(View.VISIBLE);
                holder.canceledTxv.setVisibility(View.GONE);

                break;

            case Utility.ORDER_DELIVERED :
                holder.pendingImv.setImageResource(R.drawable.red_right);
                holder.confirmedImv.setImageResource(R.drawable.red_right);
                holder.deliveredImv.setImageResource(R.drawable.subcartscreenprogressicon);
                holder.line1View.setBackgroundColor(activity.getResources().getColor(R.color.red));
                holder.line2View.setBackgroundColor(activity.getResources().getColor(R.color.red));
                holder.statusLayout.setVisibility(View.VISIBLE);
                holder.canceledTxv.setVisibility(View.GONE);

            break;

            case Utility.ORDER_CANCELED :
                holder.canceledTxv.setVisibility(View.VISIBLE);
                holder.statusLayout.setVisibility(View.GONE);

                break;

        }
    }


    public static String gettingDeviceId(Context context){

        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        return "" + tm.getDeviceId();
    }


     public static void setCurrentActivity(Activity currentActivity) {
        Utility.currentActivity = currentActivity;
        if(!checkNetworkStatus())
        {
            Dialogs.noInternetDialog();
        }
    }

    public static boolean checkNetworkStatus()
    {
        if(currentActivity==null)
            return false;
        ConnectivityManager connectivityManager = ((ConnectivityManager) currentActivity.getSystemService(
                Context.CONNECTIVITY_SERVICE));
        return (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected());
    }


     public static void downloadImage(Activity activity , String imageUrl , int placeHolder, ImageView imageView){

        if ( imageUrl != "" && imageUrl != null)
        {
            try {
                Picasso.with(activity).load(imageUrl)
                        .placeholder(placeHolder)
                        .into(imageView);
            } catch (Exception e) {
                imageView.setImageResource(placeHolder);
            }
            catch (Error error)
            {
                imageView.setImageResource(placeHolder);
            }
        }else
             imageView.setImageResource(placeHolder);

    }


    public static void setTabs(ArrayList<Store> storeArrayList , TabLayout tabLayout){
        for (int i = 0; i < storeArrayList.size() ; i ++ ) {
            tabLayout.addTab(tabLayout.newTab().setText(storeArrayList.get(i).getName()));
        }
    }


    public static String setAddress(Address address){

       return (address.getFloorNum() != null ? address.getFloorNum() : "") +
               address.getBuildingNum() + " ," +  address.getStreet() + " ," + address.getBlock()
                       + " ," + address.getCity() + " ," + address.getCountry() ;
    }

    public static String setPrice(Activity activity ,double price){
        SharedPref sharedPref = new SharedPref(activity);
        String priceStr ;
        if ( sharedPref.getLang().equals("ar") )
            priceStr =  activity.getResources().getString(R.string.currency) + "\u200E "+ setPriceFormat( price);
        else
            priceStr = setPriceFormat( price) + " " + activity.getResources().getString(R.string.currency);

        return priceStr;
    }

    public static String setPriceFormat(double price){

        double x = price - Math.floor(price);
        if (  x * 10 > 0  )
            return  String.format("%.2f", price);

        return  (int) price + "";
    }

    public static String setText(Activity activity , String firstStr ,String secondStr){
        if ( new SharedPref(activity).getLang().equals("ar"))
            return  secondStr  + "\u200E "+ firstStr;
        else
            return  firstStr + " "+ secondStr ;
    }
    public static double getTotalPrice(ArrayList<Product> productArrayList) {

        double totalPrice = 0 ;
        for ( int i = 0 ; i < productArrayList.size() ; i ++ ){
            totalPrice += (productArrayList.get(i).getPrice()* productArrayList.get(i).getQuantityCart() );
        }
        return totalPrice;
    }


    public static int searchStoreId(ArrayList<Product> productArrayList, String id) {
        for ( int i = 0 ; i < productArrayList.size() ; i ++ ){
            if ( id.equals(productArrayList.get(i).getStore().getId()))
                return i;
        }
        return -1;
    }

    public static int searchStore(ArrayList<Store> storeArrayList, String id) {
        for ( int i = 0 ; i < storeArrayList.size() ; i ++ ){
            if ( id.equals(storeArrayList.get(i).getId()))
                return i;
        }
        return -1;
    }

    public static void setSpinner(String str , Spinner spinner , List<String> data){

        for(int i = 0;i<data.size();i++)
        {
            if(data.get(i).toLowerCase().equals(str.toLowerCase())) {
                spinner.setSelection(i);
            }
        }
    }

    public static int getCountry(String str , List<String> data){

        for(int i = 0;i<data.size();i++)
        {
            if(data.get(i).toLowerCase().equals(str.toLowerCase())) {
                return i;
            }
        }
        return -1;
    }

    public static void setUserRawView(Activity activity, LinearLayout inputLayout , int drawable , EditText inputEdt , String hint , String data){

        TextInputLayout inputTxtLayout = (TextInputLayout) inputLayout.findViewById(R.id.enter_txtLayout);
        inputTxtLayout.setHint(hint);
        ImageView icon = (ImageView) inputLayout.findViewById(R.id.icon_imv);
        icon.setImageDrawable(activity.getResources().getDrawable(drawable));
        inputEdt.setText(data);

    }

    public static void setAddressRawView(TextView titleTxv , EditText inputEdt , String hint ,String title){
        titleTxv.setText(title);
        inputEdt.setHint(hint);
        titleTxv.setAllCaps(false);

    }
}