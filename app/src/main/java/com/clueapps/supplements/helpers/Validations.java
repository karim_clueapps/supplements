package com.clueapps.supplements.helpers;

import android.view.View;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1>Implement all Validations in data</h1>
 * Validations class for validate data and animate errors
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-08-9
 */

public class Validations {

    /**
     * This method is used to validate user name and animate  error view.
     * called when handle validation of user data.
     * @param name of user
     * @param targetTxv of name text view error
     */

    public static void  animateNameView(String name,TextView targetTxv){

        if ( !isValidName(name) ) {
            targetTxv.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.Tada)
                    .duration(1300)
                    .playOn(targetTxv);
        }else
            targetTxv.setVisibility(View.GONE);

    }
    /**
     * This method is used to validate string length and animate error view.
     * called when handle validation of string at least one char.
     * @param str
     * @param targetTxv of str text view error
     */
    public static void  animateStrView(String str,TextView targetTxv){

        if ( !isValidStr(str) ) {
            targetTxv.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.Tada)
                    .duration(1300)
                    .playOn(targetTxv);
        }else
            targetTxv.setVisibility(View.GONE);

    }
    /**
     * This method is used to validate spinner input and animate error view.
     * called when handle validation of data in spinner.
     * @param pos of selected item in spinner
     * @param targetTxv of spinner text view error
     */
    public static void animateSpinnerView(int pos,TextView targetTxv){

        if ( !isValidSpinnerItem(pos)) {
            targetTxv.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.Tada)
                    .duration(1300)
                    .playOn(targetTxv);
        }else
            targetTxv.setVisibility(View.GONE);

    }
    /**
     * This method is used to animate error view.
     * called when there is errors from api.
     * @param targetTxv of text view error
     */
    public static void animateView(TextView targetTxv){


            targetTxv.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.Tada)
                    .duration(1300)
                    .playOn(targetTxv);
    }
    /**
     * This method is used to validate email and animate error view.
     * called when handle validation of user data.
     * @param email of user
     * @param targetTxv of text view error
     */

    public static void  animateEmailView(String email,TextView targetTxv){

        if ( !isValidEmail(email)) {
            targetTxv.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.Tada)
                    .duration(1300)
                    .playOn(targetTxv);
        }else
            targetTxv.setVisibility(View.GONE);

    }

    /**
     * This method is used to validate mobile and animate error view.
     * called when handle validation of user data.
     * @param mobile of user
     * @param targetTxv of text view error
     */
    public static void  animateMobileView(String mobile,TextView targetTxv){

        if ( !isValidMobile(mobile)) {
            targetTxv.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.Tada)
                    .duration(1300)
                    .playOn(targetTxv);
        }else
            targetTxv.setVisibility(View.GONE);

    }

    /**
     * This method is used to validate password and animate error view.
     * called when handle validation of user data.
     * @param password of user
     * @param targetTxv of text view error
     */
    public static void  animatePasswordView(String password,TextView targetTxv){

        if ( !isValidPassword(password)) {
            targetTxv.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.Tada)
                    .duration(1300)
                    .playOn(targetTxv);
        }else
            targetTxv.setVisibility(View.GONE);

    }

    /**
     * This method is used to validate password and animate error view.
     * called when handle validation of user data.
     * @param code of user
     * @param targetTxv of text view error
     */
    public static void  animateCodeView(String code,TextView targetTxv){

        if ( !isValidCode(code)) {
            targetTxv.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.Tada)
                    .duration(1300)
                    .playOn(targetTxv);
        }else
            targetTxv.setVisibility(View.GONE);

    }

    /**
     * This method is used to compare password with confirm password and animate error view.
     * called when handle validation of user data.
     * @param pass of user
     * @param confPass of confirm password
     * @param targetTxv of text view error
     */
    public static void  animatePasswordNotMatchView(String pass,String confPass,TextView targetTxv){

        if ( !pass.equals(confPass)) {
            targetTxv.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.Tada)
                    .duration(1300)
                    .playOn(targetTxv);
        }else
            targetTxv.setVisibility(View.GONE);

    }

    public static boolean isValidStr(String str){
        return str.length() > 0 ;
    }

    public static boolean isValidName(String name){

        return name.length() > 2 && name.matches("[a-zA-Z]+");
    }

    public static boolean isValidMobile(String mobile){
        return mobile.length() > 7 && mobile.length() <= 15 ;
    }
    public static boolean isValidCode(String code){
        return code.length() == 4 ;
    }

    public static boolean isValidPassword(String password) {
        return password.length() >= 6 ;
    }

    public static boolean isValidSpinnerItem(int pos) {
        return pos != 0 ;
    }


    public static boolean isValidEmail(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
}
