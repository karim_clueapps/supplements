package com.clueapps.supplements.helpers;

import android.content.Context;

import com.clueapps.supplements.API.API;
import com.clueapps.supplements.R;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;


public class RefreshToken {

    public static Call<DataTokenResponse> refreshToken(Context context){
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(API.BASE_URL)
                .build();

        refreshTokenAPI api = retrofit.create(refreshTokenAPI.class);

        TokenRequest request = new TokenRequest();
        request.setDeviceId(Utility.gettingDeviceId(context));
        request.setFirebaseToken(FirebaseInstanceId.getInstance().getToken());
        SharedPref sharedPref = new SharedPref(context);
        String token = sharedPref.getToken();

        Call<DataTokenResponse> call = api.getToken(token,Utility.version , sharedPref.getLang(),request);

        return call;
    }

    public static class DataTokenResponse{
        @SerializedName("data")
        @Expose
        private TokenResponse tokenResponse;

        public TokenResponse getTokenResponse() {
            return tokenResponse;
        }
    }


    public static class TokenResponse{
        @SerializedName("msg")
        @Expose
        private String msg;
        @SerializedName("api_token")
        @Expose
        private String token;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }

    private static class TokenRequest{
        @SerializedName("device_id")
        @Expose
        private String deviceId;

        @SerializedName("firebase_token")
        @Expose
        private String firebaseToken;


        public void setFirebaseToken(String firebaseToken) {
            this.firebaseToken = firebaseToken;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }
    }

    public interface refreshTokenAPI{

        @POST("refresh")
        Call<DataTokenResponse> getToken(@Header("Authorization") String token,
                                     @Header("Version") int version,
                                     @Header("locale") String locale ,
                                     @Body TokenRequest request);
    }
}
