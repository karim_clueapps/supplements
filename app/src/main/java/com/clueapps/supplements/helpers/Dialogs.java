package com.clueapps.supplements.helpers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.clueapps.supplements.API.UserAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.User.LoginActivity;

/**
 * <h1>Implement reusable dialogs</h1>
 * Dialogs class for all dialogs and toasts
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-08-9
 */

public abstract class Dialogs {


    public static Dialog dialog;

    public static Dialog noInternetDialog;

    public static void showForgetDialog(Activity activity){
        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_success_forget_pass);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    public static void showLoginDialog(final Activity activity){
        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_confirm_remove);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Button okBtn = (  Button) dialog.findViewById(R.id.ok_btn);
        Button cancelBtn = (  Button) dialog.findViewById(R.id.cancel_btn);

        TextView titleTxv = (  TextView) dialog.findViewById(R.id.dialoge_title_txt);

        okBtn.setText(activity.getString(R.string.login));
        titleTxv.setText(activity.getString(R.string.must_login));


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(activity, LoginActivity.class,null);
                dialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    public static void informativeDialog(Activity activity, String title, String body, String dismissTxt) {

        if ( activity != null ) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_informative);

            TextView titleTxv = (TextView) dialog.findViewById(R.id.title_txv);
            TextView bodyTxv = (TextView) dialog.findViewById(R.id.body_txv);
            Button reloadBtn = (Button) dialog.findViewById(R.id.reload_btn);

            titleTxv.setText(title);
            bodyTxv.setText(body);
            reloadBtn.setText(dismissTxt);

            dialog.setCancelable(true);
            reloadBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    public static void showDialogOrToast(Activity activity,String title,String body,String dismissTxt ,int type ){
        if ( type == 1 )
            informativeDialog(activity,title,body,dismissTxt) ;
        if ( type == 2 )
            showToast(title,activity);

    }

    public static void customProgDialog(Activity activity) {

        if ( dialog == null || !dialog.isShowing()) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_prog);
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    public static void dismissDialog() {
        if ( dialog != null && dialog.isShowing()){
            try {
                dialog.dismiss();
            }catch (Exception e){
            }
        }
    }

    public static void noInternetDialog()
    {
        try {
            if (Utility.currentActivity != null && !Utility.currentActivity.isFinishing() && noInternetDialog == null) {
                noInternetDialog = new Dialog(Utility.currentActivity, R.style.DialogFullScreenTheme);
                noInternetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                noInternetDialog.setContentView(R.layout.dialog_no_internet);
                noInternetDialog.setCancelable(false);
                noInternetDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                Button reloadBtn = (Button) noInternetDialog.findViewById(R.id.reload_button);
                reloadBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Utility.checkNetworkStatus()) {
                            Utility.currentActivity.recreate();
                            noInternetDialog.dismiss();
                        } else {
                            if (Utility.currentActivity != null)
                                showToast(Utility.currentActivity.getResources().getString(R.string.no_connection_str), Utility.currentActivity);
                        }
                    }
                });
            }
            if (Utility.currentActivity != null && !Utility.currentActivity.isFinishing() && noInternetDialog != null)
                noInternetDialog.show();
        }catch (Exception e){}
    }

    public static void dismissInternetDialog() {
        if(noInternetDialog!=null)
        {
            noInternetDialog.dismiss();
            if(Utility.currentActivity!=null) {
                   Utility.currentActivity.recreate();
                }else {
                    Utility.currentActivity.startActivity(Utility.currentActivity.getIntent());
                    Utility.currentActivity.finish();
                }
            }
    }


    public static void showToast(String message, Activity activity) {
        Toast.makeText(activity,message,Toast.LENGTH_SHORT).show();
    }

    public static void showToast(String message, Context context) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

}
