package com.clueapps.supplements.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * <h1>Implement reusable methods of shared preferences</h1>
 * SharedPref class implement methods that insert,get,delete and update in shared preferences
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-08-9
 */


public class SharedPref {

    public static SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name

    private static final String PREF_NAME = "pref";

    public void clearUserData(){
        editor.remove("token");
        editor.remove("fName");
        editor.remove("lName");
        editor.remove("userMob");
        editor.remove("userEmail");
        editor.remove("address");
        editor.remove("addressId");
        editor.commit();
    }
    public SharedPref(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public String getString(String key){
        return sharedPreferences.getString(key,"");
    }
    public void putString(String key, String value){
        editor.putString(key, value);
        editor.commit();
    }
    public void putLong(String key, long value){
        editor.putLong(key, value);
        editor.commit();
    }
    public void clear(){
        editor.clear();
        editor.commit();
    }
    public void putFloat(String key, float value){
        editor.putFloat(key, value);
        editor.commit();
    }
    public float getFloat(String key , float defaultVal ){
        return sharedPreferences.getFloat(key,defaultVal);
    }

    public void putInt(String key, int value){
        editor.putInt(key, value);
        editor.commit();
    }


    public int getInt(String key , int defaultVal ){
        return sharedPreferences.getInt(key,defaultVal);
    }

    public long getLong(String key , int defaultVal ){
        return sharedPreferences.getLong(key,defaultVal);
    }
    public void putBoolean(String status, boolean status1) {
        editor.putBoolean(status, status1);
        editor.commit();
    }

    public boolean getBoolean(String key , boolean defaultVal ){
        return sharedPreferences.getBoolean(key,defaultVal);
    }

    public String getLang() {
        return  !getString("lang").equals("") ? getString("lang") : "en";
    }

    // userData    { token ,id ,name ,mobile ,email ,image ,lang }

    public String getToken() {
        if ( getString("token").equals(""))
            return "";

        return   "Bearer " + getString("token");
    }

    public void setLang(String value)
    {
        editor.putString("lang", value);
        editor.commit();
    }

    public void setToken(String value)
    {
        putString("token",value);

    }

    public  void setUserId (String value){
        editor.putString("userId", value);
        editor.commit();
    }

    public int getUserId(){

        return getInt("userId",0);
    }


    public String getUserImage()

    {
        return   getString("userImage");
    }


    public void setUserImage(String value)
    {
        editor.putString("userImage", value);
        editor.commit();
    }

    public String getFName()

    {
        return   getString("fName");
    }

    public void setFName(String value)
    {
        editor.putString("fName", value);
        editor.commit();
    }

    public String getLName()

    {
        return   getString("lName");
    }

    public void setLName(String value)
    {
        editor.putString("lName", value);
        editor.commit();
    }
    public String getAddress() {
        return   getString("address");
    }

    public void setAddress(String value)
    {
        editor.putString("address", value);
        editor.commit();
    }
    public String getBirth() {
        return   getString("birth");
    }

    public void setBirth(String value)
    {
        editor.putString("birth", value);
        editor.commit();
    }
    public String getUserMob() {
        return   getString("userMob");
    }

    public void setUserMob(String value)
    {
        editor.putString("userMob", value);
        editor.commit();
    }

    public void setGender(String value)
    {
        editor.putString("gender", value);
        editor.commit();
    }
    public String getGender()

    {
        return   getString("gender");
    }

    public String getUserEmail()

    {
        return   getString("userEmail");
    }

    public void setUserEmail(String value)
    {
        editor.putString("userEmail", value);
        editor.commit();
    }
}
