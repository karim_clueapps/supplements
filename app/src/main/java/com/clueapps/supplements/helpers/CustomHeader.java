package com.clueapps.supplements.helpers;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Cart.CartActivity;
import com.clueapps.supplements.realm.RealmController;


/**
 * <h1>CustomHeader for customize bar</h1>
 * CustomHeader class for handle clicks and view in bar
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-08-9
 */
public abstract class CustomHeader {

    /**
     * This method is used to set title in bar and handle click of back.
     * called when open activity.
     * @param activity of viewed activity
     * @param title of bar
     */
    public static void setToolbar(final Activity activity, String title) {
        Toolbar toolbar = (Toolbar)activity.findViewById(R.id.toolbar);
        TextView titleTxv = (TextView)toolbar.findViewById(R.id.toolbar_title);
        ImageView ic_back = (ImageView)toolbar.findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });

        titleTxv.setText(title);
    }

    /**
     * This method is used to set title in bar and handle click of cart and back.
     * called when open activity with cart in view.
     * @param activity of viewed activity
     * @param title of bar
     */

    public static void setToolbarCart(final Activity activity, String title) {
        Toolbar toolbar = (Toolbar)activity.findViewById(R.id.toolbar);
        TextView titleTxv = (TextView)toolbar.findViewById(R.id.toolbar_title);
        ImageView ic_back = (ImageView)toolbar.findViewById(R.id.ic_back);
        ImageView cartImv = (ImageView)toolbar.findViewById(R.id.cart_imv);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });
        
        cartImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                IntentClass.goToActivity(activity, CartActivity.class,null);
            }
        });

        titleTxv.setText(title);
        // view products number in cart
        setCartNumItems(toolbar);
    }

    /**
     * This method is used to set products number in cart.
     * called when open activity with cart in view with bar.
     * @param toolbar of viewed activity
     */
    public static void setCartNumItems(Toolbar toolbar){
        RelativeLayout numItemLayout = (RelativeLayout) toolbar.findViewById(R.id.numItem_layout);
        TextView numTxv = (TextView) toolbar.findViewById(R.id.num_txv);

        // if there is no products in cart hide circle in view
        if (   RealmController.getProductsNumber() == 0 )
            numItemLayout.setVisibility(View.GONE);
        // if there is products in cart show circle in view and number of products
        else{
            numItemLayout.setVisibility(View.VISIBLE);
            numTxv.setText( RealmController.getProductsNumber()+"");
        }


    }
    /**
     * This method is used to set products number in cart.
     * called when open activity with cart in view without bar.
     * @param cartLayout of viewed activity
     */
    public static void setCartNumItems(RelativeLayout cartLayout){
        if ( cartLayout != null ) {
            RelativeLayout numItemLayout = (RelativeLayout) cartLayout.findViewById(R.id.numItem_layout);
            TextView numTxv = (TextView) cartLayout.findViewById(R.id.num_txv);

            // if there is no products in cart hide circle in view
            if (RealmController.getProductsNumber() == 0)
                numItemLayout.setVisibility(View.GONE);
                // if there is products in cart show circle in view and number of products
            else {
                numItemLayout.setVisibility(View.VISIBLE);
                numTxv.setText(RealmController.getProductsNumber() + "");
            }
        }

    }
    /**
     * This method is used to set toolbar dialog and handle click of close.
     * called when open dialog with cart in view without bar.
     * @param dialog of viewed dialog
     * @param title of bar
     */
    public static void setDialogToolbar(final Dialog dialog, String title){
        Toolbar toolbar = (Toolbar)dialog.findViewById(R.id.toolbar);
        TextView titleTxv = (TextView)toolbar.findViewById(R.id.toolbar_title);
        ImageView ic_back = (ImageView)toolbar.findViewById(R.id.ic_back);

        ic_back.setImageResource(R.drawable.close_icon);

        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        titleTxv.setText(title);
    }
    /**
     * This method is used to change products number in cart.
     * called when user change products quantity in cart.
     * @param activity of viewed activity
     */
    public static void changeCartNum(Activity activity){
        RelativeLayout cartLayout = (RelativeLayout) activity.findViewById(R.id.cart_layout);
        CustomHeader.setCartNumItems(cartLayout);
    }

}
