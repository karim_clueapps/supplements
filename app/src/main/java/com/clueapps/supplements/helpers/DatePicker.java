package com.clueapps.supplements.helpers;

import android.app.Activity;
import android.view.View;

/**
 * Created by as on 5/22/2017.
 */

public abstract class DatePicker {
    public static void chooseDate(View view, Activity activity) {
        activity.showDialog(0);
    }

    public static void chooseTime(View view, Activity activity) {
        activity.showDialog(1);
    }

    public static String pad(int c)
    {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

}
