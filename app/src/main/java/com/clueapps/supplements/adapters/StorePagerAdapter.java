package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.clueapps.supplements.models.Data.Store;

import java.util.ArrayList;

public class StorePagerAdapter extends FragmentPagerAdapter {

    Activity activity;
    ArrayList<Store> storeArrayList;

    public StorePagerAdapter(FragmentManager fm, Activity activity , ArrayList<Store> storeArrayList) {
        super(fm);
         this.activity = activity ;
        this.storeArrayList = storeArrayList;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return storeArrayList.get(position).getName();

    }


}

