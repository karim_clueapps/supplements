package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clueapps.supplements.Holders.OrderViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Order.OrderActivity;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Order;

import java.util.ArrayList;

/**
 * Created by mac on 5/18/2017 AD.
 */

public class OrdersAdapter extends RecyclerView.Adapter<OrderViewHolder> {

    Activity activity;
    ArrayList<Order> orderArrayList;

    public OrdersAdapter(Activity activity, ArrayList<Order> orderArrayList){
        this.activity=activity;
        this.orderArrayList = orderArrayList;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_order, parent, false);
        return new OrderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final OrderViewHolder holder, final int position) {
        holder.statusTxv.setText(orderArrayList.get(position).getStatus());
        holder.numberTxv.setText(orderArrayList.get(position).getOrderId());
        holder.quantityTxv.setText(orderArrayList.get(position).getQuantity() +"");
        holder.priceTxv.setText(Utility.setPrice(activity,orderArrayList.get(position).getTotalCost()));
        holder.fromTxv.setText(orderArrayList.get(position).getStore().getName());

        holder.openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("order",orderArrayList.get(position));
                IntentClass.goToActivity(activity, OrderActivity.class , bundle);
            }
        });

    }

    @Override
    public int getItemCount() {
        return orderArrayList.size();
    }


}
