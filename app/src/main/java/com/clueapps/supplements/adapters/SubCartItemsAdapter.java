package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.clueapps.supplements.Holders.ProductCartItemViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Cart.SubCartActivity;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.models.Data.Store;
import com.clueapps.supplements.realm.RealmController;

import java.util.ArrayList;

/**
 * Created by mac on 5/18/2017 AD.
 */

public class SubCartItemsAdapter extends RecyclerView.Adapter<ProductCartItemViewHolder> {
    Activity activity;
    ArrayList<Product> productArrayList;
    Store store;
    public SubCartItemsAdapter(Activity activity, ArrayList<Product> productArrayList,Store store){
        this.activity=activity;
        this.productArrayList = productArrayList;
        this.store = store;
    }

    @Override
    public ProductCartItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sub_cart_item, parent, false);
        return new ProductCartItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProductCartItemViewHolder holder, final int position) {
        Product product = productArrayList.get(position);
        holder.nameTxv.setText(product.getName());
        holder.priceTxv.setText(Utility.setPrice(activity, product.getPrice()) );
        holder.quantityTxv.setText((Utility.setText(activity, product.getQuantityCart()+"" ,activity.getString(R.string.pieces) )));
        holder.quantityViewTxv.setText(product.getQuantityCart()+"");
        holder.changeQuantityLayout.setVisibility(View.VISIBLE);
        Utility.downloadImage(activity,product.getIcon(),R.drawable.blackplaceholder,holder.imageImv);

        listeners(holder,position);
    }

    public void listeners(final ProductCartItemViewHolder holder, final int position){
        final Product product = productArrayList.get(position) ;
        holder.removeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showConfirmRemoveDialog(position);

            }
        });
        holder.increaseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (product.getQuantityCart()  < product.getQuantity()) {
                    product.setQuantityCart((product.getQuantityCart() + 1));

                    holder.quantityViewTxv.setText(product.getQuantityCart() + "");
                    RealmController.updateProduct(product);
                    productArrayList.set(position, product);

                    double productsPrice = Utility.getTotalPrice(productArrayList);

                    SubCartActivity.totalPriceTxv.setText(Utility.setPrice(activity, productsPrice));
                    SubCartActivity.totalCostTxv.setText(Utility.setPrice(activity, productsPrice + store.getFees()));
                }else
                    Dialogs.showToast(activity.getString(R.string.quantity_max),activity);
            }
        });
        holder.decreaseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( product.getQuantityCart() == 1 )
                    showConfirmRemoveDialog(position);
                else {
                    product.setQuantityCart((product.getQuantityCart() - 1));
                    holder.quantityViewTxv.setText(product.getQuantityCart() + "");
                    RealmController.updateProduct(product);
                    productArrayList.set(position, product);

                    double productsPrice = Utility.getTotalPrice(productArrayList);
                    SubCartActivity.totalPriceTxv.setText(Utility.setPrice(activity, productsPrice));
                    SubCartActivity.totalCostTxv.setText(Utility.setPrice(activity, productsPrice + store.getFees() ));
                }
            }
        });
    }

    public void showConfirmRemoveDialog(final int position){
        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_confirm_remove);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Button okBtn = (  Button) dialog.findViewById(R.id.ok_btn);
        Button cancelBtn = (  Button) dialog.findViewById(R.id.cancel_btn);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RealmController.removeFromCart(productArrayList.get(position).getId());
                productArrayList.remove(position);
                notifyDataSetChanged();

                double productsPrice = Utility.getTotalPrice(productArrayList);
                SubCartActivity.totalPriceTxv.setText(Utility.setPrice(activity, productsPrice ));
                SubCartActivity.totalCostTxv.setText(Utility.setPrice(activity, (productsPrice + store.getFees()) ));

                if ( productArrayList.size() == 0 ){
                    activity.finish();
                }
                dialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }


}
