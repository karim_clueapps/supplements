package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.AdsImagesActivity;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Ad;

import java.util.ArrayList;

/**
 * Created by mac on 5/9/2017 AD.
 */

public class AdsAdapter extends PagerAdapter {

    Activity activity;
    ArrayList<Ad> ads;

    public AdsAdapter(Activity activity, ArrayList<Ad> ads){
        this.activity=activity;
        this.ads = ads;
    }

    @Override
    public int getCount() {
        return ads.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public Object instantiateItem(ViewGroup container,final int position) {
        LayoutInflater mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = mLayoutInflater.inflate(R.layout.row_ad, container, false);

        ImageView adImv = (ImageView) v.findViewById(R.id.ad_imv);

        Utility.downloadImage(activity,ads.get(position).getImage(), R.drawable.ad_banner_space,adImv);

        adImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle= new Bundle();
                bundle.putParcelableArrayList("images" , ads );
                bundle.putInt("position" , position);
                IntentClass.goToActivity(activity , AdsImagesActivity.class, bundle);
            }
        });

        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        container.removeView((RelativeLayout) obj);
    }
}
