package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.clueapps.supplements.API.AddressAPI;
import com.clueapps.supplements.Holders.AddressViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Address.AddressesActivity;
import com.clueapps.supplements.activities.Cart.ShippingActivity;
import com.clueapps.supplements.activities.Address.EditAddressActivity;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Address;

import java.util.ArrayList;

/**
 * Created by mac on 5/18/2017 AD.
 */

public class AddressesAdapter extends RecyclerView.Adapter<AddressViewHolder> {

    Activity activity;
    ArrayList<Address> addressArrayList;

    public AddressesAdapter(Activity activity, ArrayList<Address> addressArrayList){
        this.activity=activity;
        this.addressArrayList = addressArrayList;
    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_address, parent, false);
        return new AddressViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AddressViewHolder holder, final int position) {
        holder.cityTxv.setText(addressArrayList.get(position).getCity());
        holder.countryTxv.setText(addressArrayList.get(position).getCountry());
        holder.blockTxv.setText(addressArrayList.get(position).getBlock());
        holder.buildingTxv.setText(addressArrayList.get(position).getBuildingNum());
        holder.floorTxv.setText(addressArrayList.get(position).getFloorNum());
        holder.streetTxv.setText(addressArrayList.get(position).getStreet());


        holder.closeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showConfirmRemoveDialog(position);

            }
        });

        holder.editLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("address", addressArrayList.get(position));
                IntentClass.goToActivity(activity,EditAddressActivity.class,bundle);
            }
        });

        holder.chooseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( AddressesActivity.fromCart ){
                    Address address = addressArrayList.get(position) ;
                    String addressStr = Utility.setAddress(address);

                    ShippingActivity.address = address;
                    ShippingActivity.address.setFullAddress(addressStr);
                    ShippingActivity.addressTxv.setText( addressStr);
                    AddressesActivity.fromCart =false ;
                    activity.finish();
                }
            }
        });


    }
    public void showConfirmRemoveDialog(final int position){
        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_confirm_remove);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Button okBtn = (  Button) dialog.findViewById(R.id.ok_btn);
        Button cancelBtn = (  Button) dialog.findViewById(R.id.cancel_btn);
        TextView title = (TextView) dialog.findViewById(R.id.dialoge_title_txt);
        title.setText(activity.getString(R.string.remove_address_msg));
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddressAPI.removeAddress(activity,addressArrayList.get(position).getId());
                addressArrayList.remove(position);
                notifyDataSetChanged();
                dialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public int getItemCount() {
        return addressArrayList.size();
    }


}
