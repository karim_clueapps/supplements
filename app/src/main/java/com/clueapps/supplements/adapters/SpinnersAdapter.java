package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.clueapps.supplements.R;

import java.util.List;

/**
 * Created by mac on 5/9/2017 AD.
 */

public class SpinnersAdapter extends BaseAdapter {
    List<String> groupList;
    Activity activity;
    int textColor ;
    public SpinnersAdapter(List<String> groupList, Activity activity, int textColor) {
        this.groupList = groupList;
        this.activity = activity;
        this.textColor = textColor ;
    }

    @Override
    public int getCount() {
        return groupList.size();
    }

    @Override
    public Object getItem(int position) {
        return groupList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = activity.getLayoutInflater().inflate(R.layout.item_spinner, parent, false);
        TextView textView = (TextView) view.findViewById(R.id.spinner_txv);
        textView.setText(groupList.get(position));
        textView.setTextColor(activity.getResources().getColor(textColor));
        return view;
    }


}

