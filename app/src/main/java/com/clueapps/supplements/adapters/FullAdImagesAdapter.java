package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.clueapps.supplements.R;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Ad;
import com.clueapps.supplements.models.Data.ImagePath;

import java.util.ArrayList;

/**
 * Created by as on 6/28/2017.
 */

public class FullAdImagesAdapter extends PagerAdapter {

    Activity activity;
    ArrayList<Ad> images;

    public FullAdImagesAdapter(Activity activity, ArrayList<Ad> images){
        this.activity=activity;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = mLayoutInflater.inflate(R.layout.row_ad_image, container, false);

        ImageView adImv = (ImageView) v.findViewById(R.id.image_imv);

        Utility.downloadImage(activity, images.get(position).getImage(), R.drawable.blackplaceholder,adImv);

        container.addView(v);
        return v;

    }

    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        container.removeView((RelativeLayout) obj);
    }
}
