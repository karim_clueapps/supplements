package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.clueapps.supplements.R;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.ImagePath;

import java.util.ArrayList;

/**
 * Created by as on 6/28/2017.
 */

public class ImagesAdapter extends PagerAdapter {

    Activity activity;
    ArrayList<ImagePath> images;

    public onClickListener onClickListener;
    public ImagesAdapter(Activity activity, ArrayList<ImagePath> images){
        this.activity=activity;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = mLayoutInflater.inflate(R.layout.row_image, container, false);

        ImageView adImv = (ImageView) v.findViewById(R.id.image_imv);

        Utility.downloadImage(activity, images.get(position).getPath(), R.drawable.blackplaceholder,adImv);

        if ( new SharedPref(activity).getLang().equals("ar")) {
            adImv.setRotation(180);
        }
        adImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClick(position);
            }
        });
        container.addView(v);
        return v;

    }

    public interface onClickListener
    {
        void onClick(int position);
    }

    public void setOnClickListener(onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        container.removeView((RelativeLayout) obj);
    }
}
