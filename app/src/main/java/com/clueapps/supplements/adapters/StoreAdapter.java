package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clueapps.supplements.Holders.StoreViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Product.ProductsActivity;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Category;
import com.clueapps.supplements.models.Data.Store;

import java.util.ArrayList;

/**
 * Created by mac on 5/18/2017 AD.
 */

public class StoreAdapter extends RecyclerView.Adapter<StoreViewHolder> {
    Activity activity;
    ArrayList<Store> storeArrayList;
    Category category;
    public StoreAdapter(Activity activity, ArrayList<Store> storeArrayList, Category category ){
        this.activity=activity;
        this.storeArrayList = storeArrayList;
        this.category = category ;
    }
    @Override
    public StoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_store, parent, false);
        return new StoreViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StoreViewHolder holder, final int position) {

        if ( position == 0 ){
            holder.imageImv.setImageResource(R.drawable.rounded_rectangle_black);
            holder.allTxv.setVisibility(View.VISIBLE);
        }
        else {
            Utility.downloadImage(activity, storeArrayList.get(position).getImage(), R.drawable.shopplaceholder, holder.imageImv);
            holder.nameTxv.setText(storeArrayList.get(position).getName());
            holder.allTxv.setVisibility(View.GONE);
        }

        holder.openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();

                bundle.putParcelable("category", category);
                if ( position != 0 )
                    bundle.putParcelable("store", storeArrayList.get(position));

                IntentClass.goToActivity(activity, ProductsActivity.class,bundle);
            }
        });

    }

    @Override
    public int getItemCount() {
        return storeArrayList.size();
    }


}
