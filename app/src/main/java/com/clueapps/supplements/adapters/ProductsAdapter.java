package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.clueapps.supplements.API.ProductAPI;
import com.clueapps.supplements.Holders.ProductViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Product.ProductActivity;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.realm.RealmController;

import java.util.ArrayList;

/**
 * Created by mac on 5/18/2017 AD.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductViewHolder> {
    Activity activity;
    ArrayList<Product> productArrayList;
    boolean isInWishList ;
    public ProductsAdapter(Activity activity, ArrayList<Product> productArrayList,boolean isInWishList){
        this.activity=activity;
        this.productArrayList = productArrayList;
        this.isInWishList = isInWishList ;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_product, parent, false);
        return new ProductViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position) {
        Product product = productArrayList.get(position);
        listeners(holder,position);

        holder.nameTxv.setText(product.getName());
        holder.weightTxv.setText(Utility.setText(activity,product.getWeight(), activity.getString(R.string.kg)));
        holder.priceTxv.setText(Utility.setPrice( activity,product.getPrice()));
        holder.storeTxv.setText(product.getStore().getName());

        if ( product.getQuantity() <= 0 ) {
            holder.addCartBtn.setEnabled(false);
            holder.soldOutImv.setVisibility(View.VISIBLE);
        }

        if ( RealmController.getProduct(product.getId()) == null ){
            holder.addCartBtn.setVisibility(View.VISIBLE);
            holder.changeQuantityLayout.setVisibility(View.GONE);
        }
        else{
            holder.addCartBtn.setVisibility(View.GONE);
            holder.changeQuantityLayout.setVisibility(View.VISIBLE);
            Product product1 = RealmController.getProduct(product.getId());
            holder.quantityViewTxv.setText(product1.getQuantityCart()+"");
        }

        if ( isInWishList || product.isWishlisted()){
            holder.favImv.setImageResource(R.drawable.hearticonselected);
        }else
            holder.favImv.setImageResource(R.drawable.hearticonnotselected);

        if ( product.getPercentage() > 0 )
            holder.offerImv.setVisibility(View.VISIBLE);

         Utility.downloadImage(activity,product.getIcon(),R.drawable.blackplaceholder,holder.imageImv);



    }

    public void listeners(final ProductViewHolder holder, final int position) {
        final Product product = productArrayList.get(position);
        if (RealmController.getProduct(product.getId()) != null ){
            product.setQuantityCart(RealmController.getProduct(product.getId()).getQuantityCart());
            productArrayList.set(position, product);
        }

        holder.addCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product.setQuantityCart(1);
                RealmController.addToCart(product);
                holder.addCartBtn.setVisibility(View.GONE);
                holder.changeQuantityLayout.setVisibility(View.VISIBLE);
                holder.quantityViewTxv.setText(1 +"");
                CustomHeader.changeCartNum(activity);
            }
        });

        holder.favImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( new SharedPref(activity).getToken().length() > 0 ) {
                    Product product = productArrayList.get(position);

                    if (!product.isWishlisted()) {
                        holder.favImv.setImageResource(R.drawable.hearticonselected);
                        ProductAPI.addToWishlist(activity, product.getId());
                    } else {

                        holder.favImv.setImageResource(R.drawable.hearticonnotselected);
                        ProductAPI.removeFromWishlist(activity, product.getId());
                    }

                    product.setWishlisted(!product.isWishlisted());
                    productArrayList.set(position, product);

                    if (isInWishList) {
                        productArrayList.remove(position);
                        notifyDataSetChanged();
                    }
                }else {
                   Dialogs.showLoginDialog(activity);
                }
            }
        });

        holder.increaseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if (product.getQuantityCart() < product.getQuantity()) {

                product.setQuantityCart((product.getQuantityCart() + 1));
                holder.quantityViewTxv.setText(product.getQuantityCart() + "");
                RealmController.updateProduct(product);
                productArrayList.set(position, product);
                CustomHeader.changeCartNum(activity);
            }else
                Dialogs.showToast(activity.getString(R.string.quantity_max),activity);
            }
        });

        holder.decreaseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( product.getQuantityCart() == 1 )
                    showConfirmRemoveDialog(position,holder);
                else {
                    product.setQuantityCart((product.getQuantityCart() - 1));
                    holder.quantityViewTxv.setText(product.getQuantityCart() + "");
                    RealmController.updateProduct(product);
                    productArrayList.set(position, product);
                }

                CustomHeader.changeCartNum(activity);
            }
        });


        holder.openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("product",productArrayList.get(position));
              //  ProductActivity productActivity = new ProductActivity(productArrayList.get(position));
                IntentClass.goToResultActivity(activity, ProductActivity.class,1233,bundle);
            }
        });

    }

    public void showConfirmRemoveDialog(final int position , final  ProductViewHolder holder){
        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_confirm_remove);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Button okBtn = (  Button) dialog.findViewById(R.id.ok_btn);
        Button cancelBtn = (  Button) dialog.findViewById(R.id.cancel_btn);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RealmController.removeFromCart(productArrayList.get(position).getId());
                holder.addCartBtn.setVisibility(View.VISIBLE);
                holder.changeQuantityLayout.setVisibility(View.GONE);
                dialog.dismiss();
                CustomHeader.changeCartNum(activity);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }


    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    public void UpdateItem(Product item) {
        for (int i = 0; i < productArrayList.size(); i++) {
            if (productArrayList.get(i).getId().equals(item.getId())) {
                productArrayList.set(i, item);
                notifyItemChanged(i);
            }
        }
    }

}
