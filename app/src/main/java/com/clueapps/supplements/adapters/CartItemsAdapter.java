package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.clueapps.supplements.Holders.CartStoreViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Cart.CartActivity;
import com.clueapps.supplements.activities.Cart.SubCartActivity;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.realm.RealmController;

import java.util.ArrayList;

/**
 * Created by mac on 5/18/2017 AD.
 */

public class CartItemsAdapter extends RecyclerView.Adapter<CartStoreViewHolder> {
    Activity activity;
    ArrayList<Product> productArrayList;

    public CartItemsAdapter(Activity activity, ArrayList<Product> productArrayList){
        this.activity=activity;
        this.productArrayList = productArrayList;
    }

    @Override
    public CartStoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_cart_item, parent, false);
        return new CartStoreViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CartStoreViewHolder holder, final int position) {
        Product product = productArrayList.get(position);
        holder.nameTxv.setText(product.getStore().getName());
        holder.priceTxv.setText(Utility.setPrice(activity,product.getPrice() * product.getQuantityCart()));
        holder.quantityTxv.setText(Utility.setText(activity,product.getQuantityCart()+"" ,activity.getString(R.string.pieces)));

        Utility.downloadImage(activity,product.getStore().getImage(),R.drawable.blackplaceholder,holder.imageImv);

        listeners(holder,position);

    }
    public void listeners(final CartStoreViewHolder holder, final int position){
        final Product product = productArrayList.get(position);
        holder.openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("store" , product.getStore() );
                IntentClass.goToActivity(activity, SubCartActivity.class,bundle);
            }
        });
        holder.removeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showConfirmRemoveDialog(position);
            }
        });
    }

    public void showConfirmRemoveDialog(final int position){
        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_confirm_remove);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Button okBtn = (  Button) dialog.findViewById(R.id.ok_btn);
        Button cancelBtn = (  Button) dialog.findViewById(R.id.cancel_btn);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RealmController.removeFromStore(productArrayList.get(position).getStore().getId());
                productArrayList.remove(position);
                notifyDataSetChanged();

                if ( productArrayList.size() == 0 ){
                    CartActivity.cartEmptyLayout.setVisibility(View.VISIBLE);
                    CartActivity.productsRecycler.setVisibility(View.GONE);
                }

                dialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }


}
