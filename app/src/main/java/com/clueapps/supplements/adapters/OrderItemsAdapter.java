package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clueapps.supplements.Holders.OrderItemViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Product;

import java.util.ArrayList;

/**
 * Created by mac on 5/18/2017 AD.
 */

public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemViewHolder> {
    Activity activity;
    ArrayList<Product> productArrayList;

    public OrderItemsAdapter(Activity activity, ArrayList<Product> productArrayList){
        this.activity=activity;
        this.productArrayList = productArrayList;
    }

    @Override
    public OrderItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_order_item, parent, false);
        return new OrderItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final OrderItemViewHolder holder, final int position) {
        Product product = productArrayList.get(position);
        holder.nameTxv.setText(product.getName());
        holder.priceTxv.setText(Utility.setPrice(activity,product.getPrice()));
        holder.quantityTxv.setText(Utility.setText(activity,product.getQuantity() +"" ,activity.getString(R.string.pieces)));

        if ( product.getQuantityCart() > 0 )
            holder.quantityTxv.setText(Utility.setText(activity,product.getQuantityCart() +"" ,activity.getString(R.string.pieces)));


        holder.weightTxv.setText(Utility.setText(activity,product.getWeight()+"" ,activity.getString(R.string.kg)));

        Utility.downloadImage(activity,product.getIcon(),R.drawable.blackplaceholder,holder.imageImv);

    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }


}
