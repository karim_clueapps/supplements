package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clueapps.supplements.Holders.CategoryViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Product.StoresActivity;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Category;

import java.util.ArrayList;

/**
 * Created by mac on 5/18/2017 AD.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {
    Activity activity;
    ArrayList<Category> categoryArrayList;

    public CategoryAdapter(Activity activity, ArrayList<Category> categoryArrayList){
        this.activity=activity;
          this.categoryArrayList = categoryArrayList;
    }
    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_category, parent, false);
        return new CategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, final int position) {
        holder.nameTxv.setText(categoryArrayList.get(position).getName());
        holder.numItemsTxv.setText(Utility.setText(activity,categoryArrayList.get(position).getNumItems()+"" , activity.getString(R.string.items)));
        Utility.downloadImage(activity,categoryArrayList.get(position).getImage(),R.drawable.blackplaceholder,holder.imageImv);

        holder.openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("category",categoryArrayList.get(position));
                IntentClass.goToActivity(activity, StoresActivity.class,bundle);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryArrayList.size();
    }


}
