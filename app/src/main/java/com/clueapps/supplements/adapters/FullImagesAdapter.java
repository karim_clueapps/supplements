package com.clueapps.supplements.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.clueapps.supplements.Holders.ImageViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.ImagePath;

import java.util.ArrayList;

/**
 * Created by as on 6/28/2017.
 */

public class FullImagesAdapter extends RecyclerView.Adapter<ImageViewHolder> {

    Activity activity;
    ArrayList<ImagePath> images;
    ImageView imgImv;

    public FullImagesAdapter(Activity activity, ArrayList<ImagePath> images, ImageView imgImv) {
        this.activity = activity;
        this.images = images;
        this.imgImv = imgImv;
    }


    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate a new card view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_full_screen_image, parent, false);
        return new ImageViewHolder(view);
    }

    // replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ImageViewHolder holder, final int position) {

        Utility.downloadImage(activity, images.get(position).getPath(), R.drawable.blackplaceholder, holder.imageImv);

        holder.imageImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.downloadImage(activity, images.get(position).getPath(), R.drawable.blackplaceholder, imgImv);

            }
        });

    }

    @Override
    public int getItemCount() {
        if (images == null)
            return 0;
        return images.size();
    }
}
