package com.clueapps.supplements.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.clueapps.supplements.helpers.Fonts;

public class CustomButtonBold extends android.support.v7.widget.AppCompatButton {


    public CustomButtonBold(Context context) {
        super(context);
        this.setTypeface(Fonts.boldTypeface(context));


    }

    public CustomButtonBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Fonts.boldTypeface(context));

    }

    public CustomButtonBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(Fonts.boldTypeface(context));

    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);


    }
}
