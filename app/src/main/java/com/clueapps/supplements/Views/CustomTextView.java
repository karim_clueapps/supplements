package com.clueapps.supplements.Views;


import android.content.Context;
import android.util.AttributeSet;

import com.clueapps.supplements.helpers.Fonts;


public class CustomTextView extends android.support.v7.widget.AppCompatTextView {
    public CustomTextView(Context context) {
        super(context);
        this.setTypeface(Fonts.regTypeface(context));


    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Fonts.regTypeface(context));

    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(Fonts.regTypeface(context));

    }

}

