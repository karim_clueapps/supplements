package com.clueapps.supplements.Views;


import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.clueapps.supplements.helpers.Fonts;


public class CustomEditText extends android.support.v7.widget.AppCompatEditText {
    public CustomEditText(Context context) {
        super(context);

        this.setTypeface(Fonts.regTypeface(context));


    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Fonts.regTypeface(context));
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(Fonts.regTypeface(context));
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);

    }
}


