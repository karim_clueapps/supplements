package com.clueapps.supplements.Views;

import android.content.Context;
import android.util.AttributeSet;

import com.clueapps.supplements.helpers.Fonts;

/**
 * Created by as on 5/9/2017.
 */

public class CustomTextViewLight extends android.support.v7.widget.AppCompatTextView {
    public CustomTextViewLight(Context context) {
        super(context);
        this.setTypeface(Fonts.lightTypeface(context));


    }

    public CustomTextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Fonts.lightTypeface(context));

    }

    public CustomTextViewLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(Fonts.lightTypeface(context));

    }

}

