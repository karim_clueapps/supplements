package com.clueapps.supplements.Views;


import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.clueapps.supplements.helpers.Fonts;


public class CustomTextInputEditText extends android.support.design.widget.TextInputEditText {

    public CustomTextInputEditText(Context context) {
        super(context);

        this.setTypeface(Fonts.regTypeface(context));


    }

    public CustomTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Fonts.regTypeface(context));
    }

    public CustomTextInputEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(Fonts.regTypeface(context));
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);

    }
}


