package com.clueapps.supplements.Views;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

import com.clueapps.supplements.helpers.Fonts;

/**
 * Created by m on 8/24/2017.
 */

public class CustomTextInputLayout extends TextInputLayout {

    public CustomTextInputLayout(Context context) {
        super(context);
        this.setTypeface(Fonts.regTypeface(context));
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Fonts.regTypeface(context));

    }

    public CustomTextInputLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(Fonts.regTypeface(context));
    }
}
