package com.clueapps.supplements.realm;


import android.app.Application;

import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.models.Data.ProductRealm;
import com.clueapps.supplements.models.Data.Store;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;


/**
 * <h1>Database on device</h1>
 * RealmController class implement methods that insert,get,delete and update in database
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class RealmController {

    public static  Realm realm= Realm.getDefaultInstance();

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    /**
     * This method is used to get stores.
     * called when SubCartActivity opened.
     * @return result stores.
     */
    public static  ArrayList<Product> getStores() {
        ArrayList<Product> tmpProductArrayList = new ArrayList<>();

        List<ProductRealm> productRealms = realm.where(ProductRealm.class).findAll();
        for (ProductRealm productRealm : productRealms){
            tmpProductArrayList.add(convertProduct(productRealm));
        }
        ArrayList<Product> productArrayList = new ArrayList<>();

        for (Product product : tmpProductArrayList){
            int pos = Utility.searchStoreId(productArrayList,product.getStore().getId()) ;
            if ( pos == -1 ) {
                productArrayList.add(product);
            }
            else{
                int quantity = productArrayList.get(pos).getQuantityCart()+ product.getQuantityCart();
                product.setQuantityCart(quantity);
                productArrayList.set(pos,product);
            }

        }

        return productArrayList;
    }
    /**
     * This method is used to get count of products in cart.
     * called when cart in view.
     * @return result products number.
     */
    public static  int getProductsNumber() {
        int size = 0 ;
        List<ProductRealm> productRealms = realm.where(ProductRealm.class).findAll();
        for (ProductRealm productRealm : productRealms){
            size += productRealm.getQuantityCart() ;
        }
        return size;
    }

    /**
     * This method is used to get specific products in store.
     * called when .
     * @param storeId of store
     * @return result products of specific store.
     */
    public static  ArrayList<Product> getStoreProducts(String storeId) {
        ArrayList<Product> productArrayList = new ArrayList<>();

        List<ProductRealm> productRealms = realm.where(ProductRealm.class).equalTo("storeId", storeId).findAll();
        for (ProductRealm productRealm : productRealms){
            productArrayList.add(convertProduct(productRealm));
        }
        return productArrayList;
    }

    /**
     * This method is used to get specific product.
     * called when.
     * @param productId of product
     * @return result products of specific store.
     */
    public static Product getProduct(String productId) {
        ProductRealm productRealm = realm.where(ProductRealm.class).equalTo("id", productId).findFirst();

        if ( productRealm == null )
            return null;


        Product product = convertProduct(productRealm);
        return product;
    }
    /**
     * This method is used to add product in cart.
     * called when user click add to cart button.
     * @param product to add it in cart
     */
    public static void addToCart(Product product){

        realm.beginTransaction();
        ProductRealm productRealm = realm.createObject(ProductRealm.class);
        productRealm.setId(product.getId());
        productRealm.setName(product.getName());
        productRealm.setIcon(product.getIcon());
        productRealm.setPrice(product.getPrice());
        productRealm.setDescription(product.getDescription());
        productRealm.setQuantity(product.getQuantity());
        productRealm.setQuantityCart(product.getQuantityCart());
      //  productRealm.setBrand(product.getBrand());
        productRealm.setWeight(product.getWeight());
        productRealm.setStoreId(product.getStore().getId());
        productRealm.setStoreName(product.getStore().getName());
        productRealm.setStoreFees(product.getStore().getFees());
        productRealm.setStoreImg(product.getStore().getImage() != null ? product.getStore().getImage() : "https://supplementhunt.com/images/strength/merch-02.png");

        realm.commitTransaction();

    }
    /**
     * This method is used to update product quantity in cart.
     * called when user change quantity.
     * @param product to update it in cart
     */
    public static void updateProduct(Product product){
        Realm realm = Realm.getDefaultInstance();


        realm.beginTransaction();

        ProductRealm productRealm = realm.where(ProductRealm.class)
                .equalTo("id", product.getId()).findFirst();

        if ( product.getQuantityCart() != 0 )
            productRealm.setQuantityCart(product.getQuantityCart());


        realm.commitTransaction();

    }
    /**
     * This method is used to remove all products in specific store.
     * called when user remove store.
     * @param storeId to remove all product
     */
    public static void removeFromStore(String storeId) {

        realm.beginTransaction();
        List<ProductRealm> result = realm.where(ProductRealm.class).equalTo("storeId", storeId).findAll();

        while(result.size() > 0 ) {
            result.get(0).removeFromRealm();
        }

        realm.commitTransaction();

    }
    /**
     * This method is used to remove all products in cart.
     * called when user all products (notes : not used).
     */
    public static void clearAll() {

        realm.beginTransaction();
        realm.clear(ProductRealm.class);
        realm.commitTransaction();
    }

    /**
     * This method is used to remove specific product.
     * called when user remove product.
     * @param productId of product
     */
    public static void removeFromCart(String productId) {

        realm.beginTransaction();
        ProductRealm result = realm.where(ProductRealm.class).equalTo("id", productId).findFirst();
        if ( result != null )
            result.removeFromRealm();
        realm.commitTransaction();

    }

    /**
     * This method is used to convert result of database to product .
     * called when get products from database  .
     * @param productRealm the result from database
     * @return product.
     */
    public static Product convertProduct(ProductRealm productRealm){
        Product product = new Product();
        product.setId(productRealm.getId());
        product.setName(productRealm.getName());
        product.setIcon(productRealm.getIcon());
        product.setPrice(productRealm.getPrice());
     //   product.setBrand(productRealm.getBrand());
        product.setWeight(productRealm.getWeight());
        product.setQuantityCart(productRealm.getQuantityCart());
        product.setQuantity(productRealm.getQuantity());
        Store store = new Store();
        store.setId(productRealm.getStoreId());
        store.setName(productRealm.getStoreName());
        store.setImage(productRealm.getStoreImg());
        store.setFees(productRealm.getStoreFees());
        product.setStore(store);

        return product;
    }

}
