package com.clueapps.supplements.models.responses;

import com.clueapps.supplements.models.Data.Order;
import com.clueapps.supplements.models.Data.Product;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class OrdersResponse {

    @SerializedName("orders")
    @Expose
    private ArrayList<Order> orderArrayList;

    public ArrayList<Order> getOrderArrayList() {
        return orderArrayList;
    }
}