package com.clueapps.supplements.models.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by m on 10/17/2017.
 */

public class ImagePath implements Parcelable{

    @SerializedName("path")
    @Expose
    String path ;

    public ImagePath(String path){
        this.path = path;
    }
    protected ImagePath(Parcel in) {
        path = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(path);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ImagePath> CREATOR = new Creator<ImagePath>() {
        @Override
        public ImagePath createFromParcel(Parcel in) {
            return new ImagePath(in);
        }

        @Override
        public ImagePath[] newArray(int size) {
            return new ImagePath[size];
        }
    };

    public String getPath() {
        return path;
    }
}
