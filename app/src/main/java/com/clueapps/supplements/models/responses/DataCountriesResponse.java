package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DataCountriesResponse {

    @SerializedName("data")
    @Expose
    private CountriesResponse countriesResponse;

    public CountriesResponse getCountriesResponse() {
        return countriesResponse;
    }
}