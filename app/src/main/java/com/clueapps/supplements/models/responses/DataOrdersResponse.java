package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DataOrdersResponse {

    @SerializedName("data")
    @Expose
    private OrdersResponse ordersResponse;

    @SerializedName("paginator")
    @Expose
    private Paginator paginator;

    public Paginator getPaginator() {
        return paginator;
    }

    public OrdersResponse getOrdersResponse() {
        return ordersResponse;
    }
}