package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Paginator {

    @SerializedName("page")
    @Expose
    private int page;


    @SerializedName("next")
    @Expose
    private String next;


    public int getPage() {
        return page;
    }

    public String getNext() {
        return next;
    }
}