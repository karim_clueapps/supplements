package com.clueapps.supplements.models.responses;

import com.clueapps.supplements.models.Data.Ad;
import com.clueapps.supplements.models.Data.Category;
import com.clueapps.supplements.models.Data.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class CategoriesResponse {

    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categoryArrayList;

    @SerializedName("ads")
    @Expose
    private ArrayList<Ad> adArrayList;

    public ArrayList<Ad> getAdArrayList() {
        return adArrayList;
    }

    public ArrayList<Category> getCategoryArrayList() {
        return categoryArrayList;
    }


}