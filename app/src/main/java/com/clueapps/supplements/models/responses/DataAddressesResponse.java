package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DataAddressesResponse {

    @SerializedName("data")
    @Expose
    private AddressesResponse addressesResponse;

    @SerializedName("paginator")
    @Expose
    private Paginator paginator;

    public AddressesResponse getAddressesResponse() {
        return addressesResponse;
    }

    public Paginator getPaginator() {
        return paginator;
    }


}