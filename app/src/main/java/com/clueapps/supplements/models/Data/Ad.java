package com.clueapps.supplements.models.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by m on 9/19/2017.
 */

public class Ad  implements Parcelable {

    @SerializedName("link")
    @Expose
    private String link;

    @SerializedName("image_url")
    @Expose
    private String image;

    protected Ad(Parcel in) {
        link = in.readString();
        image = in.readString();
    }

    public static final Creator<Ad> CREATOR = new Creator<Ad>() {
        @Override
        public Ad createFromParcel(Parcel in) {
            return new Ad(in);
        }

        @Override
        public Ad[] newArray(int size) {
            return new Ad[size];
        }
    };

    public String getLink() {
        return link;
    }

    public String getImage() {
        return image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(link);
        parcel.writeString(image);
    }
}
