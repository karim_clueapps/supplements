package com.clueapps.supplements.models.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by m on 9/19/2017.
 */

public class Product implements Parcelable {


    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("icon")
    @Expose
    private String icon;

    @SerializedName("images")
    @Expose
    private ArrayList<ImagePath> images;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("weight")
    @Expose
    private String weight;

    @SerializedName("price")
    @Expose
    private double price;


    @SerializedName("brand")
    @Expose
    private String brand;

    @SerializedName("store")
    @Expose
    private Store store;

    @SerializedName("quantity")
    @Expose
    private int quantity;

    private int quantityCart;

    @SerializedName("expiry_date")
    @Expose
    private String expDate;


    @SerializedName("usage_age")
    @Expose
    private String usageAge;

   @SerializedName("size")
    @Expose
    private String size;

    @SerializedName("discount")
    @Expose
    private String discount;

    @SerializedName("wishlisted")
    @Expose
    private boolean isWishlisted;

    @SerializedName("percentage_off")
    @Expose
    private int percentage;

    public Product(){}

    protected Product(Parcel in) {
        id = in.readString();
        icon = in.readString();
        images = in.createTypedArrayList(ImagePath.CREATOR);
        name = in.readString();
        description = in.readString();
        weight = in.readString();
        price = in.readDouble();
        brand = in.readString();
        store = in.readParcelable(Store.class.getClassLoader());
        quantity = in.readInt();
        quantityCart = in.readInt();
        expDate = in.readString();
        usageAge = in.readString();
        size = in.readString();
        discount = in.readString();
        isWishlisted = in.readByte() != 0;
        percentage = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(icon);
        dest.writeTypedList(images);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(weight);
        dest.writeDouble(price);
        dest.writeString(brand);
        dest.writeParcelable(store, flags);
        dest.writeInt(quantity);
        dest.writeInt(quantityCart);
        dest.writeString(expDate);
        dest.writeString(usageAge);
        dest.writeString(size);
        dest.writeString(discount);
        dest.writeByte((byte) (isWishlisted ? 1 : 0));
        dest.writeInt(percentage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getQuantityCart() {
        return quantityCart;
    }

    public void setQuantityCart(int quantityCart) {
        this.quantityCart = quantityCart;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }



    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUsageAge() {
        return usageAge;
    }

    public void setUsageAge(String usageAge) {
        this.usageAge = usageAge;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public boolean isWishlisted() {
        return isWishlisted;
    }

    public void setWishlisted(boolean wishlisted) {
        isWishlisted = wishlisted;
    }


    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public ArrayList<ImagePath> getImages() {
        return images;
    }

    public void setImages(ArrayList<ImagePath> images) {
        this.images = images;
    }
}
