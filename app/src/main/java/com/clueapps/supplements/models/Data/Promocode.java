package com.clueapps.supplements.models.Data;

/**
 * Created by as on 6/20/2018.
 */

public class Promocode {


    private int id;
    private String code;
    private int discount;
    private boolean active;
    private String created_at;
    private String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getDiscount() {
        return discount;
    }

}
