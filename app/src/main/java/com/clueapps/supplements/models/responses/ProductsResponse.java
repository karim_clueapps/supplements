package com.clueapps.supplements.models.responses;

import com.clueapps.supplements.models.Data.Category;
import com.clueapps.supplements.models.Data.Product;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class ProductsResponse {

    @SerializedName("products")
    @Expose
    private ArrayList<Product> productArrayList;

    public ArrayList<Product> getProductArrayList() {
        return productArrayList;
    }
}