package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by m on 10/12/2017.
 */

public class ErrorResponse {

    @SerializedName("error")
    public ErrorBody errorBody;

    public ErrorBody getErrorBody() {
        return errorBody;
    }
}
