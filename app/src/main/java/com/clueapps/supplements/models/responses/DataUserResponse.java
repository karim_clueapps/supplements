package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DataUserResponse {



    @SerializedName("data")
    @Expose
    private UserResponse userResponse;


    @SerializedName("message")
    @Expose
    private String message;

    public UserResponse getUserResponse() {
        return userResponse;
    }

    public String getMessage() {
        return message;
    }
}