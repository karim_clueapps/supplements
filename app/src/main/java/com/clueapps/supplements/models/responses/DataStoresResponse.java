package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DataStoresResponse {

    @SerializedName("data")
    @Expose
    private StoresResponse storesResponse;

    @SerializedName("paginator")
    @Expose
    private Paginator paginator;

    public Paginator getPaginator() {
        return paginator;
    }

    public StoresResponse getStoresResponse() {
        return storesResponse;
    }
}