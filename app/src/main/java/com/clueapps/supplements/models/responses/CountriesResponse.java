package com.clueapps.supplements.models.responses;

import com.clueapps.supplements.models.Data.Country;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CountriesResponse {

    @SerializedName("countries")
    @Expose
    private ArrayList<Country> countries;

    public ArrayList<Country> getCountries() {
        return countries;
    }
}