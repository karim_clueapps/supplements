package com.clueapps.supplements.models.Data;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Country {


    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("code")
    @Expose
    private String code;

    public String getCode() {
        return code;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}