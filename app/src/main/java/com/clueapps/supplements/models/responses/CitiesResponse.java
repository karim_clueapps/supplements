package com.clueapps.supplements.models.responses;

import com.clueapps.supplements.models.Data.City;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CitiesResponse {

    @SerializedName("cities")
    @Expose
    private ArrayList<City> cities;

    public ArrayList<City> getCities() {
        return cities;
    }
}