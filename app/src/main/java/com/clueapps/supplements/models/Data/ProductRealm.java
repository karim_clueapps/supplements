package com.clueapps.supplements.models.Data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * <h1>Product item module in database </h1>
 * ProductRealm class for hold food item in database
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-08-9
 */

public class ProductRealm extends RealmObject {

    @PrimaryKey
    private String id;

    private String icon;
    private String name;
    private String description;
    private String weight;
    private double price;
    private int quantity;
    private int quantityCart;
    private String expDate;
    private String brand;
    private String storeId;
    private String storeName;
    private String storeImg;
    private double storeFees;
    private String usageAge;
    private String size;
    private String discount;
    private boolean isWishlisted;
    private double fees;
    private int percentage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUsageAge() {
        return usageAge;
    }

    public void setUsageAge(String usageAge) {
        this.usageAge = usageAge;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public boolean isWishlisted() {
        return isWishlisted;
    }

    public void setWishlisted(boolean wishlisted) {
        isWishlisted = wishlisted;
    }

    public double getFees() {
        return fees;
    }

    public void setFees(double fees) {
        this.fees = fees;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreImg() {
        return storeImg;
    }

    public void setStoreImg(String storeImg) {
        this.storeImg = storeImg;
    }

    public double getStoreFees() {
        return storeFees;
    }

    public void setStoreFees(double storeFees) {
        this.storeFees = storeFees;
    }

    public void setQuantityCart(int quantityCart) {
        this.quantityCart = quantityCart;
    }

    public int getQuantityCart() {
        return quantityCart;
    }
}
