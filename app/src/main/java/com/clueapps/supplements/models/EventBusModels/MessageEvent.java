package com.clueapps.supplements.models.EventBusModels;

import com.google.firebase.messaging.RemoteMessage;

public class MessageEvent {
    RemoteMessage message;

    public MessageEvent(RemoteMessage message) {
        this.message = message;
    }

    public RemoteMessage getMessage() {
        return message;
    }
}
