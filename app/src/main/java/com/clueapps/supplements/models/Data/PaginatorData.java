package com.clueapps.supplements.models.Data;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * <h1>PaginatorData module</h1>
 * PaginatorData class for hold view of paging in api
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-08-9
 */
public class PaginatorData {


    public int page = 1;
    public boolean loading = false;
    public boolean empty = false;
    public SwipeRefreshLayout refreshLayout ;
    public LinearLayout noResultLayout ;
    public RecyclerView recyclerView ;

    public RelativeLayout progressRel;
    public RelativeLayout progressMoreDataRel ;
}