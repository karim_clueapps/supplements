package com.clueapps.supplements.models.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by m on 9/19/2017.
 */

public class Address implements Parcelable {


    @SerializedName("address_id")
    @Expose
    private String addressId;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("city_id")
    @Expose
    private String cityId;

    @SerializedName("country_id")
    @Expose
    private String countryId;


    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("block")
    @Expose
    private String block;

    @SerializedName("street")
    @Expose
    private String street;

    @SerializedName("building_no")
    @Expose
    private String buildingNum;


    @SerializedName("floor_no")
    @Expose
    private String floorNum;

    @SerializedName("name")
    @Expose
    private String name;

    private String fullAddress ;

    public Address(){}

    protected Address(Parcel in) {
        id = in.readString();
        cityId = in.readString();
        countryId = in.readString();
        city = in.readString();
        country = in.readString();
        block = in.readString();
        street = in.readString();
        buildingNum = in.readString();
        floorNum = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(cityId);
        dest.writeString(countryId);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(block);
        dest.writeString(street);
        dest.writeString(buildingNum);
        dest.writeString(floorNum);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNum() {
        return buildingNum;
    }

    public void setBuildingNum(String buildingNum) {
        this.buildingNum = buildingNum;
    }

    public String getFloorNum() {
        return floorNum;
    }

    public void setFloorNum(String floorNum) {
        this.floorNum = floorNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
