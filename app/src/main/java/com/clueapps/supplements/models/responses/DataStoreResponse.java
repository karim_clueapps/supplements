package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DataStoreResponse {

    @SerializedName("data")
    @Expose
    private ProductsResponse productsResponse;

    @SerializedName("paginator")
    @Expose
    private Paginator paginator;

    public Paginator getPaginator() {
        return paginator;
    }

    public ProductsResponse getProductsResponse() {
        return productsResponse;
    }
}