package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DataOrderResponse {

    @SerializedName("data")
    @Expose
    private OrderResponse orderResponse;

    public OrderResponse getOrderResponse() {
        return orderResponse;
    }
}