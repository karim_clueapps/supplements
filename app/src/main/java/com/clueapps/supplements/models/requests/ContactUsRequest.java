package com.clueapps.supplements.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactUsRequest {

    @SerializedName("email_address")
    @Expose
    private String email;

    @SerializedName("message")
    @Expose
    private String message;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
