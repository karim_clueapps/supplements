package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DataCitiesResponse {

    @SerializedName("data")
    @Expose
    private CitiesResponse citiesResponse;

    public CitiesResponse getCitiesResponse() {
        return citiesResponse;
    }
}