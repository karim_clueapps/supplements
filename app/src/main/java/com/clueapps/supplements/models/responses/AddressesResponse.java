package com.clueapps.supplements.models.responses;

import com.clueapps.supplements.models.Data.Address;
import com.clueapps.supplements.models.Data.Order;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class AddressesResponse {

    @SerializedName("addresses")
    @Expose
    private ArrayList<Address> addressArrayList;

    public ArrayList<Address> getAddressArrayList() {
        return addressArrayList;
    }
}