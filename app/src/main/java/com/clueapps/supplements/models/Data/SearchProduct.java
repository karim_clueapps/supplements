package com.clueapps.supplements.models.Data;

import com.clueapps.supplements.helpers.Utility;

/**
 * <h1>SearchProduct module </h1>
 * SearchProduct class for hold search data to api
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-08-9
 */
public class SearchProduct {

    String storeId ;
    String categoryId ;
    String keyword;
    double minPrice ;
    double maxPrice ;
    String orderBy ;

    public SearchProduct(){
        orderBy = Utility.LOW_TO_HIGH;
        maxPrice = 1000 ;
        minPrice = 0 ;
        categoryId = "" ;
        storeId = "" ;
        keyword = "";
    }
    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
