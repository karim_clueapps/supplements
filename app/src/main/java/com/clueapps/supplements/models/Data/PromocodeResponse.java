package com.clueapps.supplements.models.Data;

/**
 * Created by as on 6/20/2018.
 */

public class PromocodeResponse {


    private boolean isValid ;
    private String message;
    private Promocode promocode;

    public boolean isValid() {
        return isValid;
    }

    public String getMessage() {
        return message;
    }

    public Promocode getPromocode() {
        return promocode;
    }
}
