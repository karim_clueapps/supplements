package com.clueapps.supplements.models.responses;

import com.clueapps.supplements.models.Data.Category;
import com.clueapps.supplements.models.Data.Store;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class StoresResponse {

    @SerializedName("stores")
    @Expose
    private ArrayList<Store> stores;

    public ArrayList<Store> getStores() {
        return stores;
    }
}