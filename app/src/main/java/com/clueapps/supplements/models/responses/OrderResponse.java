package com.clueapps.supplements.models.responses;

import com.clueapps.supplements.models.Data.Order;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.models.Data.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class OrderResponse {

    @SerializedName("order")
    @Expose
    private Order order;


    @SerializedName("user")
    @Expose
    private User user;


    @SerializedName("products")
    @Expose
    private ArrayList<Product> productArrayList;

    public ArrayList<Product> getProductArrayList() {
        return productArrayList;
    }

    public User getUser() {
        return user;
    }

    public Order getOrder() {
        return order;
    }

}