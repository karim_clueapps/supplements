package com.clueapps.supplements.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgotPasswordRequest {

    @SerializedName("email_address")
    @Expose
    private String email;

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("password")
    @Expose
    private String password;

    public void setCode(int code) {
        this.code = code;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
