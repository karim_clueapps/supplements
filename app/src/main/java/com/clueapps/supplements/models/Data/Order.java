package com.clueapps.supplements.models.Data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by m on 9/19/2017.
 */

public class Order implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("address_id")
    @Expose
    private String addresId;


    @SerializedName("order_id")
    @Expose
    private String orderId;

    @SerializedName("first_name")
    @Expose
    private String fName;


    @SerializedName("last_name")
    @Expose
    private String lName;


    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("number")
    @Expose
    private String number;


    @SerializedName("from")
    @Expose
    private String from;

    @SerializedName("products_no")
    @Expose
    private int quantity;

    @SerializedName("products_price")
    @Expose
    private float productsPrice;


    @SerializedName("total_price")
    @Expose
    private float totalCost;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("delivery_fees")
    @Expose
    private float deliveryFees;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("block")
    @Expose
    private String block;

    @SerializedName("street")
    @Expose
    private String street;

    @SerializedName("building_no")
    @Expose
    private String buildingNum;


    @SerializedName("floor_no")
    @Expose
    private String floorNum;


    @SerializedName("store")
    @Expose
    private Store store;

    @SerializedName("store_id")
    @Expose
    private String storeId;

    @SerializedName("payment_method")
    @Expose
    private String payment;

    @SerializedName("promocode_id")
    @Expose
    private int promocode_id;

    @SerializedName("products")
    @Expose
    private ArrayList<Product> productArrayList;

    public Order(){}


    protected Order(Parcel in) {
        id = in.readString();
        addresId = in.readString();
        orderId = in.readString();
        fName = in.readString();
        lName = in.readString();
        phone = in.readString();
        number = in.readString();
        from = in.readString();
        quantity = in.readInt();
        productsPrice = in.readFloat();
        totalCost = in.readFloat();
        status = in.readString();
        deliveryFees = in.readFloat();
        city = in.readString();
        country = in.readString();
        block = in.readString();
        street = in.readString();
        buildingNum = in.readString();
        floorNum = in.readString();
        store = in.readParcelable(Store.class.getClassLoader());
        storeId = in.readString();
        payment = in.readString();
        productArrayList = in.createTypedArrayList(Product.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(addresId);
        dest.writeString(orderId);
        dest.writeString(fName);
        dest.writeString(lName);
        dest.writeString(phone);
        dest.writeString(number);
        dest.writeString(from);
        dest.writeInt(quantity);
        dest.writeFloat(productsPrice);
        dest.writeFloat(totalCost);
        dest.writeString(status);
        dest.writeFloat(deliveryFees);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(block);
        dest.writeString(street);
        dest.writeString(buildingNum);
        dest.writeString(floorNum);
        dest.writeParcelable(store, flags);
        dest.writeString(storeId);
        dest.writeString(payment);
        dest.writeTypedList(productArrayList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    public void setPromocode_id(int promocode_id) {
        this.promocode_id = promocode_id;
    }

    public String getAddresId() {
        return addresId;
    }

    public void setAddresId(String addresId) {
        this.addresId = addresId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNum() {
        return buildingNum;
    }

    public void setBuildingNum(String buildingNum) {
        this.buildingNum = buildingNum;
    }

    public String getFloorNum() {
        return floorNum;
    }

    public void setFloorNum(String floorNum) {
        this.floorNum = floorNum;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public ArrayList<Product> getProductArrayList() {
        return productArrayList;
    }

    public void setProductArrayList(ArrayList<Product> productArrayList) {
        this.productArrayList = productArrayList;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getProductsPrice() {
        return productsPrice;
    }

    public void setProductsPrice(float productsPrice) {
        this.productsPrice = productsPrice;
    }

    public float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(float totalCost) {
        this.totalCost = totalCost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getDeliveryFees() {
        return deliveryFees;
    }

    public void setDeliveryFees(float deliveryFees) {
        this.deliveryFees = deliveryFees;
    }
}
