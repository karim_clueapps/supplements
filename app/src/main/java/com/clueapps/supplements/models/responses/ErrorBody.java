package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by m on 10/12/2017.
 */

public class ErrorBody {


    @SerializedName("messages")
    public HashMap<String,String> errorMap;

    @SerializedName("message")
    public String message;

    @SerializedName("code")
    public int code;

    public HashMap<String, String> getErrorMap() {
        return errorMap;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
}
