package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DataProductResponse {

    @SerializedName("data")
    @Expose
    private ProductResponse productResponse;


    public ProductResponse getProductResponse() {
        return productResponse;
    }
}