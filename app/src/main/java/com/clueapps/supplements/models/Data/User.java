package com.clueapps.supplements.models.Data;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User  {


    @SerializedName("first_name")
    @Expose
    private String fName;

    @SerializedName("last_name")
    @Expose
    private String lName;

    @SerializedName("birth_date")
    @Expose
    private String birth;

    @SerializedName("language")
    @Expose
    private String language;


    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("email_address")
    @Expose
    private String email;

    @SerializedName("phone")
    @Expose
    private String mobile;

    @SerializedName("images")
    @Expose
    private String image;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("fb_access_token")
    @Expose
    private String fbAccessToken;
    @SerializedName("facebook_id")
    @Expose
    private String facebookId;
    @SerializedName("type_login")
    @Expose
    private String typeLogin;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("firebase_token")
    @Expose
    private String registerId;

    @SerializedName("platform")
    @Expose
    private String platform = "android";


    @SerializedName("is_block")
    @Expose

    private int isBlock;

    @SerializedName("password")
    @Expose
    private String password ;


    @SerializedName("token")
    @Expose
    private String token ;


    public User(){}

    public String getLanguage() {
        return language;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public void setFbAccessToken(String fbAccessToken) {
        this.fbAccessToken = fbAccessToken;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public void setTypeLogin(String typeLogin) {
        this.typeLogin = typeLogin;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public void setIsBlock(int isBlock) {
        this.isBlock = isBlock;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFbAccessToken() {
        return fbAccessToken;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public String getTypeLogin() {
        return typeLogin;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public int getIsBlock() {
        return isBlock;
    }

    public String getToken() {
        return token;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}