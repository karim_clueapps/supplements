package com.clueapps.supplements.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DataCategoriesResponse {

    @SerializedName("data")
    @Expose
    private CategoriesResponse categoriesResponse;

    @SerializedName("paginator")
    @Expose
    private Paginator paginator;

    public Paginator getPaginator() {
        return paginator;
    }

    public CategoriesResponse getCategoriesResponse() {
        return categoriesResponse;
    }
}