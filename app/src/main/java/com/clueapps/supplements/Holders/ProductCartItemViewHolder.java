package com.clueapps.supplements.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;

import butterknife.ButterKnife;



/**
 * <h1>Holder view for product view</h1>
 * ProductCartItemViewHolder class for hold view of product in cart view
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class ProductCartItemViewHolder extends RecyclerView.ViewHolder   {

    //declaration
    public ImageView  imageImv ;
    public TextView nameTxv ,quantityTxv , priceTxv , weightTxv , quantityViewTxv;
    public RelativeLayout  changeQuantityLayout , decreaseLayout , increaseLayout , removeLayout;

    public ProductCartItemViewHolder(View convertView) {
        super(convertView);

        ButterKnife.bind(this, convertView);
         imageImv = (ImageView) convertView.findViewById(R.id.image_imv);
         removeLayout = (RelativeLayout) convertView.findViewById(R.id.remove_layout);
         nameTxv = (TextView) convertView.findViewById(R.id.name_txv);
         weightTxv = (TextView) convertView.findViewById(R.id.weight_txv);
         priceTxv = (TextView) convertView.findViewById(R.id.price_txv);
         quantityTxv = (TextView) convertView.findViewById(R.id.quantity_txv);
         weightTxv = (TextView) convertView.findViewById(R.id.weight_txv);
         decreaseLayout = (RelativeLayout) convertView.findViewById(R.id.decrease_layout);
         increaseLayout = (RelativeLayout) convertView.findViewById(R.id.increase_layout);
        changeQuantityLayout = (RelativeLayout) convertView.findViewById(R.id.changeQuantity_layout);
         quantityViewTxv = (TextView) convertView.findViewById(R.id.quantityView_txv);

     }
}
