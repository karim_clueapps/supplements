package com.clueapps.supplements.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * <h1>Holder view for address</h1>
 * AddressViewHolder class for hold view of address
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class AddressViewHolder extends RecyclerView.ViewHolder   {

    //declaration

    public @BindView(R.id.city_txv) TextView cityTxv ;
    public @BindView(R.id.country_txv) TextView countryTxv ;
    public @BindView(R.id.block_txv) TextView blockTxv;
    public @BindView(R.id.street_txv) TextView streetTxv ;
    public @BindView(R.id.building_txv) TextView buildingTxv ;
    public @BindView(R.id.floor_txv) TextView floorTxv ;
    public @BindView(R.id.close_layout) RelativeLayout closeLayout  ;
    public @BindView(R.id.edit_layout) RelativeLayout editLayout  ;
    public @BindView(R.id.choose_layout) RelativeLayout chooseLayout  ;

    public AddressViewHolder(View convertView) {
        super(convertView);
         ButterKnife.bind(this, convertView);
    }
}
