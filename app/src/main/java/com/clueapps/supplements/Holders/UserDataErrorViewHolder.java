package com.clueapps.supplements.Holders;

import android.app.Activity;
import android.widget.TextView;

import com.clueapps.supplements.R;


/**
 * <h1>Holder view for errors</h1>
 * UserDataErrorViewHolder class for hold view of errors of user data
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class UserDataErrorViewHolder {

    //declaration

    public TextView errorFNameTxv ,errorMobTxv , errorEmailTxv,errorPassTxv ,errorLNameTxv ;

    public UserDataErrorViewHolder(Activity activity) {
        errorFNameTxv = (TextView) activity.findViewById(R.id.first_name_error_txv);
        errorLNameTxv = (TextView) activity.findViewById(R.id.last_name_error_txv);
        errorMobTxv = (TextView) activity.findViewById(R.id.mobile_error_txv);
        errorEmailTxv = (TextView) activity.findViewById(R.id.email_error_txv);
        errorPassTxv = (TextView) activity.findViewById(R.id.password_error_txv);
    }
}
