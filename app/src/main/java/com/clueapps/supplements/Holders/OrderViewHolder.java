package com.clueapps.supplements.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * <h1>Holder view for order view</h1>
 * OrderViewHolder class for hold view of order view
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class OrderViewHolder extends RecyclerView.ViewHolder   {

    //declaration
    public @BindView(R.id.from_txv) TextView fromTxv;
    public @BindView(R.id.number_txv) TextView numberTxv;
    public @BindView(R.id.quantity_txv) TextView quantityTxv;
    public @BindView(R.id.price_txv) TextView priceTxv;
    public @BindView(R.id.status_txv) TextView statusTxv;
    public @BindView(R.id.open_layout) RelativeLayout openLayout;

    public OrderViewHolder(View convertView) {
        super(convertView);

        ButterKnife.bind(this, convertView);

    }
}
