package com.clueapps.supplements.Holders;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;


/**
 * <h1>Holder view for order details view</h1>
 * OrderDetailsViewHolder class for hold view of order details view
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class OrderDetailsViewHolder {

    //declaration

    public TextView totalPriceTxv,delFeesTxv,totalCostTxv , canceledTxv;
    public EditText fNameEdt , lNameEdt , mobileEdt, addressEdt, paymentEdt , productsPriceEdt , delFeesEdt , totalCostEdt;
    public RecyclerView productsRecycler ;
    public RelativeLayout statusLayout ;
    public ImageView pendingImv , confirmedImv , deliveredImv;
    public View line1View , line2View ;
     public OrderDetailsViewHolder(Activity activity) {
         totalPriceTxv = (TextView) activity.findViewById(R.id.totalPrice_txv);
         delFeesTxv = (TextView) activity.findViewById(R.id.delFees_txv);
         totalCostTxv = (TextView) activity.findViewById(R.id.totalCost_txv);
         productsRecycler = (RecyclerView) activity.findViewById(R.id.products_recycler);
         canceledTxv = (TextView) activity.findViewById(R.id.canceled_txv);
         pendingImv = (ImageView) activity.findViewById(R.id.pending_imv);
         deliveredImv = (ImageView) activity.findViewById(R.id.delivered_imv);
         confirmedImv = (ImageView) activity.findViewById(R.id.confirmed_imv);
         line1View = (View) activity.findViewById(R.id.line1_view);
         line2View = (View) activity.findViewById(R.id.line2_view);
         statusLayout = (RelativeLayout) activity.findViewById(R.id.status_layout);

     }
}
