package com.clueapps.supplements.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;

import butterknife.ButterKnife;


/**
 * <h1>Holder view for order item view</h1>
 * OrderItemViewHolder class for hold view of order item view
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class OrderItemViewHolder extends RecyclerView.ViewHolder   {

    //declaration

    public ImageView  imageImv ;
    public TextView nameTxv ,quantityTxv , priceTxv , weightTxv;
     public OrderItemViewHolder(View convertView) {
        super(convertView);

         ButterKnife.bind(this, convertView);
         imageImv = (ImageView) convertView.findViewById(R.id.image_imv);
         weightTxv = (TextView) convertView.findViewById(R.id.weight_txv);
         nameTxv = (TextView) convertView.findViewById(R.id.name_txv);
         quantityTxv = (TextView) convertView.findViewById(R.id.quantity_txv);
         priceTxv = (TextView) convertView.findViewById(R.id.price_txv);
     }
}
