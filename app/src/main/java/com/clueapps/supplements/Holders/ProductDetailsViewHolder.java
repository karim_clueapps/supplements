package com.clueapps.supplements.Holders;

import android.app.Activity;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;
import com.clueapps.supplements.adapters.ImagesAdapter;
import com.clueapps.supplements.models.Data.ImagePath;
import com.clueapps.supplements.models.Data.Product;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * <h1>Holder view for product details view</h1>
 * ProductDetailsViewHolder class for hold view of product details view
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class ProductDetailsViewHolder{

    //declaration

    public TextView nameTxv,oldPriceTxv,descTxv,weightTxv,quantityTxv,storeTxv,priceTxv,usageAgeTxv,titleTxv,brandTxv,discountTxv;
    public ViewPager imagesPager ;
    public TabLayout dotsTab;
    public ImagesAdapter imagesAdapter;
    public ArrayList<ImagePath> imagePaths;
    public  ImageView  favImv , soldOutImv;
    public Product product ;
    public Button addCartBtn;
    public ProductDetailsViewHolder(Activity activity) {
         nameTxv = (TextView) activity.findViewById(R.id.name_txv);
         titleTxv = (TextView) activity.findViewById(R.id.title_txv);
         oldPriceTxv = (TextView) activity.findViewById(R.id.oldPrice_txv);
         descTxv = (TextView) activity.findViewById(R.id.desc_txv);
         weightTxv = (TextView) activity.findViewById(R.id.weight_txv);
         quantityTxv = (TextView) activity.findViewById(R.id.quantity_txv);
         storeTxv = (TextView) activity.findViewById(R.id.store_txv);
         priceTxv = (TextView) activity.findViewById(R.id.price_txv);
         usageAgeTxv = (TextView) activity.findViewById(R.id.usageAge_txv);
         brandTxv = (TextView) activity.findViewById(R.id.brand_txv);
         discountTxv = (TextView) activity.findViewById(R.id.discount_txv);
         imagesPager = (ViewPager)activity.findViewById(R.id.images_pager);
         dotsTab = (TabLayout)activity.findViewById(R.id.dots_tab);
        favImv = (ImageView)activity.findViewById(R.id.fav_imv);
        soldOutImv = (ImageView)activity.findViewById(R.id.sold_out_imv);
        addCartBtn = (Button)activity.findViewById(R.id.addCart_btn);

     }
}
