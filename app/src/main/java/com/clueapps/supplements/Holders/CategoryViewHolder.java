package com.clueapps.supplements.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * <h1>Holder view for category</h1>
 * CategoryViewHolder class for hold view of category
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class CategoryViewHolder extends RecyclerView.ViewHolder   {

    //declaration
    public @BindView(R.id.image_imv) ImageView imageImv ;
    public @BindView(R.id.name_txv) TextView nameTxv ;
    public @BindView(R.id.numItems_txv) TextView numItemsTxv ;
    public @BindView(R.id.open_layout) RelativeLayout openLayout ;

    public CategoryViewHolder(View convertView) {
        super(convertView);

         ButterKnife.bind(this, convertView);
     }
}
