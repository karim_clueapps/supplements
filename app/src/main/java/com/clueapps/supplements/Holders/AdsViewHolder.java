package com.clueapps.supplements.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * <h1>Holder view for ad</h1>
 * AdsViewHolder class for hold view of ad
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class AdsViewHolder extends RecyclerView.ViewHolder   {

    //declaration
    public @BindView(R.id.image_imv) ImageView imageImv ;

     public AdsViewHolder(View convertView) {
        super(convertView);

         ButterKnife.bind(this, convertView);
     }
}
