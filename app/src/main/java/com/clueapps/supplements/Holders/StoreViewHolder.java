package com.clueapps.supplements.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;

import butterknife.ButterKnife;


/**
 * <h1>Holder view for store view</h1>
 * StoreViewHolder class for hold view of store view
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class StoreViewHolder extends RecyclerView.ViewHolder   {

    //declaration
    public ImageView  imageImv ;
    public TextView nameTxv , allTxv;
    public RelativeLayout openLayout ;
     public StoreViewHolder(View convertView) {
        super(convertView);

         ButterKnife.bind(this, convertView);
         imageImv = (ImageView) convertView.findViewById(R.id.image_imv);
         nameTxv = (TextView) convertView.findViewById(R.id.name_txv);
         allTxv = (TextView) convertView.findViewById(R.id.all_txv);
         openLayout = (RelativeLayout) convertView.findViewById(R.id.open_layout);
     }
}
