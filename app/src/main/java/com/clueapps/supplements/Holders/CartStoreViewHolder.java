package com.clueapps.supplements.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * <h1>Holder view for ad</h1>
 * CartStoreViewHolder class for hold view of Stores in cart
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class CartStoreViewHolder extends RecyclerView.ViewHolder   {

    //declaration

    public @BindView(R.id.name_txv) TextView nameTxv ;
    public @BindView(R.id.quantity_txv) TextView quantityTxv ;
    public @BindView(R.id.price_txv) TextView priceTxv ;
    public @BindView(R.id.image_imv) ImageView imageImv ;
    public @BindView(R.id.open_layout) RelativeLayout openLayout ;
    public @BindView(R.id.remove_layout) RelativeLayout removeLayout ;

     public CartStoreViewHolder(View convertView) {
        super(convertView);
         ButterKnife.bind(this, convertView);

     }
}
