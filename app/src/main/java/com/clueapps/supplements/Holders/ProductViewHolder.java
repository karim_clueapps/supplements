package com.clueapps.supplements.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;

import butterknife.ButterKnife;


/**
 * <h1>Holder view for product view</h1>
 * ProductViewHolder class for hold view of product view
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class ProductViewHolder extends RecyclerView.ViewHolder   {

    //declaration
    public ImageView  imageImv , favImv , offerImv , soldOutImv;
    public TextView nameTxv , weightTxv , storeTxv, priceTxv,quantityViewTxv;
    public RelativeLayout   changeQuantityLayout ,decreaseLayout , increaseLayout;
    public RelativeLayout openLayout;
    public Button addCartBtn ;
     public ProductViewHolder(View convertView) {
        super(convertView);

         ButterKnife.bind(this, convertView);
         imageImv = (ImageView) convertView.findViewById(R.id.image_imv);
         soldOutImv = (ImageView) convertView.findViewById(R.id.sold_out_imv);
         favImv = (ImageView) convertView.findViewById(R.id.fav_imv);
         offerImv = (ImageView) convertView.findViewById(R.id.offer_imv);
         nameTxv = (TextView) convertView.findViewById(R.id.name_txv);
         weightTxv = (TextView) convertView.findViewById(R.id.weight_txv);
         priceTxv = (TextView) convertView.findViewById(R.id.price_txv);
         storeTxv = (TextView) convertView.findViewById(R.id.store_txv);
         weightTxv = (TextView) convertView.findViewById(R.id.weight_txv);
         addCartBtn = (Button) convertView.findViewById(R.id.addCart_btn);
         openLayout = (RelativeLayout) convertView.findViewById(R.id.open_layout);
         changeQuantityLayout = (RelativeLayout) convertView.findViewById(R.id.changeQuantity_layout);
         decreaseLayout = (RelativeLayout) convertView.findViewById(R.id.decrease_layout);
         increaseLayout = (RelativeLayout) convertView.findViewById(R.id.increase_layout);
         quantityViewTxv = (TextView) convertView.findViewById(R.id.quantityView_txv);


     }
}
