package com.clueapps.supplements.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import com.clueapps.supplements.helpers.Dialogs;



/**
 * <h1>Notify when Change network status on device</h1>
 * ConnectionChangeReceiver class for handle internet off and internet on view
 * <p>
 *
 * @author  kemo94
 * @version 1.0
 * @since   2017-10-9
 */
public class ConnectionChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

            ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(
                    Context.CONNECTIVITY_SERVICE));
        // internet is on
        if (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected()) {
            Dialogs.dismissInternetDialog();
         }
        // internet is off show no internet dialog
        else {
            Dialogs.noInternetDialog();
         }

    }

}
