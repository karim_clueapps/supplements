package com.clueapps.supplements.Fragments;

import android.support.v4.app.Fragment;

import java.lang.reflect.Field;
import java.util.ArrayList;

import retrofit2.Call;

/**
 * Created by as on 8/20/2017.
 */

public class SafeFragment extends Fragment {
    private ArrayList<Call> calls;

    public void addCallToCancel(Call call)
    {
        if (calls==null)
            calls = new ArrayList<>();
        calls.add(call);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        if(calls!=null)
        {
            for (int i = 0;i<calls.size();i++)
            {
                if(calls.get(i)!=null)
                    calls.get(i).cancel();
            }
        }
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
