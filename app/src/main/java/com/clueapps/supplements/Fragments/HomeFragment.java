package com.clueapps.supplements.Fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.clueapps.supplements.API.API;
import com.clueapps.supplements.API.AddressAPI;
import com.clueapps.supplements.API.ProductAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Address.AddressesActivity;
import com.clueapps.supplements.activities.Product.SearchActivity;
import com.clueapps.supplements.adapters.AdsAdapter;
import com.clueapps.supplements.adapters.CategoryAdapter;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Ad;
import com.clueapps.supplements.models.Data.Category;
import com.clueapps.supplements.models.Data.PaginatorData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends Fragment {

    public RecyclerView categoryRecycler ;
    public ViewPager adsPager;

    public CategoryAdapter categoryAdapter;
    public AdsAdapter adsAdapter;
    public RelativeLayout progressRel,progressMoreDataRel;
    public LinearLayout noResultLayout;
    public ArrayList<Category> categoryArrayList =new ArrayList<>();
    public ArrayList<Ad> adsArrayList =new ArrayList<>();
    PaginatorData paginatorData ;


    Handler handler;
    Runnable runnable ;

    @BindView(R.id.categories_nested_scroll_view) NestedScrollView nestedScrollView ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(getActivity(),view);

        initView(view);
        listeners();
        prepareView();
        return view;
    }
    public void initView(View view) {

        paginatorData = new PaginatorData();
        paginatorData.refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_layout);
        paginatorData.progressMoreDataRel = (RelativeLayout) view.findViewById(R.id.progressMoreDataRel);

        adsPager = (ViewPager) view.findViewById(R.id.ads_pager);
        nestedScrollView = (NestedScrollView) view.findViewById(R.id.categories_nested_scroll_view);
        categoryRecycler = (RecyclerView) view.findViewById(R.id.category_recycler);
        categoryRecycler.setNestedScrollingEnabled(false);
        
        adsAdapter =new AdsAdapter(getActivity(), adsArrayList);
        categoryAdapter =new CategoryAdapter(getActivity(), categoryArrayList);
        progressRel=(RelativeLayout)view.findViewById(R.id.progressRel);
        progressMoreDataRel =(RelativeLayout)view.findViewById(R.id.progressMoreDataRel);
        noResultLayout=(LinearLayout) view.findViewById(R.id.no_result_relative);


        adsPager.setAdapter(adsAdapter);

        categoryRecycler.setHasFixedSize(true);

        categoryRecycler.setAdapter(categoryAdapter);
    }
    public void prepareView(){
        resetCategoriesView();
        ProductAPI.getCategories(getActivity(),categoryAdapter,categoryArrayList,adsAdapter,adsArrayList,paginatorData);
        repeatedSlideShow();
    }
    public void listeners() {


        paginatorData.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetCategoriesView();
                adsArrayList.clear();
                adsAdapter.notifyDataSetChanged();
                ProductAPI.getCategories(getActivity(),categoryAdapter,categoryArrayList,adsAdapter,adsArrayList,paginatorData);

             }
        });

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (!paginatorData.loading && ! paginatorData.empty) {
                                ProductAPI.getCategories(getActivity(),categoryAdapter,categoryArrayList,adsAdapter,adsArrayList,paginatorData);
                        }
                    }
                }
            }
        });
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        categoryRecycler.setLayoutManager(linearLayoutManager);

    }
    /**
     * This method is used to reset categories recycler ana paging.
     * called when refresh or resume activity.
     */
    public void resetCategoriesView(){
        categoryArrayList.clear();
        categoryAdapter.notifyDataSetChanged();

        paginatorData.page = 1 ;
        paginatorData.loading = false ;
        paginatorData.empty = false ;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void repeatedSlideShow() {

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                animateandSlideShow();
                handler.postDelayed(this, 3 * 1000);
            }
        };
        handler.postDelayed(runnable, 3 * 1000);
    }

    public void animateandSlideShow() {
        int count = adsPager.getCurrentItem();
        if (count == adsArrayList.size() - 1) {
            count = 0;
            adsPager.setCurrentItem(count, true);
        } else {
            count++;
            adsPager.setCurrentItem(count, true);
        }
    }

}
