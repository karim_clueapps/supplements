package com.clueapps.supplements.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.IntentClass;

public class AboutAppFragment extends Fragment {

    ImageView ic_mail , ic_website,ic_linkedin,ic_facebook;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_app, container, false);


        ic_mail =(ImageView) view.findViewById(R.id.ic_mail);
        ic_website=(ImageView) view.findViewById(R.id.ic_website);
        ic_linkedin=(ImageView) view.findViewById(R.id.ic_linkedin);
        ic_facebook =(ImageView) view.findViewById(R.id.ic_facebook);


        ic_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentClass.goToGmail(getActivity(),"info@clueapps.net");
            }
        });

        ic_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentClass.goToLink(getActivity(),"https://clueapps.net");
            }
        });
        ic_linkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentClass.goToLink(getActivity(),"https://google.com");
            }
        });
        ic_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentClass.goToFacebook(getActivity(),"898864560163206");
            }
        });

        return view;
    }


}
