package com.clueapps.supplements.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Address.AddressesActivity;
import com.clueapps.supplements.activities.Order.OrdersActivity;
import com.clueapps.supplements.activities.User.AccountActivity;
import com.clueapps.supplements.activities.Product.WishListActivity;
import com.clueapps.supplements.helpers.IntentClass;

import butterknife.BindView;

public class ProfileFragment extends Fragment {

    @BindView(R.id.account_layout) RelativeLayout accountLayout ;
    @BindView(R.id.wishlist_layout) RelativeLayout wishlistLayout ;
    @BindView(R.id.order_layout) RelativeLayout ordersLayout ;
    @BindView(R.id.address_layout) RelativeLayout addressesLayout ;
    @BindView(R.id.rate_layout) RelativeLayout rateLayout ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        initView(view);
        listeners();
        return view;
    }


    public void initView(View view) {

        accountLayout = (RelativeLayout) view.findViewById(R.id.account_layout);
        TextView accountTxv = (TextView) accountLayout.findViewById(R.id.title_txv);
        ImageView  accountImv  = (ImageView) accountLayout.findViewById(R.id.icon_imv);

        accountTxv.setText(getActivity().getString(R.string.account));
        accountImv.setImageResource(R.drawable.userinfoicon);


        wishlistLayout = (RelativeLayout) view.findViewById(R.id.wishlist_layout);
        TextView wishListTxv = (TextView) wishlistLayout.findViewById(R.id.title_txv);
        ImageView  wishListImv  = (ImageView) wishlistLayout.findViewById(R.id.icon_imv);

        wishListTxv.setText(getActivity().getString(R.string.my_wishlist));
        wishListImv.setImageResource(R.drawable.mywishlisticon);


        ordersLayout = (RelativeLayout) view.findViewById(R.id.orders_layout);
        TextView ordersTxv = (TextView) ordersLayout.findViewById(R.id.title_txv);
        ImageView  ordersImv  = (ImageView) ordersLayout.findViewById(R.id.icon_imv);

        ordersTxv.setText(getActivity().getString(R.string.my_orders));
        ordersImv.setImageResource(R.drawable.myordericon);


        addressesLayout = (RelativeLayout) view.findViewById(R.id.addresses_layout);
        TextView addressesTxv = (TextView) addressesLayout.findViewById(R.id.title_txv);
        ImageView  addressesImv  = (ImageView) addressesLayout.findViewById(R.id.icon_imv);

        addressesTxv.setText(getActivity().getString(R.string.my_addresses));
        addressesImv.setImageResource(R.drawable.myadressesicon);


        rateLayout = (RelativeLayout) view.findViewById(R.id.rate_layout);
        TextView rateTxv = (TextView) rateLayout.findViewById(R.id.title_txv);
        ImageView  rateImv  = (ImageView) rateLayout.findViewById(R.id.icon_imv);

        rateTxv.setText(getActivity().getString(R.string.rate_app));
        rateImv.setImageResource(R.drawable.rateappicon);

    }
    public void listeners() {
        accountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(getActivity(), AccountActivity.class,null);
            }
        });

        wishlistLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(getActivity(), WishListActivity.class,null);
            }
        });

        addressesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(getActivity(), AddressesActivity.class,null);
            }
        });

        ordersLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(getActivity(), OrdersActivity.class,null);
            }
        });

        rateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.rateApp(getActivity());
            }
        });
    }

}
