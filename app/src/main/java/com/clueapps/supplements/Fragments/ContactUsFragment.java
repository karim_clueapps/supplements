package com.clueapps.supplements.Fragments;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.UserAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.helpers.Validations;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactUsFragment extends Fragment {

    EditText emailEdt , msgEdt ;

    @BindView(R.id.msg_error_txv) TextView msgErrorTxv ;
    @BindView(R.id.email_error_txv) TextView emailErrorTxv ;
    SharedPref sharedPref;
    Button sendBtn ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(view,getActivity());

        sharedPref = new SharedPref(getActivity());

        initView(view);
        listener();
        return view;
    }


    public void initView(View view) {

        msgErrorTxv = (TextView) view.findViewById(R.id.msg_error_txv);
        emailErrorTxv = (TextView) view.findViewById(R.id.email_error_txv);
        sendBtn = (Button) view.findViewById(R.id.send_btn);

        LinearLayout emailLayout = (LinearLayout) view.findViewById(R.id.email_layout);
        emailEdt = (EditText) emailLayout.findViewById(R.id.input_edt);
        emailEdt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        Utility.setUserRawView(getActivity(), emailLayout , R.drawable.emailiconblack ,emailEdt,getString(R.string.email)  , "" );

        if ( sharedPref.getToken().length() > 0 )
            emailEdt.setText(sharedPref.getUserEmail());

        LinearLayout msgLayout = (LinearLayout) view.findViewById(R.id.message_layout);
        msgEdt = (EditText) msgLayout.findViewById(R.id.input_edt);
        Utility.setUserRawView(getActivity(), msgLayout , R.drawable.emailiconblack ,msgEdt,getString(R.string.msg)  , "");

    }

    public void listener(){

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateInput();
            }
        });
        msgEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) { }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) { }

            @Override
            public void afterTextChanged(Editable editable) {
                if (null != msgEdt.getLayout() && msgEdt.getLayout().getLineCount() > 8) {
                    msgEdt.getText().delete(msgEdt.getText().length() - 1, msgEdt.getText().length());
                }
            }
        });
    }

    @OnClick(R.id.send_btn)
    public void validateInput(){
        boolean validated =true;

        String email = emailEdt.getText().toString();
        String msg = msgEdt.getText().toString().trim();

        validated = validated&& Validations.isValidEmail(email);
        Validations.animateEmailView(email,emailErrorTxv);

        validated = validated&&Validations.isValidStr(msg);
        Validations.animateStrView(msg,msgErrorTxv);

        if(validated) {
            UserAPI.contactUsApi(getActivity(),email,msg);
        }
    }

}
