package com.clueapps.supplements.activities.Cart;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.OrderAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.adapters.OrderItemsAdapter;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Order;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.models.Data.PromocodeResponse;
import com.clueapps.supplements.models.Data.Store;
import com.clueapps.supplements.realm.RealmController;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckoutActivity extends AppCompatActivity {
    SharedPref sharedPref;
    TextView editAddressTxv, editCartTxv;
    String payment;
    String address, addressId;
    Store store;
    OrderItemsAdapter orderItemsAdapter;

    RecyclerView productsRecycler;
    public TextView totalPriceTxv, totalCostTxv;
    TextView delFeesTxv;
    public ArrayList<Product> productArrayList = new ArrayList<>();
    Button deliveryDetailsBtn;
    Order order = new Order();

    EditText promocode_edt;
    TextView status_code_txv;
    ProgressBar progress_bar;

    int promocodeId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);

        initData();
        initView();
        listeners();
    }


    public void initData() {
        Bundle bundle = getIntent().getBundleExtra("data");

        if (bundle != null) {
            address = bundle.getString("address");
            addressId = bundle.getString("addressId");
            payment = bundle.getString("payment");
            store = bundle.getParcelable("store");
        }
        sharedPref = new SharedPref(this);
        Utility.setCurrentActivity(this);
        CustomHeader.setToolbar(this, getString(R.string.checkout));

        productArrayList = RealmController.getStoreProducts(store.getId());

    }

    public void initView() {
        promocode_edt = (EditText) findViewById(R.id.promocode_edt);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        status_code_txv = (TextView) findViewById(R.id.status_code_txv);
        productsRecycler = (RecyclerView) findViewById(R.id.products_recycler);
        productsRecycler.setNestedScrollingEnabled(false);

        orderItemsAdapter = new OrderItemsAdapter(this, productArrayList);
        deliveryDetailsBtn = (Button) findViewById(R.id.deliveryDetails_btn);
        totalPriceTxv = (TextView) findViewById(R.id.totalPrice_txv);
        delFeesTxv = (TextView) findViewById(R.id.delFees_txv);
        totalCostTxv = (TextView) findViewById(R.id.totalCost_txv);

        productsRecycler.setHasFixedSize(true);

        productsRecycler.setAdapter(orderItemsAdapter);


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        productsRecycler.setLayoutManager(linearLayoutManager);

        editAddressTxv = (TextView) findViewById(R.id.editAddress_txv);
        editCartTxv = (TextView) findViewById(R.id.editCart_txv);


        RelativeLayout fNameLayout = (RelativeLayout) findViewById(R.id.fName_layout);
        TextView fNameTxv = (TextView) fNameLayout.findViewById(R.id.title_txv);
        EditText fNameEdt = (EditText) fNameLayout.findViewById(R.id.input_edt);

        fNameTxv.setText(getString(R.string.first_name));
        fNameEdt.setText(sharedPref.getFName());
        fNameEdt.setEnabled(false);


        RelativeLayout lNameLayout = (RelativeLayout) findViewById(R.id.lName_layout);
        TextView lNameTxv = (TextView) lNameLayout.findViewById(R.id.title_txv);
        EditText lNameEdt = (EditText) lNameLayout.findViewById(R.id.input_edt);

        lNameTxv.setText(getString(R.string.last_name));
        lNameEdt.setText(sharedPref.getLName());
        lNameEdt.setEnabled(false);


        RelativeLayout mobileLayout = (RelativeLayout) findViewById(R.id.mobile_layout);
        TextView mobileTxv = (TextView) mobileLayout.findViewById(R.id.title_txv);
        EditText mobileEdt = (EditText) mobileLayout.findViewById(R.id.input_edt);

        mobileTxv.setText(getString(R.string.mobile));
        mobileEdt.setText(sharedPref.getUserMob());
        mobileEdt.setEnabled(false);


        RelativeLayout addressLayout = (RelativeLayout) findViewById(R.id.address_layout);
        TextView addressTxv = (TextView) addressLayout.findViewById(R.id.title_txv);
        EditText addressEdt = (EditText) addressLayout.findViewById(R.id.input_edt);

        addressTxv.setText(getString(R.string.address));
        addressEdt.setText(address);
        addressEdt.setEnabled(false);

        RelativeLayout paymentLayout = (RelativeLayout) findViewById(R.id.payment_layout);
        TextView paymentTxv = (TextView) paymentLayout.findViewById(R.id.title_txv);
        EditText paymentEdt = (EditText) paymentLayout.findViewById(R.id.input_edt);

        paymentTxv.setText(getString(R.string.payment_method));
        paymentEdt.setText(payment);
        paymentEdt.setEnabled(false);


    }

    public void listeners() {
        promocode_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String code = promocode_edt.getText().toString();
                if (code.length() > 0) {
                    progress_bar.setVisibility(View.VISIBLE);
                    status_code_txv.setVisibility(View.GONE);
                    OrderAPI.validatePromoCode(CheckoutActivity.this, code, new OrderAPI.ApiResponseListener() {
                        @Override
                        public void onSuccess(String response) {

                            PromocodeResponse promocodeResponse = new Gson().fromJson(response, new TypeToken<PromocodeResponse>() {
                            }.getType());
                            progress_bar.setVisibility(View.GONE);
                            status_code_txv.setVisibility(View.VISIBLE);

                            if (promocodeResponse.isValid()) {
                                promocodeId = promocodeResponse.getPromocode().getId();
                                status_code_txv.setText(promocodeResponse.getPromocode().getDiscount() + "%");
                                status_code_txv.setTextColor(getResources().getColor(R.color.right_green));
                                double productsPrice = Utility.getTotalPrice(productArrayList);
                                double total = productsPrice + store.getFees();

                                totalCostTxv.setText((total - (total * ((double) promocodeResponse.getPromocode().getDiscount() / (double) 100))) + "");
                            } else {
                                promocodeId = 0;
                                status_code_txv.setText("X");
                                status_code_txv.setTextColor(getResources().getColor(R.color.colorPrimary));
                                double productsPrice = Utility.getTotalPrice(productArrayList);
                                totalCostTxv.setText(Utility.setPrice(CheckoutActivity.this, productsPrice + store.getFees()));
                            }

                        }
                    });
                }
            }
        });
        editAddressTxv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        editCartTxv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                ShippingActivity.activity.finish();
            }
        });
    }


    @OnClick(R.id.checkout_btn)
    public void checkout() {
        order.setStoreId(store.getId());
        order.setAddresId(addressId);
        for (int i = 0; i < productArrayList.size(); i++) {
            productArrayList.get(i).setQuantity(productArrayList.get(i).getQuantityCart());
        }
        order.setProductArrayList(productArrayList);
        order.setPayment(payment.equals("cash") ? "cash_collection" : "credit_card");
        order.setPromocode_id(promocodeId);
        OrderAPI.addOrder(CheckoutActivity.this, order);
    }

    public void prepareView() {
        double productsPrice = Utility.getTotalPrice(productArrayList);
        delFeesTxv.setText(Utility.setPrice(this, store.getFees()));
        totalPriceTxv.setText(Utility.setPrice(this, productsPrice));
        totalCostTxv.setText(Utility.setPrice(this, productsPrice + store.getFees()));
    }

    @Override
    protected void onResume() {
        prepareView();

        super.onResume();
    }

}
