package com.clueapps.supplements.activities.Product;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.clueapps.supplements.API.ProductAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.adapters.ProductsAdapter;
import com.clueapps.supplements.adapters.SpinnersAdapter;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.DatePicker;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Category;
import com.clueapps.supplements.models.Data.PaginatorData;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.models.Data.SearchProduct;
import com.clueapps.supplements.models.Data.Store;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    public RecyclerView productsRecycler;
    public ProductsAdapter productsAdapter;
    public ArrayList<Product> productArrayList =new ArrayList<>();
    ImageView backImv , filterImv;
    TextView dateTxv;
    Spinner categorySpinner , storeSpinner;

    Dialog filterDialog;
    List<String> categoryList , storeList;

    ArrayList<Category> categoryArray  = new ArrayList<>();
    ArrayList<Store> storeArray = new ArrayList<>();
    EditText searchEdt ;
    ImageView searchImv ;
    SearchProduct searchProduct = new SearchProduct();
    PaginatorData paginatorData = new PaginatorData();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        initView();
        listeners();
    }

    public void resetProductsView(){
        productArrayList.clear();
        productsAdapter.notifyDataSetChanged();
        paginatorData.page = 1;
        paginatorData.loading = false;
        paginatorData.empty = false;

    }
    public void prepareView() {

        resetProductsView();
        ProductAPI.getCategoriesSpinner(this,categorySpinner,categoryArray);
        ProductAPI.searchProducts(SearchActivity.this,productsAdapter,searchProduct,productArrayList,paginatorData);

    }
    public void initView() {

        paginatorData.refreshLayout=(SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        paginatorData.noResultLayout = (LinearLayout) findViewById(R.id.no_result_layout);
        paginatorData.recyclerView = (RecyclerView) findViewById(R.id.products_recycler);
        paginatorData.progressMoreDataRel = (RelativeLayout) findViewById(R.id.progressMoreDataRel);

        productsRecycler = (RecyclerView) findViewById(R.id.products_recycler);
        searchImv = (ImageView) findViewById(R.id.search_imv);

        productsAdapter =new ProductsAdapter(this, productArrayList,false);

        backImv =(ImageView) findViewById(R.id.back_imv);
        filterImv =(ImageView) findViewById(R.id.filter_imv);
        searchEdt = (EditText) findViewById(R.id.search_edt);

        productsRecycler.setHasFixedSize(true);

        productsRecycler.setAdapter(productsAdapter);

        initViewFilterDialog();
    }

    public void listeners() {

        paginatorData.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetProductsView();
                ProductAPI.searchProducts(SearchActivity.this,productsAdapter,searchProduct,productArrayList,paginatorData);
            }
        });

        final GridLayoutManager gridLayoutManager =  new GridLayoutManager(this,2);
        productsRecycler.setLayoutManager(gridLayoutManager);

        productsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = gridLayoutManager.getChildCount();
                int totalItemCount = gridLayoutManager.getItemCount();
                int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();

                if (!paginatorData.loading && !paginatorData.empty) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= Utility.take) {
                        ProductAPI.searchProducts(SearchActivity.this,productsAdapter,searchProduct,productArrayList,paginatorData);
                    }
                }

            }
        });

        backImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        filterImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                filterDialog.show();
            }
        });

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0){
                    ProductAPI.getStoresSpinner(SearchActivity.this,categoryArray.get(position-1).getId(),storeSpinner,storeArray);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        searchImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!paginatorData.loading) {
                    resetProductsView();
                    searchProduct.setKeyword(searchEdt.getText().toString());
                    ProductAPI.searchProducts(SearchActivity.this, productsAdapter, searchProduct, productArrayList, paginatorData);
                }
            }
        });
        searchEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!paginatorData.loading) {
                        resetProductsView();
                        searchProduct.setKeyword(searchEdt.getText().toString());
                        ProductAPI.searchProducts(SearchActivity.this, productsAdapter, searchProduct, productArrayList, paginatorData);
                    }
                    return true;
                }
                return false;
            }
        });


    }

    @Override
    protected void onResume() {

        prepareView();

        super.onResume();
    }


    public void initViewFilterDialog() {

        filterDialog = new Dialog(SearchActivity.this,R.style.DialogFullScreenTheme);
        filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        filterDialog.setContentView(R.layout.dialog_filter);
        filterDialog.setCancelable(true);
        filterDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        CustomHeader.setDialogToolbar(filterDialog,getString(R.string.filters));

        categorySpinner = (Spinner) filterDialog.findViewById(R.id.category_spinner);
        categoryList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.category)));
        SpinnersAdapter categoryAdapter = new SpinnersAdapter(categoryList,this,R.color.charcoal_grey_85);
        categorySpinner.setAdapter(categoryAdapter);

        storeSpinner = (Spinner) filterDialog.findViewById(R.id.sub_category_spinner);
        storeList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.store)));
        SpinnersAdapter subCategoryAdapter = new SpinnersAdapter(storeList,this,R.color.charcoal_grey_85);
        storeSpinner.setAdapter(subCategoryAdapter);

        final CrystalRangeSeekbar rangeSeekbar = (CrystalRangeSeekbar) filterDialog.findViewById(R.id.range_seekbar);
        RelativeLayout pickDateLayout =  (RelativeLayout) filterDialog.findViewById(R.id.pickDate_layout);
        final ImageView lowImv =  (ImageView) filterDialog.findViewById(R.id.low_imv);
        final ImageView highImv =  (ImageView) filterDialog.findViewById(R.id.high_imv);
        final TextView highTxv =  (TextView) filterDialog.findViewById(R.id.high_txv);
        final TextView lowTxv =  (TextView) filterDialog.findViewById(R.id.low_txv);
        final TextView minPriceTxv =  (TextView) filterDialog.findViewById(R.id.min_price_txv);
        final TextView maxPriceTxv =  (TextView) filterDialog.findViewById(R.id.max_price_txv);

        Button clearBtn =   (Button) filterDialog.findViewById(R.id.clear_btn);
        Button applyBtn =   (Button) filterDialog.findViewById(R.id.apply_btn);


        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                minPriceTxv.setText(Utility.setPrice(SearchActivity.this, minValue.floatValue()) );
                maxPriceTxv.setText(Utility.setPrice(SearchActivity.this, maxValue.floatValue()) );

                searchProduct.setMaxPrice(maxValue.floatValue());
                searchProduct.setMinPrice(minValue.floatValue());
            }
        });

        highImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lowImv.setImageResource(R.drawable.lowtohigh);
                lowTxv.setTextColor(getResources().getColor(R.color.black));
                highImv.setImageResource(R.drawable.hightolow_selected );
                highTxv.setTextColor(getResources().getColor(R.color.red));
                searchProduct.setOrderBy(Utility.HIGH_TO_LOW);
            }
        });

        lowImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                highImv.setImageResource(R.drawable.hightolow);
                highTxv.setTextColor(getResources().getColor(R.color.black));
                lowImv.setImageResource(R.drawable.lowtohigh_selected);
                lowTxv.setTextColor(getResources().getColor(R.color.red));
                searchProduct.setOrderBy(Utility.LOW_TO_HIGH);
            }
        });

        pickDateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePicker.chooseDate(view,SearchActivity.this);
            }
        });

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categorySpinner.setSelection(0);
                storeSpinner.setSelection(0);

                searchProduct.setMaxPrice(rangeSeekbar.getSelectedMaxValue().floatValue());
                searchProduct.setMinPrice(rangeSeekbar.getSelectedMinValue().floatValue());

                lowImv.setImageResource(R.drawable.lowtohigh);
                lowTxv.setTextColor(getResources().getColor(R.color.black));
                highImv.setImageResource(R.drawable.hightolow_selected );
                highTxv.setTextColor(getResources().getColor(R.color.red));

                searchProduct = new SearchProduct();
            }
        });

        applyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String categoryId = "", storeId = "";

                if ( categorySpinner.getSelectedItemPosition() != 0 )
                    categoryId = categoryArray.get(categorySpinner.getSelectedItemPosition()-1).getId() ;

                if ( storeSpinner.getSelectedItemPosition() != 0 )
                    storeId = storeArray.get(storeSpinner.getSelectedItemPosition()-1).getId() ;

                searchProduct.setCategoryId(categoryId);
                searchProduct.setStoreId(storeId);
                resetProductsView();
                ProductAPI.searchProducts(SearchActivity.this,productsAdapter,searchProduct,productArrayList,paginatorData);

                filterDialog.dismiss();
            }
        });

    }

    @Override
    protected Dialog onCreateDialog(int id)
    {
        switch (id) {
            case 0:
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(this, R.style.DatePickerStyle,datePickerListener, year, month,day);
                 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    // Call some material design APIs here
                } else {
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }
                return dialog;

        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {

            // set selected date into textview

            String dateStr = new StringBuilder().append(year)
                    .append("-").append( DatePicker.pad(month+1)).append("-").append(DatePicker.pad(dayOfMonth))
                    .toString();

            dateTxv.setText(dateStr);

        }

    };
}