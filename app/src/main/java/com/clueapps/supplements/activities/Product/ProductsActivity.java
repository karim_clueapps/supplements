package com.clueapps.supplements.activities.Product;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.ProductAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Cart.CartActivity;
import com.clueapps.supplements.adapters.ProductsAdapter;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Category;
import com.clueapps.supplements.models.Data.PaginatorData;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.models.Data.Store;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsActivity extends AppCompatActivity {

    public RecyclerView productsRecycler;
    public ProductsAdapter productsAdapter;

    public ArrayList<Product> productArrayList = new ArrayList<>();
    ImageView backImv, cartImv, imageImv;
    private TabLayout tabLayout;
    TextView nameTxv;
    public ArrayList<Store> storeArrayList = new ArrayList<>();
    Category category;
    Store store;
    PaginatorData paginatorData = new PaginatorData();
    boolean firstTime = true;
    boolean firstAll = true;

    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        ButterKnife.bind(this);

        initData();
        initView();
        listeners();
        prepareView();
    }

    public void initData() {

        category = getIntent().getBundleExtra("data").getParcelable("category");
        store = getIntent().getBundleExtra("data").getParcelable("store");

    }

    public void initView() {

        collapsingToolbar.requestLayout();
        paginatorData.refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        paginatorData.progressMoreDataRel = (RelativeLayout) findViewById(R.id.progressMoreDataRel);
        paginatorData.recyclerView = (RecyclerView) findViewById(R.id.products_recycler);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        productsRecycler = (RecyclerView) findViewById(R.id.products_recycler);
        productsAdapter = new ProductsAdapter(this, productArrayList, false);


        backImv = (ImageView) findViewById(R.id.back_imv);
        cartImv = (ImageView) findViewById(R.id.cart_imv);
        imageImv = (ImageView) findViewById(R.id.image_imv);
        nameTxv = (TextView) findViewById(R.id.name_txv);
        productsRecycler.setHasFixedSize(true);

        productsRecycler.setAdapter(productsAdapter);

    }

    public void listeners() {

        paginatorData.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetProductsView();
                if (store == null)
                    ProductAPI.getProductsByCategory(ProductsActivity.this, productsAdapter, category.getId(), productArrayList, paginatorData);
                else
                    ProductAPI.getProducts(ProductsActivity.this, productsAdapter, store.getId(), productArrayList, paginatorData);
            }
        });

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        productsRecycler.setLayoutManager(gridLayoutManager);

        productsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = gridLayoutManager.getChildCount();
                int totalItemCount = gridLayoutManager.getItemCount();
                int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();

                if (!paginatorData.loading && !paginatorData.empty) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= Utility.take) {
                        if (store == null)
                            ProductAPI.getProductsByCategory(ProductsActivity.this, productsAdapter, category.getId(), productArrayList, paginatorData);
                        else
                            ProductAPI.getProducts(ProductsActivity.this, productsAdapter, store.getId(), productArrayList, paginatorData);
                    }
                }

            }
        });


        backImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cartImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(ProductsActivity.this, CartActivity.class, null);
            }
        });


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (!paginatorData.loading) {
                    productArrayList.clear();
                    productsAdapter.notifyDataSetChanged();
                    paginatorData.page = 1;
                    paginatorData.loading = false;
                    paginatorData.empty = false;
                    if (!firstTime) {
                        if (tab.getPosition() != 0) {
                            store = storeArrayList.get(tab.getPosition());
                            ProductAPI.getProducts(ProductsActivity.this, productsAdapter, store.getId(), productArrayList, paginatorData);
                        } else {
                            store = null;
                            ProductAPI.getProductsByCategory(ProductsActivity.this, productsAdapter, category.getId(), productArrayList, paginatorData);
                        }
                    }
                }
                firstTime = false;
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                if (!paginatorData.loading) {
                    productArrayList.clear();
                    productsAdapter.notifyDataSetChanged();
                    paginatorData.page = 1;
                    paginatorData.loading = false;
                    paginatorData.empty = false;

                    if (firstAll) {
                        if (tab.getPosition() != 0) {
                            store = storeArrayList.get(tab.getPosition());
                            ProductAPI.getProducts(ProductsActivity.this, productsAdapter, store.getId(), productArrayList, paginatorData);
                        } else {
                            store = null;
                            ProductAPI.getProductsByCategory(ProductsActivity.this, productsAdapter, category.getId(), productArrayList, paginatorData);
                        }
                    }
                    if (tab.getPosition() == 0)
                        firstAll = false;
                }

            }
        });
    }


    public void prepareView() {
        CustomHeader.changeCartNum(this);

        tabLayout.removeAllTabs();

        Utility.downloadImage(this, category.getImage(), R.drawable.blackplaceholder, imageImv);

        collapsingToolbar.setTitle(category.getName());

        storeArrayList.clear();

        Store tmpStore = new Store();
        tmpStore.setName(getString(R.string.all));
        storeArrayList.add(tmpStore);

        Utility.setTabs(storeArrayList, tabLayout);

        ProductAPI.getStoresTabs(this, tabLayout, category.getId(), store, storeArrayList, paginatorData);


        if (store == null)
            ProductAPI.getProductsByCategory(ProductsActivity.this, productsAdapter, category.getId(), productArrayList, paginatorData);
        else
            ProductAPI.getProducts(ProductsActivity.this, productsAdapter, store.getId(), productArrayList, paginatorData);
    }

    public void resetProductsView() {
        productArrayList.clear();
        productsAdapter.notifyDataSetChanged();
        paginatorData.page = 1;
        paginatorData.loading = false;
        paginatorData.empty = false;
    }


    @Override
    protected void onResume() {
        super.onResume();


        try {
            productsAdapter.notifyDataSetChanged();
        } catch (Exception e) {

        }
        CustomHeader.changeCartNum(this);
    }

    @Override
    protected void onStop() {
        firstTime = true;
        firstAll = true;
        super.onStop();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1233 && resultCode == RESULT_OK) {
            Bundle bundle = data.getBundleExtra("data");

            if (bundle != null) {
                Product product = bundle.getParcelable("product");
                productsAdapter.UpdateItem(product);
            }
        }

    }
}
