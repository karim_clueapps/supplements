package com.clueapps.supplements.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.clueapps.supplements.R;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;

public class ChangeLanguageActivity extends AppCompatActivity {
    RelativeLayout enLayout, arLayout;
    SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_language);

        initData();
        initView();
        listeners();
    }
    public void initData(){
        Utility.setCurrentActivity(this);

        sharedPref = new SharedPref(this);
    }
    public void initView(){

        arLayout = (RelativeLayout) findViewById(R.id.ar_layout);
        enLayout = (RelativeLayout) findViewById(R.id.en_layout);

    }
    public void listeners(){

        arLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPref.setLang("ar");
                sharedPref.putBoolean("first_time",false);
                IntentClass.goToActivityAndClear(ChangeLanguageActivity.this,HomeActivity.class,null);
                finish();
            }
        });

        enLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPref.setLang("en");
                sharedPref.putBoolean("first_time",false);
                IntentClass.goToActivityAndClear(ChangeLanguageActivity.this,HomeActivity.class,null);
                finish();
            }
        });
    }

}
