package com.clueapps.supplements.activities.User;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.clueapps.supplements.API.UserAPI;
import com.clueapps.supplements.Holders.UserDataErrorViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.adapters.SpinnersAdapter;
import com.clueapps.supplements.helpers.DatePicker;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.helpers.Validations;
import com.clueapps.supplements.models.Data.Store;
import com.clueapps.supplements.models.Data.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity {

    private EditText fNameEdt, lNameEdt, emailEdt ,birthEdt, passwordEdt,mobileEdt;
    public View showDate ;
    SharedPref sharedPref;
    List<String> genderList;
    boolean fromCart ;
    Store store;

    @BindView(R.id.login_txv) TextView loginTxv ;
    @BindView(R.id.gender_spinner) Spinner genderSpinner;

    UserDataErrorViewHolder holder ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);
        initData();
        initView();
        listeners();
    }

    public void initData(){
        Bundle bundle = getIntent().getBundleExtra("data") ;

        if ( bundle != null ) {
            fromCart = bundle.getBoolean("fromCart");
            store = bundle.getParcelable("store");

        }

        Utility.setCurrentActivity(this);
        sharedPref = new SharedPref(getApplicationContext());
    }
    public void initView() {

        holder = new UserDataErrorViewHolder(this);

        genderList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.gender)));
        SpinnersAdapter genderAdapter = new SpinnersAdapter(genderList,this,R.color.black);
        genderSpinner.setAdapter(genderAdapter);

        LinearLayout firstNameLayout = (LinearLayout) findViewById(R.id.first_name_layout);
        fNameEdt = (EditText) firstNameLayout.findViewById(R.id.input_edt);
        Utility.setUserRawView(this, firstNameLayout , R.drawable.firtstnameicon ,fNameEdt, getString(R.string.first_name)+"*"  , "");

        LinearLayout lastNameLayout = (LinearLayout) findViewById(R.id.last_name_layout);
        lNameEdt = (EditText) lastNameLayout.findViewById(R.id.input_edt);

        Utility.setUserRawView(this, lastNameLayout , R.drawable.firtstnameicon ,lNameEdt, getString(R.string.last_name) , "");

        LinearLayout emailLayout = (LinearLayout) findViewById(R.id.email_layout);
        emailEdt = (EditText) emailLayout.findViewById(R.id.input_edt);
        emailEdt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        Utility.setUserRawView(this, emailLayout , R.drawable.emailiconwhite ,emailEdt, getString(R.string.email)+"*" , "");


        LinearLayout mobileLayout = (LinearLayout) findViewById(R.id.mobile_layout);
        mobileEdt = (EditText) mobileLayout.findViewById(R.id.input_edt);
        mobileEdt.setInputType(InputType.TYPE_CLASS_NUMBER);

        mobileEdt.setImeOptions(EditorInfo.IME_ACTION_NONE);
        Utility.setUserRawView(this, mobileLayout , R.drawable.phonenumbericon ,mobileEdt,getString(R.string.mobile)+"*"  , "");


        LinearLayout birthLayout = (LinearLayout) findViewById(R.id.birth_layout);
        birthEdt = (EditText) birthLayout.findViewById(R.id.input_edt);
        birthEdt.setEnabled(false);
        showDate = birthLayout.findViewById(R.id.click_view);
        Utility.setUserRawView(this, birthLayout , R.drawable.birthdaywhite ,birthEdt, getString(R.string.birth_of_date) , "");


        LinearLayout passLayout = (LinearLayout) findViewById(R.id.password_layout);
        passwordEdt = (EditText) passLayout.findViewById(R.id.input_edt);
        Utility.setUserRawView(this, passLayout , R.drawable.passwordicon ,passwordEdt,getString(R.string.password)+"*" , "");

    }
    public void listeners() {
        loginTxv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        showDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePicker.chooseDate(view,SignUpActivity.this);
            }
        });
        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                view.setBackgroundColor(getResources().getColor(R.color.transparent));
                TextView spinnerTxv = (TextView) view.findViewById(R.id.spinner_txv);
                spinnerTxv.setTextColor(getResources().getColor(R.color.white));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

     @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 105:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    validateInput();
                } else {
                    Toast.makeText(this, "Please Enable Phone permission to Continue", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    /**
     * This method is used to validate user data.
     * called when user click signup button.
     */
    @OnClick(R.id.signup_btn)
    public void validateInput(){

        holder.errorEmailTxv.setText(getString(R.string.valid_email));
        holder.errorMobTxv.setText(getString(R.string.valid_mobile));


        User user = new User() ;
        String email = emailEdt.getText().toString();
        String gender = genderSpinner.getSelectedItemPosition() == 0 ? "" : genderList.get(genderSpinner.getSelectedItemPosition());
        String fName = fNameEdt.getText().toString();
        String lName = lNameEdt.getText().toString();
        String mobile = mobileEdt.getText().toString();
        String birth = birthEdt.getText().toString();
        String password = passwordEdt.getText().toString().trim();

        user.setfName(fName);
        user.setPassword(password);
        user.setEmail(email);
        user.setlName(lName);
        user.setMobile(mobile);
        user.setGender(gender.toLowerCase());
        user.setBirth(birth);
        user.setLanguage(sharedPref.getLang());

        boolean validated = true;


        validated = validated&&Validations.isValidName(fName);
        Validations.animateNameView(fName, holder.errorFNameTxv);

        if ( lName.length() > 0 ) {
            validated = validated && Validations.isValidName(lName);
            Validations.animateNameView(lName, holder.errorLNameTxv);
        }


        validated = validated&&Validations.isValidMobile(mobile);
        Validations.animateMobileView(mobile,holder.errorMobTxv);

        validated = validated&&Validations.isValidEmail(email);
        Validations.animateEmailView(email,holder.errorEmailTxv);

        validated = validated&&Validations.isValidPassword(password);
        Validations.animatePasswordView(password,holder.errorPassTxv);


        if(validated) {
            if (ContextCompat.checkSelfPermission(SignUpActivity.this,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(SignUpActivity.this,
                        new String[]{Manifest.permission.READ_PHONE_STATE},
                        105);
            } else {
                 UserAPI.signUpApi(this,user,fromCart,store,holder);
            }
        }
    }

    @Override
    protected Dialog onCreateDialog(int id)
    {
        switch (id) {
            case 0:
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int  month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(this, R.style.DatePickerStyle,datePickerListener, year, month,day);
                dialog.getDatePicker().setMaxDate(new Date().getTime());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    // Call some material design APIs here
                } else {
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }
                return dialog;

        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {

            // set selected date into textview

            String dateStr = new StringBuilder().append(year)
                    .append("-").append( DatePicker.pad(month+1)).append("-").append(DatePicker.pad(dayOfMonth))
                    .toString();

            birthEdt.setText(dateStr);

        }

    };
}
