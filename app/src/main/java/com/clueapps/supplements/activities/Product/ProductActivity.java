package com.clueapps.supplements.activities.Product;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.ProductAPI;
import com.clueapps.supplements.Holders.ProductDetailsViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.adapters.ImagesAdapter;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.models.Data.ImagePath;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.realm.RealmController;

import java.util.ArrayList;

public class ProductActivity extends AppCompatActivity {


    ImageView backImv, shareImv;
    TextView quantityViewTxv;
    RelativeLayout changeQuantityLayout, addCartLayout, increaseLayout, decreaseLayout;
    ProductDetailsViewHolder holder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        initData();
        initView();
        listener();
    }


    public void initData() {

        holder = new ProductDetailsViewHolder(this);

        Bundle bundle = getIntent().getBundleExtra("data");

        if (bundle != null) {
            holder.product = bundle.getParcelable("product");
        }

        if (holder.product == null) {
            Intent appLinkIntent = getIntent();
            try {
                Uri appLinkData = appLinkIntent.getData();
                holder.product = new Product();
                holder.product.setId(appLinkData.getLastPathSegment());
            } catch (Exception e) {
            }
        }

    }

    public void initView() {

        shareImv = (ImageView) findViewById(R.id.share_imv);
        backImv = (ImageView) findViewById(R.id.back_imv);
        changeQuantityLayout = (RelativeLayout) findViewById(R.id.changeQuantity_layout);
        holder.addCartBtn = (Button) findViewById(R.id.addCart_btn);
        addCartLayout = (RelativeLayout) findViewById(R.id.addCart_layout);
        decreaseLayout = (RelativeLayout) findViewById(R.id.decrease_layout);
        increaseLayout = (RelativeLayout) findViewById(R.id.increase_layout);
        quantityViewTxv = (TextView) findViewById(R.id.quantityView_txv);

        holder.favImv.setImageResource(holder.product.isWishlisted() ? R.drawable.heartwhitefill : R.drawable.hearticonwhitenofill);

        holder.imagePaths = holder.product.getImages() != null ? holder.product.getImages() : new ArrayList<ImagePath>();
        if (holder.imagePaths.size() == 0) {
            holder.product.getImages().add(new ImagePath(holder.product.getIcon()));
        }
        if (new SharedPref(this).getLang().equals("ar")) {
            holder.imagesPager.setRotation(180);
        }

        holder.imagesAdapter = new ImagesAdapter(this, holder.imagePaths);
        holder.imagesPager.setAdapter(holder.imagesAdapter);
        holder.dotsTab.setupWithViewPager(holder.imagesPager, true);

    }

    public void listener() {

        backImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        holder.favImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (new SharedPref(ProductActivity.this).getToken().length() > 0) {

                    if (!holder.product.isWishlisted()) {
                        holder.favImv.setImageResource(R.drawable.heartwhitefill);
                        ProductAPI.addToWishlist(ProductActivity.this, holder.product.getId());
                    } else {
                        holder.favImv.setImageResource(R.drawable.hearticonwhitenofill);
                        ProductAPI.removeFromWishlist(ProductActivity.this, holder.product.getId());
                    }

                    holder.product.setWishlisted(!holder.product.isWishlisted());
                } else
                    Dialogs.showLoginDialog(ProductActivity.this);

            }
        });

        shareImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goSharedata(ProductActivity.this,
                        "Hey check out this supplement : " + "\n" + holder.product.getName() + "\n" + "http://supplement.com/product/" + holder.product.getId(),
                        "");
            }
        });
        holder.addCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.product.setQuantityCart(1);
                RealmController.addToCart(holder.product);
                changeQuantityLayout.setVisibility(View.VISIBLE);
                addCartLayout.setVisibility(View.GONE);
                quantityViewTxv.setText(1 + "");
            }
        });

        increaseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.product.getQuantityCart() < holder.product.getQuantity()) {

                    holder.product.setQuantityCart((holder.product.getQuantityCart() + 1));
                    quantityViewTxv.setText(holder.product.getQuantityCart() + "");
                    RealmController.updateProduct(holder.product);
                } else
                    Dialogs.showToast(getString(R.string.quantity_max), ProductActivity.this);
            }
        });

        decreaseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.product.getQuantityCart() == 1)
                    showConfirmRemoveDialog();
                else {
                    holder.product.setQuantityCart((holder.product.getQuantityCart() - 1));
                    quantityViewTxv.setText(holder.product.getQuantityCart() + "");
                    RealmController.updateProduct(holder.product);
                }
            }
        });

        holder.imagesAdapter.setOnClickListener(new ImagesAdapter.onClickListener() {
            @Override
            public void onClick(int position) {
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("images", holder.product.getImages());
                bundle.putInt("position", position);
                bundle.putParcelable("product", holder.product);
                IntentClass.goToActivity(ProductActivity.this, ImagesActivity.class, bundle);

            }
        });

    }

    public void showConfirmRemoveDialog() {
        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_confirm_remove);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Button okBtn = (Button) dialog.findViewById(R.id.ok_btn);
        Button cancelBtn = (Button) dialog.findViewById(R.id.cancel_btn);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RealmController.removeFromCart(holder.product.getId());
                addCartLayout.setVisibility(View.VISIBLE);
                changeQuantityLayout.setVisibility(View.GONE);
                dialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }


    public void prepareView() {

        if (holder.product.getName() != null)
            holder.titleTxv.setText(holder.product.getName());

        if (RealmController.getProduct(holder.product.getId()) != null) {
            changeQuantityLayout.setVisibility(View.VISIBLE);
            addCartLayout.setVisibility(View.GONE);
            holder.product = RealmController.getProduct(holder.product.getId());
            quantityViewTxv.setText(holder.product.getQuantityCart() + "");
        }

        ProductAPI.getProduct(this, holder);
    }

    @Override
    protected void onResume() {
        prepareView();
        super.onResume();
    }

    @Override
    public void onBackPressed() {


        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putParcelable("product", holder.product);
        intent.putExtra("data", bundle);
        setResult(RESULT_OK, intent);


        super.onBackPressed();
    }

}
