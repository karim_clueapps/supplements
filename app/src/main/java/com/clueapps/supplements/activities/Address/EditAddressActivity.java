package com.clueapps.supplements.activities.Address;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.clueapps.supplements.API.API;
import com.clueapps.supplements.API.AddressAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.adapters.SpinnersAdapter;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.helpers.Validations;
import com.clueapps.supplements.models.Data.Address;
import com.clueapps.supplements.models.Data.City;
import com.clueapps.supplements.models.Data.Country;
import com.clueapps.supplements.models.responses.DataCitiesResponse;
import com.clueapps.supplements.models.responses.DataCountriesResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EditAddressActivity extends AppCompatActivity {
    EditText blockEdt, streetEdt , buildingEdt , floorEdt ;
    Button addBtn ;

    Spinner citySpinner , countrySpinner;
    List<String> cityList , countryList;

    ArrayList<Country> countriesArray ;
    ArrayList<City> citiesArray ;
    @BindView(R.id.block_error_txv) TextView blockErrorTxv;
    @BindView(R.id.building_error_txv) TextView buildingErrorTxv ;
    @BindView(R.id.country_error_txv) TextView countryErrorTxv ;
    @BindView(R.id.city_error_txv) TextView cityErrorTxv ;
    @BindView(R.id.street_error_txv) TextView streetErrorTxv ;
    Address address ;
    @BindView(R.id.add_btn) Button updateBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        ButterKnife.bind(this);
        initData();
        initView();
        listeners();
        prepareView();
    }

    public void initData(){
        Bundle bundle = getIntent().getBundleExtra("data");
        if ( bundle != null ){
            address = bundle.getParcelable("address");
        }
    }

    public void initView(){
        updateBtn.setText(getString(R.string.update_address));
        CustomHeader.setToolbar(this,getString(R.string.add_address_str));

        RelativeLayout countryLayout = (RelativeLayout) findViewById(R.id.country_layout);
        TextView countryTitleTxv = (TextView) countryLayout.findViewById(R.id.title_txv);
        countryTitleTxv.setText(Utility.setText(this,getString(R.string.country),"*") );
        countrySpinner = (Spinner) countryLayout.findViewById(R.id.spinner);
        countryList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.country)));
        SpinnersAdapter countryAdapter = new SpinnersAdapter(countryList,this,R.color.black);
        countrySpinner.setAdapter(countryAdapter);


        RelativeLayout cityLayout = (RelativeLayout) findViewById(R.id.city_layout);
        TextView cityTitleTxv = (TextView) cityLayout.findViewById(R.id.title_txv);
        cityTitleTxv.setText(Utility.setText(this,getString(R.string.city),"*"));
        citySpinner = (Spinner) cityLayout.findViewById(R.id.spinner);
        cityList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.city)));
        SpinnersAdapter genderAdapter = new SpinnersAdapter(cityList,this,R.color.black);
        citySpinner.setAdapter(genderAdapter);


        RelativeLayout areaLayout = (RelativeLayout) findViewById(R.id.block_layout);
        TextView areaTxv = (TextView) areaLayout.findViewById(R.id.title_txv);
        blockEdt = (EditText) areaLayout.findViewById(R.id.input_edt);
        Utility.setAddressRawView(areaTxv, blockEdt,getString(R.string.enter_block),Utility.setText(this,getString(R.string.block),"*"));

        RelativeLayout streetLayout = (RelativeLayout) findViewById(R.id.street_layout);
        TextView streetTxv = (TextView) streetLayout.findViewById(R.id.title_txv);
        streetEdt = (EditText) streetLayout.findViewById(R.id.input_edt);
        Utility.setAddressRawView(streetTxv,streetEdt,getString(R.string.enter_street),Utility.setText(this,getString(R.string.street),"*"));


        RelativeLayout buildingLayout = (RelativeLayout) findViewById(R.id.building_layout);
        TextView buildingTxv = (TextView) buildingLayout.findViewById(R.id.title_txv);
        buildingEdt = (EditText) buildingLayout.findViewById(R.id.input_edt);
        Utility.setAddressRawView(buildingTxv,buildingEdt,getString(R.string.enter_building_num),Utility.setText(this,getString(R.string.building_num),"*") );


        RelativeLayout floorLayout = (RelativeLayout) findViewById(R.id.floor_layout);
        TextView floorTxv = (TextView) floorLayout.findViewById(R.id.title_txv);
        floorEdt = (EditText) floorLayout.findViewById(R.id.input_edt);
        Utility.setAddressRawView(floorTxv,floorEdt,getString(R.string.enter_floor_num),getString(R.string.floor_num));

        addBtn = (Button) findViewById(R.id.add_btn);


    }

    public void listeners(){
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0){
                    getCities(countriesArray.get(position-1).getCode());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public void prepareView(){

        if ( address != null ) {
            streetEdt.setText(address.getStreet());
            buildingEdt.setText(address.getBuildingNum());
            floorEdt.setText(address.getFloorNum() != null ? address.getFloorNum() : "");
            blockEdt.setText(address.getBlock());
        }
        // get countries and fill it in spinner
        getCountries();
    }

    /**
     * This method is used to validate address data.
     * called when user click add button.
     */
    @OnClick(R.id.add_btn)
    public void validateInput(){

        String area = blockEdt.getText().toString();
        String city = cityList.get(citySpinner.getSelectedItemPosition()); 
        String country = countryList.get(countrySpinner.getSelectedItemPosition());
        String cityId = citiesArray.get(citySpinner.getSelectedItemPosition()-1).getId();
        String countryId = countriesArray.get(countrySpinner.getSelectedItemPosition()-1).getId();
        String building = buildingEdt.getText().toString();
        String floor = floorEdt.getText().toString();
        String street = streetEdt.getText().toString();


        address.setBlock(area);
        address.setCountry(country);
        address.setCity(city);
        address.setBuildingNum(building);
        address.setFloorNum(floor);
        address.setStreet(street);
        address.setCityId(cityId);
        address.setCountryId(countryId);

        boolean validated = true;


        validated = validated&& Validations.isValidStr(area);
        Validations.animateStrView(area, blockErrorTxv);

        validated = validated&&Validations.isValidStr(building);
        Validations.animateStrView(building, buildingErrorTxv);

        validated = validated&&Validations.isValidStr(street);
        Validations.animateStrView(street,streetErrorTxv);

        validated = validated&&Validations.isValidSpinnerItem(countrySpinner.getSelectedItemPosition());
        Validations.animateSpinnerView(countrySpinner.getSelectedItemPosition(),countryErrorTxv);

        validated = validated&&Validations.isValidSpinnerItem(citySpinner.getSelectedItemPosition());
        Validations.animateSpinnerView(citySpinner.getSelectedItemPosition(),cityErrorTxv);


        //  when validated is true that mean the data is valid
        if(validated) {
            // edit address using api
            AddressAPI.editAddress(this,address);
        }
    }

    /**
     * This method is used to get countries.
     * called when user open activity.
     */
    public void getCountries(){

        // disable city spinner until choose country
        citySpinner.setEnabled(false);

        API.getCountries(this).enqueue(new Callback<DataCountriesResponse>() {
            @Override
            public void onResponse(Call<DataCountriesResponse> call, final Response<DataCountriesResponse> response) {

                switch (response.code())
                {
                    case 200:

                        if(response.body()!=null) {
                            countriesArray = response.body().getCountriesResponse().getCountries();
                            countryList = new ArrayList();
                            // put msg in first item
                            countryList.add(getString(R.string.select_country));

                            // put countries in string array to fill it in spinner
                            for (Country country : countriesArray )
                                countryList.add(country.getName());

                            // fill spinner
                            SpinnersAdapter countriesAdapter = new SpinnersAdapter(countryList,EditAddressActivity.this,R.color.black);
                            countrySpinner.setAdapter(countriesAdapter);

                            // set saved country in address object
                            if ( address != null )
                                Utility.setSpinner(address.getCountry(), countrySpinner, countryList);

                            int pos = Utility.getCountry(address.getCountry(), countryList);
                            // get cities of saved country
                            getCities( countriesArray.get(pos).getId() );


                        }

                        break;
                    default:
                        Dialogs.showToast(getResources().getString(R.string.something_wrong),EditAddressActivity.this);



                }
            }

            @Override
            public void onFailure(Call<DataCountriesResponse> call, Throwable t) {
                Log.e("RetrofitFailure",t.toString());
                Dialogs.showToast(getResources().getString(R.string.no_connection_str),EditAddressActivity.this);
            }
        });
    }

    /**
     * This method is used to get cities of country .
     * called when user select country .
     * @param countryCode to get cities by it
     */

    public void getCities(String countryCode) {

        Dialogs.customProgDialog(this);

        API.getCities(this,countryCode).enqueue(new Callback<DataCitiesResponse>() {
            @Override
            public void onResponse(Call<DataCitiesResponse> call, final Response<DataCitiesResponse> response) {
                Dialogs.dismissDialog();
                switch (response.code())
                {
                    case 200:

                        if(response.body()!=null) {
                            citiesArray = response.body().getCitiesResponse().getCities();
                            cityList = new ArrayList();
                            // put msg in first item
                            cityList.add(getString(R.string.select_city));

                            // put cities in string array to fill it in spinner
                            for (City city : citiesArray )
                                cityList.add(city.getName());

                            // fill spinner
                            SpinnersAdapter countriesAdapter = new SpinnersAdapter(cityList,EditAddressActivity.this,R.color.black);
                            citySpinner.setAdapter(countriesAdapter);

                            // enable spinner after get cities
                            citySpinner.setEnabled(true);

                            // set saved city in address object
                            if ( address != null )
                                Utility.setSpinner(address.getCity(),citySpinner,cityList);

                        }

                        break;
                    default:
                        Dialogs.showToast(getResources().getString(R.string.something_wrong),EditAddressActivity.this);



                }
            }

            @Override
            public void onFailure(Call<DataCitiesResponse> call, Throwable t) {
                Log.e("RetrofitFailure",t.toString());
                Dialogs.dismissDialog();
                Dialogs.showToast(getResources().getString(R.string.no_connection_str),EditAddressActivity.this);
            }
        });
    }

}
