package com.clueapps.supplements.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.UserAPI;
import com.clueapps.supplements.Fragments.AboutAppFragment;
import com.clueapps.supplements.Fragments.AboutDevFragment;
import com.clueapps.supplements.Fragments.ContactUsFragment;
import com.clueapps.supplements.Fragments.HomeFragment;
import com.clueapps.supplements.Fragments.ProfileFragment;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Cart.CartActivity;
import com.clueapps.supplements.activities.Order.OrdersActivity;
import com.clueapps.supplements.activities.Product.SearchActivity;
import com.clueapps.supplements.activities.User.LoginActivity;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.EventBusModels.MessageEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

public class HomeActivity extends AppCompatActivity {

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private Toolbar toolbar;

    public static int navItemIndex = 0;


    private static final String TAG_HOME = "home";
    private static final String TAG_ABOUT_APP = "about_app";
    private static final String TAG_PROFILE = "profile";
    private static final String TAG_LOGIN = "login";
    private static final String TAG_CHANGE_LANGUAGE = "change_language";
    private static final String TAG_ABOUT_DEV = "about_dev";
    private static final String TAG_SHARE_APP = "share_app";
    private static final String TAG_CONTACT_US = "contact_us";
    private static final String TAG_LOGOUT = "logout";
    public static String CURRENT_TAG = TAG_HOME;
    int lang;
    private String[] activityTitles;
    TextView titleTxv;
    private Handler mHandler;
    private boolean shouldLoadHomeFragOnBackPress = true;
    SharedPref sharedPref ;
    ImageView cartImv ,searchIconImv;
    public RelativeLayout progressRel,progressMoreDataRel;

    int type = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkLang();

        setContentView(R.layout.activity_home);
        initData();
        initView(savedInstanceState);
        listeners();
    }
    public void checkLang() {
        sharedPref = new SharedPref(this);
        Utility.setCurrentActivity(this);

        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();

        if (sharedPref.getLang().equals("ar"))
            conf.setLocale(new Locale("ar"));
        else
            conf.setLocale(new Locale("en"));

        res.updateConfiguration(conf, dm);
    }
    public void listeners(){

        cartImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(HomeActivity.this,CartActivity.class,null);
            }
        });

        searchIconImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(HomeActivity.this,SearchActivity.class,null);
            }
        });

    }

    public void initData(){
        sharedPref = new SharedPref(this);
        Utility.setCurrentActivity(this);
        Bundle bundle = getIntent().getBundleExtra("data") ;
        if ( bundle != null ) {
            type = bundle.getInt("type");
            if (type == 1)
                IntentClass.goToActivity(this, OrdersActivity.class, null);
        }
    }
    public void initView(Bundle savedInstanceState){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        titleTxv = (TextView) findViewById(R.id.title_txv);
        cartImv = (ImageView) findViewById(R.id.cart_imv);
        searchIconImv = (ImageView) findViewById(R.id.searchIcon_imv);

        if( sharedPref.getToken().length() > 0 ) {
            navigationView.inflateMenu(R.menu.activity_main_drawer_logged_in);
        }
        else {
            navigationView.inflateMenu(R.menu.activity_main_drawer_not_logged_in);
        }

        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEventListner(MessageEvent event)
    {

        String type = event.getMessage().getData().get("type");

        if ( type == "1" )
            IntentClass.goToActivity(this, OrdersActivity.class,null);

    }

    public void prepareView(){
        CustomHeader.changeCartNum(this);
    }
    private void loadHomeFragment() {
        selectNavMenu();

        setToolbarTitle();

        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            return;
        }

        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();
            return;
        }

        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }
        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar_cart menu
        invalidateOptionsMenu();

        getHomeFragment();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                return new HomeFragment();
            case 1:
                if ( sharedPref.getToken().length() > 0 )
                    return new ProfileFragment();
                else
                    return new HomeFragment();
            case 3:
                return new AboutAppFragment();
            case 4:
                return new ContactUsFragment();
            case 5:
                return new AboutDevFragment();

        }
        return new HomeFragment();

    }

    private void setToolbarTitle() {
        titleTxv.setText(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {

        navigationView.setItemIconTintList(null);

        if ( sharedPref.getToken().length() > 0 ) {
            navigationView.getMenu().getItem(2).setCheckable(false);
            navigationView.getMenu().getItem(6).setCheckable(false);
            navigationView.getMenu().getItem(7).setCheckable(false);
        }else {
            navigationView.getMenu().getItem(1).setCheckable(false);
            navigationView.getMenu().getItem(2).setCheckable(false);
            navigationView.getMenu().getItem(6).setCheckable(false);
        }
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;
                    case R.id.nav_profile:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_PROFILE;
                        break;

                    case R.id.nav_login:

                        IntentClass.goToActivity(HomeActivity.this, LoginActivity.class, null);

                        CURRENT_TAG = TAG_LOGIN;
                        break;

                    case R.id.nav_change_lang:
                        showChangeLangDialog();
                        CURRENT_TAG = TAG_CHANGE_LANGUAGE;
                        break;

                    case R.id.nav_about_app:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_ABOUT_APP;
                        break;

                    case R.id.nav_contact_us:
                        navItemIndex = 4;
                        CURRENT_TAG = TAG_CONTACT_US;
                        break;

                    case R.id.nav_about_dev:
                        navItemIndex = 5;
                        CURRENT_TAG = TAG_ABOUT_DEV;
                        break;

                    case R.id.nav_share_app:
                        String url = "https://play.google.com/store/apps/details?id=" +  getApplicationContext().getPackageName();;
                        IntentClass.goSharedata(HomeActivity.this,
                                "Hey check out my app at:"+ url,"");
                        CURRENT_TAG = TAG_SHARE_APP;
                        break;

                    case R.id.nav_logout:
                        showLogoutDialog();
                        CURRENT_TAG = TAG_LOGOUT;
                        break;

                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }

                if (menuItem.isCheckable())
                //if ( menuItem.isCheckable() && ((sharedPref.getToken().length() != 0 && CURRENT_TAG.equals(TAG_PROFILE)) || !CURRENT_TAG.equals(TAG_PROFILE)) )
                    loadHomeFragment();

                return true;
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        actionBarDrawerToggle.setHomeAsUpIndicator(R.drawable.drawericon);
        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                    View view2 = getCurrentFocus();
                    if (view2 != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
            }
        });


        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    public void showLogoutDialog(){

        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Button  okBtn = (Button) dialog.findViewById(R.id.ok_btn);
        Button  cancelBtn = (Button) dialog.findViewById(R.id.cancel_btn);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPref.clearUserData();
                UserAPI.logout(HomeActivity.this);
                IntentClass.goToActivityAndClear(HomeActivity.this, HomeActivity.class,null);
                dialog.dismiss();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }
    public void showChangeLangDialog(){
        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_change_lang);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        RelativeLayout cancelLayout =  (RelativeLayout) dialog.findViewById(R.id.cancel_layout);
        RelativeLayout confirmLayout =  (RelativeLayout) dialog.findViewById(R.id.confirm_layout);
        final CheckBox arCheckbox = (CheckBox) dialog.findViewById(R.id.ar_checkbox);
        final CheckBox enCheckbox = (CheckBox) dialog.findViewById(R.id.en_checkbox);
        LinearLayout enLayout =  (LinearLayout) dialog.findViewById(R.id.en_layout);
        LinearLayout arLayout =  (LinearLayout) dialog.findViewById(R.id.ar_layout);


        lang = sharedPref.getLang().equals("en") ? 0 : 1 ;

        enCheckbox.setChecked( lang == 0 );
        arCheckbox.setChecked( lang == 1 );


        arCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arCheckbox.setChecked(true);
                enCheckbox.setChecked(false);
                lang = 1 ;
            }
        });

        arLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arCheckbox.setChecked(true);
                enCheckbox.setChecked(false);
                lang = 1 ;
            }
        });

        enCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                enCheckbox.setChecked(true);
                arCheckbox.setChecked(false);
                lang = 0 ;
            }
        });


        enLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                enCheckbox.setChecked(true);
                arCheckbox.setChecked(false);
                lang = 0 ;
            }
        });


        confirmLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPref.setLang(lang == 0 ? "en" : "ar");
                IntentClass.goToActivityAndClear(HomeActivity.this,HomeActivity.class,null);
            }
        });

        cancelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }


    @Override
    protected void onResume() {

        prepareView();

        super.onResume();
    }
}
