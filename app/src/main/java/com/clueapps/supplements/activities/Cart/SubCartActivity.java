package com.clueapps.supplements.activities.Cart;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.User.LoginActivity;
import com.clueapps.supplements.adapters.SubCartItemsAdapter;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.models.Data.Store;
import com.clueapps.supplements.realm.RealmController;

import java.util.ArrayList;

public class SubCartActivity extends AppCompatActivity {

    RecyclerView productsRecycler;
    public SubCartItemsAdapter subCartItemsAdapter;
    public RelativeLayout progressRel,progressMoreDataRel;
    public LinearLayout noResultLayout;
    public static  TextView totalPriceTxv , totalCostTxv;
    TextView  delFeesTxv;
    ImageView backImv , cartImv ;
    public ArrayList<Product> productArrayList =new ArrayList<>();
    public Store store;
    Button deliveryDetailsBtn ;
    public static Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_cart);

        initData();
        initView();
        listeners();
        prepareView();
    }

    public void initData(){

         activity = this;

        Bundle bundle = getIntent().getBundleExtra("data");

        if ( bundle != null ){
            store = bundle.getParcelable("store");
        }
        CustomHeader.setToolbar(this,store.getName());

        productArrayList = RealmController.getStoreProducts(store.getId());

    }

    public void initView() {


        productsRecycler = (RecyclerView) findViewById(R.id.products_recycler);

        subCartItemsAdapter =new SubCartItemsAdapter(this, productArrayList,store);
        progressRel=(RelativeLayout)findViewById(R.id.progressRel);
        progressMoreDataRel =(RelativeLayout)findViewById(R.id.progressMoreDataRel);
        noResultLayout=(LinearLayout) findViewById(R.id.no_result_relative);
        backImv =(ImageView) findViewById(R.id.back_imv);
        cartImv =(ImageView) findViewById(R.id.cart_imv);
        deliveryDetailsBtn  =(Button) findViewById(R.id.deliveryDetails_btn);
        totalPriceTxv = (TextView) findViewById(R.id.totalPrice_txv);
        delFeesTxv = (TextView) findViewById(R.id.delFees_txv);
        totalCostTxv = (TextView) findViewById(R.id.totalCost_txv);

        productsRecycler.setHasFixedSize(true);

        productsRecycler.setAdapter(subCartItemsAdapter);


        final GridLayoutManager gridLayoutManager =  new GridLayoutManager(this,2);
        productsRecycler.setLayoutManager(gridLayoutManager);
    }

    public void listeners() {
        deliveryDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( new SharedPref(SubCartActivity.this).getToken().length() > 0  ) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("store" , store);
                    IntentClass.goToActivity(SubCartActivity.this, ShippingActivity.class, bundle);
                }
                else {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("fromCart" ,true );
                    bundle.putParcelable("store" , store);
                    IntentClass.goToActivity(SubCartActivity.this, LoginActivity.class, bundle);
                }

            }
        });
    }
    public void prepareView(){
        double productsPrice = Utility.getTotalPrice(productArrayList);
        delFeesTxv.setText(Utility.setPrice(this,store.getFees()));
        totalPriceTxv.setText(Utility.setPrice(this,productsPrice));
        totalCostTxv.setText(Utility.setPrice(this,productsPrice+ store.getFees()));
    }

    @Override
    protected void onResume() {
        prepareView();

        super.onResume();
    }
}
