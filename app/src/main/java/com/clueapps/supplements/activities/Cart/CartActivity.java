package com.clueapps.supplements.activities.Cart;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.clueapps.supplements.R;
import com.clueapps.supplements.adapters.CartItemsAdapter;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.realm.RealmController;

import java.util.ArrayList;

public class CartActivity extends AppCompatActivity {

    public static RecyclerView productsRecycler;
    public CartItemsAdapter cartItemsAdapter;
    public RelativeLayout progressRel,progressMoreDataRel;
    public LinearLayout noResultLayout;
    ImageView backImv , cartImv;
    public ArrayList<Product> productArrayList =new ArrayList<>();
    public static LinearLayout cartEmptyLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        initData();
        initView();
        listeners();
    }

    public void initData(){
        CustomHeader.setToolbar(this,getResources().getString(R.string.cart));

    }

    public void initView() {


        productsRecycler = (RecyclerView) findViewById(R.id.products_recycler);

        progressRel=(RelativeLayout)findViewById(R.id.progressRel);
        progressMoreDataRel =(RelativeLayout)findViewById(R.id.progressMoreDataRel);
        noResultLayout=(LinearLayout) findViewById(R.id.no_result_relative);
        backImv =(ImageView) findViewById(R.id.back_imv);
        cartImv =(ImageView) findViewById(R.id.cart_imv);
        cartEmptyLayout =(LinearLayout) findViewById(R.id.cart_empty_layout);

        productsRecycler.setAdapter(new CartItemsAdapter(this, productArrayList));

    }

    public void listeners() {

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        productsRecycler.setLayoutManager(linearLayoutManager);

    }
    public void prepareView(){

        productArrayList = RealmController.getStores();
        productsRecycler.setAdapter(new CartItemsAdapter(this, productArrayList));


        if (productArrayList.size() == 0) {
            cartEmptyLayout.setVisibility(View.VISIBLE);
            productsRecycler.setVisibility(View.GONE);
        }else{
            cartEmptyLayout.setVisibility(View.GONE );
            productsRecycler.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        prepareView();
    }
}
