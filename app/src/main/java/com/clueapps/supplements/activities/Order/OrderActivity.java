package com.clueapps.supplements.activities.Order;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.API;
import com.clueapps.supplements.API.OrderAPI;
import com.clueapps.supplements.Holders.OrderDetailsViewHolder;
import com.clueapps.supplements.Holders.OrderItemViewHolder;
import com.clueapps.supplements.Holders.ProductDetailsViewHolder;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Cart.CheckoutActivity;
import com.clueapps.supplements.adapters.ImagesAdapter;
import com.clueapps.supplements.adapters.OrderItemsAdapter;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Order;
import com.clueapps.supplements.models.Data.Product;
import com.clueapps.supplements.realm.RealmController;

import java.util.ArrayList;

public class OrderActivity extends AppCompatActivity {

    SharedPref sharedPref ;
    Button checkoutBtn ;
    Order order ;

    public ArrayList<Product> productArrayList =new ArrayList<>();
    Button deliveryDetailsBtn ;

    OrderDetailsViewHolder holder ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        initData();
        initView();
        prepareView();

    }

    public void initData() {
        Bundle bundle = getIntent().getBundleExtra("data");

        if ( bundle != null ){
            order = bundle.getParcelable("order");
        }
        sharedPref = new SharedPref(this);
        Utility.setCurrentActivity(this);
        CustomHeader.setToolbar(this,getString(R.string.order_details));

    }
    public void initView() {
        holder = new OrderDetailsViewHolder(this);

        deliveryDetailsBtn  =(Button) findViewById(R.id.deliveryDetails_btn);
        checkoutBtn =(Button) findViewById(R.id.checkout_btn);


        RelativeLayout fNameLayout = (RelativeLayout) findViewById(R.id.fName_layout);
        TextView fNameTxv = (TextView) fNameLayout.findViewById(R.id.title_txv);
        holder.fNameEdt = (EditText) fNameLayout.findViewById(R.id.input_edt);

        fNameTxv.setText(getString(R.string.first_name));
        holder.fNameEdt.setEnabled(false);


        RelativeLayout lNameLayout = (RelativeLayout) findViewById(R.id.lName_layout);
        TextView lNameTxv = (TextView) lNameLayout.findViewById(R.id.title_txv);
        holder.lNameEdt = (EditText) lNameLayout.findViewById(R.id.input_edt);

        lNameTxv.setText(getString(R.string.last_name));
        holder.lNameEdt.setEnabled(false);


        RelativeLayout mobileLayout = (RelativeLayout) findViewById(R.id.mobile_layout);
        TextView mobileTxv = (TextView) mobileLayout.findViewById(R.id.title_txv);
        holder.mobileEdt = (EditText) mobileLayout.findViewById(R.id.input_edt);

        mobileTxv.setText(getString(R.string.mobile));
        holder.mobileEdt.setEnabled(false);


        RelativeLayout addressLayout = (RelativeLayout) findViewById(R.id.address_layout);
        TextView addressTxv = (TextView) addressLayout.findViewById(R.id.title_txv);
        holder.addressEdt = (EditText) addressLayout.findViewById(R.id.input_edt);

        addressTxv.setText(getString(R.string.address));
        holder.addressEdt.setEnabled(false);

        RelativeLayout paymentLayout = (RelativeLayout) findViewById(R.id.payment_layout);
        TextView paymentTxv = (TextView) paymentLayout.findViewById(R.id.title_txv);
        holder.paymentEdt = (EditText) paymentLayout.findViewById(R.id.input_edt);

        paymentTxv.setText(getString(R.string.payment_method));
        holder.paymentEdt.setEnabled(false);

        RelativeLayout productsPriceLayout = (RelativeLayout) findViewById(R.id.products_price_layout);
        TextView  productsPriceTxv = (TextView) productsPriceLayout.findViewById(R.id.title_txv);
        holder.productsPriceEdt = (EditText) productsPriceLayout.findViewById(R.id.input_edt);

        productsPriceTxv.setText(getString(R.string.products_price_order));
        holder.productsPriceEdt.setEnabled(false);

        RelativeLayout delFeesLayout = (RelativeLayout) findViewById(R.id.del_fees_layout);
        TextView delFeesTxv = (TextView) delFeesLayout.findViewById(R.id.title_txv);
        holder.delFeesEdt = (EditText) delFeesLayout.findViewById(R.id.input_edt);

        delFeesTxv.setText(getString(R.string.del_fees_order));
        holder.delFeesEdt.setEnabled(false);


        RelativeLayout totalCostLayout = (RelativeLayout) findViewById(R.id.total_cost_layout);
        TextView totalCostTxv = (TextView) totalCostLayout.findViewById(R.id.title_txv);
        holder.totalCostEdt = (EditText) totalCostLayout.findViewById(R.id.input_edt);

        totalCostTxv.setText(getString(R.string.total_cost_order));
        holder.totalCostEdt.setEnabled(false);

    }
    public void prepareView(){

        OrderAPI.getOrder(this, order.getId(), holder);
    }
}
