package com.clueapps.supplements.activities.User;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.UserAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.helpers.Validations;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class EnterCodeActivity extends AppCompatActivity {

    EditText codeEdt;

    @BindView(R.id.code_error_txv)
    TextView errorCodeTxv;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_code);
        ButterKnife.bind(this);
        initData();
        initView();
    }


    public void initData() {
        Utility.setCurrentActivity(this);

        Bundle bundle = getIntent().getBundleExtra("data");
        if (bundle != null) {
            email = bundle.getString("email");
        }
    }

    public void initView() {

        LinearLayout codeLayout = (LinearLayout) findViewById(R.id.code_layout);
        TextInputLayout mobTxtLayout = (TextInputLayout) codeLayout.findViewById(R.id.enter_txtLayout);
        mobTxtLayout.setHint(getString(R.string.code));
        codeEdt = (EditText) mobTxtLayout.findViewById(R.id.input_edt);
        codeEdt.setInputType(InputType.TYPE_CLASS_NUMBER);
        ImageView mobileIcon = (ImageView) codeLayout.findViewById(R.id.icon_imv);
        mobileIcon.setImageDrawable(getResources().getDrawable(R.drawable.passwordicon));

    }

    /**
     * This method is used to validate code.
     * called when user click enter button.
     */
    @OnClick(R.id.enter_btn)
    public void validateInput() {
        boolean validated = true;

        String codeStr = codeEdt.getText().toString();

        validated = validated && Validations.isValidCode(codeStr);
        Validations.animateCodeView(codeStr, errorCodeTxv);

        if (validated) {
            UserAPI.enterCodeApi(this, Integer.parseInt(codeStr), email);
        }
    }
}


