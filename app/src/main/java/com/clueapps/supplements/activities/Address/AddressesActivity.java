package com.clueapps.supplements.activities.Address;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.clueapps.supplements.API.AddressAPI;
import com.clueapps.supplements.API.ProductAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Product.ProductsActivity;
import com.clueapps.supplements.adapters.AddressesAdapter;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Address;
import com.clueapps.supplements.models.Data.PaginatorData;

import java.util.ArrayList;

public class AddressesActivity extends AppCompatActivity {

    public RecyclerView addressesRecycler;
    public AddressesAdapter addressesAdapter;
    public ArrayList<Address> addressArrayList =new ArrayList<>();
    ImageView backImv ;
    Button addBtn ;
    public static boolean fromCart ;

    PaginatorData paginatorData  = new PaginatorData();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addresses);

        initData();
        initView();
        listeners();

    }
    public void initData(){
        if ( getIntent().getBundleExtra("data") != null ) {
            fromCart = getIntent().getBundleExtra("data").getBoolean("fromCart");
        }
    }


    public void initView() {

        CustomHeader.setToolbar(this , getString(R.string.my_addresses));

        paginatorData.refreshLayout=(SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        paginatorData.noResultLayout = (LinearLayout) findViewById(R.id.no_result_layout);
        paginatorData.recyclerView = (RecyclerView) findViewById(R.id.addresses_recycler);
        paginatorData.progressMoreDataRel =(RelativeLayout)findViewById(R.id.progressMoreDataRel);


        addressesRecycler = (RecyclerView) findViewById(R.id.addresses_recycler);

        addressesAdapter =new AddressesAdapter(this, addressArrayList);


        backImv =(ImageView) findViewById(R.id.back_imv);
        addBtn =(Button) findViewById(R.id.add_btn);

        addressesRecycler.setHasFixedSize(true);

        addressesRecycler.setAdapter(addressesAdapter);

    }

    public void listeners() {

        paginatorData.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetAddressesView();
                AddressAPI.getAddresses(AddressesActivity.this, addressesAdapter,addressArrayList,paginatorData);
            }
        });

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        addressesRecycler.setLayoutManager(linearLayoutManager);

        addressesRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (!paginatorData.loading && ! paginatorData.empty) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= Utility.take) {
                        AddressAPI.getAddresses(AddressesActivity.this, addressesAdapter,addressArrayList,paginatorData);
                    }
                }

            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(AddressesActivity.this,AddAddressActivity.class,null);
            }
        });
    }

    public void prepareView(){
        resetAddressesView();
        AddressAPI.getAddresses(this, addressesAdapter,addressArrayList,paginatorData);

    }


    public void resetAddressesView(){
        addressArrayList.clear();
        addressesAdapter.notifyDataSetChanged();
        paginatorData.page = 1 ;
        paginatorData.loading = false ;
        paginatorData.empty = false ;
    }
    @Override
    protected void onResume() {

        prepareView();
        super.onResume();
    }


}
