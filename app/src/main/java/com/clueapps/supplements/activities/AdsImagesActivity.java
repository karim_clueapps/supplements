package com.clueapps.supplements.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.ProductAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.adapters.FullAdImagesAdapter;
import com.clueapps.supplements.adapters.FullImagesAdapter;
import com.clueapps.supplements.adapters.ImagesAdapter;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Ad;
import com.clueapps.supplements.models.Data.ImagePath;
import com.clueapps.supplements.models.Data.Product;

import java.util.ArrayList;

public class AdsImagesActivity extends AppCompatActivity {

    FullAdImagesAdapter imagesAdapter;
    ViewPager imagesPager ;
    ArrayList<Ad> images = new ArrayList<>();
    int position = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads_images);

        initData();
        initView();
    }
    public void initData(){

        Bundle bundle = getIntent().getBundleExtra("data");

        if ( bundle != null ){
            images = bundle.getParcelableArrayList("images");
            position = bundle.getInt("position");
        }
    }
    public void initView(){

        imagesPager = (ViewPager) findViewById(R.id.images_pager) ;
        imagesAdapter = new FullAdImagesAdapter(this,images);
        imagesPager.setAdapter(imagesAdapter);
    }

}
