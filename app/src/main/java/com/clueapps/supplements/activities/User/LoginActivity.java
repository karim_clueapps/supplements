package com.clueapps.supplements.activities.User;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clueapps.supplements.API.UserAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.helpers.Validations;
import com.clueapps.supplements.models.Data.Store;
import com.clueapps.supplements.models.Data.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginActivity extends AppCompatActivity  {

    SharedPref sharedPref;
    String mobileStr, passwordStr;
    boolean fromCart ;
    Store store;
    EditText passwordEdt , mobileEdt;

    @BindView(R.id.signup_txv) TextView signupTxv ;
    @BindView(R.id.password_error_txv) TextView errorPassTxv ;
    @BindView(R.id.mobile_error_txv) TextView errorMobTxv ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initData();
        initView();
        listeners();
    }


    public void initData() {
        Bundle bundle = getIntent().getBundleExtra("data") ;

        if ( bundle != null ) {
            fromCart = bundle.getBoolean("fromCart");
            store = bundle.getParcelable("store");

        }

        sharedPref = new SharedPref(this);
        Utility.setCurrentActivity(this);

    }
    public void initView() {

        errorMobTxv =(TextView) findViewById(R.id.mobile_error_txv);
        errorPassTxv =(TextView)findViewById(R.id.password_error_txv) ;

        LinearLayout mobileLayout = (LinearLayout) findViewById(R.id.mobile_layout);
        TextInputLayout mobTxtLayout = (TextInputLayout) mobileLayout.findViewById(R.id.enter_txtLayout);
        mobTxtLayout.setHint(getString(R.string.mobile));
        mobileEdt = (EditText) mobTxtLayout.findViewById(R.id.input_edt);
        mobileEdt.setInputType(InputType.TYPE_CLASS_NUMBER);

        ImageView mobileIcon = (ImageView) mobileLayout.findViewById(R.id.icon_imv);
        mobileIcon.setImageDrawable(getResources().getDrawable(R.drawable.phonenumebericon));

        LinearLayout passLayout = (LinearLayout) findViewById(R.id.password_layout);
        TextInputLayout passTxtLayout = (TextInputLayout) passLayout.findViewById(R.id.enter_txtLayout);
        passTxtLayout.setHint(getString(R.string.password));
        passwordEdt = (EditText) passLayout.findViewById(R.id.input_edt);

        ImageView passIcon = (ImageView) passLayout.findViewById(R.id.icon_imv);
        passIcon.setImageDrawable(getResources().getDrawable(R.drawable.passwordicon));
    }
    public void listeners() {

        signupTxv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(LoginActivity.this,SignUpActivity.class,null);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 105:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    validateInput();
                } else {
                    Toast.makeText(this, "Please Enable Phone permission to Continue", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
        }
    }

    @OnClick(R.id.forgot_password_txv)
    public void showForgetDialog(){
        final Dialog dialog = new Dialog(LoginActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_forget_pass);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Button  send_btn = (  Button) dialog.findViewById(R.id.send_btn);
        final EditText  emailEdt = (  EditText) dialog.findViewById(R.id.email_edt);
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEdt.getText().toString();
                if ( Validations.isValidEmail(email) ) {
                    UserAPI.forgotPasswordApi(LoginActivity.this,email);
                    dialog.dismiss();
                }
                else
                    Dialogs.showToast(getString(R.string.valid_email) , LoginActivity.this);
            }
        });


    }

    private boolean phonePermission() {
        if (ContextCompat.checkSelfPermission(LoginActivity.this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(LoginActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    105);
            return true;
        }else
            return false;
    }
    /**
     * This method is used to validate login data.
     * called when user click signin button.
     */
    @OnClick(R.id.signin_btn)
    public void validateInput(){
        boolean validated =true;

        mobileStr = mobileEdt.getText().toString();
        passwordStr =passwordEdt.getText().toString().trim();

        validated = validated&&Validations.isValidMobile(mobileStr);
        Validations.animateMobileView(mobileStr,errorMobTxv);

        validated = validated&&Validations.isValidPassword(passwordStr);
        Validations.animatePasswordView(passwordStr,errorPassTxv);

        if(validated) {
            User user = new User();
            user.setMobile(mobileStr);
            user.setPassword(passwordStr);
            if (!phonePermission()) {
                UserAPI.loginApi(this,user,fromCart,store);
            }
        }
    }
}


