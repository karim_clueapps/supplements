package com.clueapps.supplements.activities.Product;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.ProductAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.adapters.FullImagesAdapter;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.ImagePath;
import com.clueapps.supplements.models.Data.Product;

import java.util.ArrayList;

public class ImagesActivity extends AppCompatActivity {

    RecyclerView imagesRecycler;
    FullImagesAdapter fullImagesAdapter;
    ArrayList<ImagePath> images = new ArrayList<>();
    ImageView favImv , closeImv , imageImv ;
    TextView titleTxv;
    Product product;
    RelativeLayout favLayout , closeLayout ;
    int position = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);

        initData();
        initView();
        listeners();
        prepareView();
    }
    public void initData(){
        Bundle bundle = getIntent().getBundleExtra("data");

        if ( bundle != null ){
            images = bundle.getParcelableArrayList("images");
            position = bundle.getInt("position");
            product = bundle.getParcelable("product");
        }
    }
    public void initView(){

        favLayout =(RelativeLayout)findViewById(R.id.fav_layout);
        closeLayout =(RelativeLayout)findViewById(R.id.close_layout);
        imagesRecycler = (RecyclerView) findViewById(R.id.images_recycler) ;
        closeImv =  (ImageView) findViewById(R.id.close_imv) ;
        imageImv =  (ImageView) findViewById(R.id.image_imv) ;
        titleTxv =  (TextView) findViewById(R.id.title_txv) ;
        favImv =  (ImageView) findViewById(R.id.fav_imv) ;

        favImv.setImageResource(product.isWishlisted() ? R.drawable.heartwhitefill   :R.drawable.hearticonwhitenofill );

    }
    public void listeners(){
        closeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        favLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if ( new SharedPref(ImagesActivity.this).getToken().length() > 0 ) {
                    if ( !product.isWishlisted() ) {
                    favImv.setImageResource(R.drawable.heartwhitefill);
                    ProductAPI.addToWishlist(ImagesActivity.this,product.getId());
                }
                else {
                    favImv.setImageResource(R.drawable.hearticonwhitenofill);
                    ProductAPI.removeFromWishlist(ImagesActivity.this,product.getId());
                }

                product.setWishlisted(!product.isWishlisted());
            }else
                Dialogs.showLoginDialog(ImagesActivity.this);
            }
        });
    }
    public void prepareView(){

        fullImagesAdapter = new FullImagesAdapter(this,images,imageImv);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        imagesRecycler.setLayoutManager(linearLayoutManager);
        imagesRecycler.setAdapter(fullImagesAdapter);

        titleTxv.setText(product.getName());

        if ( images != null && images.size() > position )
            Utility.downloadImage(this,images.get(position).getPath() , R.drawable.blackplaceholder, imageImv);

    }
}
