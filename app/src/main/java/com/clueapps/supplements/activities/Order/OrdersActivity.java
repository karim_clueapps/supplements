package com.clueapps.supplements.activities.Order;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.clueapps.supplements.API.AddressAPI;
import com.clueapps.supplements.API.OrderAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Address.AddressesActivity;
import com.clueapps.supplements.adapters.OrdersAdapter;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Order;
import com.clueapps.supplements.models.Data.PaginatorData;

import java.util.ArrayList;

public class OrdersActivity extends AppCompatActivity {
    public RecyclerView ordersRecycler ;
   
    public OrdersAdapter ordersAdapter; 
    public ArrayList<Order> orderArrayList =new ArrayList<>();

    PaginatorData paginatorData  = new PaginatorData();;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        initView();
        listeners();
        
    }
    public void initView() {

        CustomHeader.setToolbar(this,getString(R.string.my_orders));

        paginatorData.refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        paginatorData.noResultLayout = (LinearLayout) findViewById(R.id.no_result_layout);
        paginatorData.recyclerView = (RecyclerView) findViewById(R.id.orders_recycler);
        paginatorData. progressMoreDataRel =(RelativeLayout)findViewById(R.id.progressMoreDataRel);

        ordersRecycler = (RecyclerView) findViewById(R.id.orders_recycler);

        ordersAdapter =new OrdersAdapter(this, orderArrayList);


        ordersRecycler.setHasFixedSize(true);

        ordersRecycler.setAdapter(ordersAdapter);
    }
    public void listeners() {

        paginatorData.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetOrdersView();
                OrderAPI.getOrders(OrdersActivity.this, ordersAdapter,orderArrayList,paginatorData);
            }
        });

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        ordersRecycler.setLayoutManager(linearLayoutManager);

        ordersRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (!paginatorData.loading && ! paginatorData.empty) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= Utility.take) {
                        OrderAPI.getOrders(OrdersActivity.this, ordersAdapter,orderArrayList,paginatorData);
                    }
                }
            }
        });
    }

    public void prepareView(){
        resetOrdersView();
        OrderAPI.getOrders(this, ordersAdapter,orderArrayList,paginatorData);

    }
    public void resetOrdersView(){
        orderArrayList.clear();
        ordersAdapter.notifyDataSetChanged();
        paginatorData.page = 1 ;
        paginatorData.loading = false ;
        paginatorData.empty = false ;
    }


        @Override
    protected void onResume() {
        prepareView();
        super.onResume();
    }

}
