package com.clueapps.supplements.activities.User;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.UserAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.helpers.Validations;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ResetPasswordActivity extends AppCompatActivity  {

    String passwordStr , code;
    EditText passwordEdt;

    @BindView(R.id.password_error_txv) TextView errorPasswordTxv;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
        initData();
        initView();
    }


    public void initData() {
        code = getIntent().getBundleExtra("data").getString("code") ;
        Utility.setCurrentActivity(this);

    }
    public void initView() {

        LinearLayout passwordLayout = (LinearLayout) findViewById(R.id.password_layout);
        TextInputLayout mobTxtLayout = (TextInputLayout) passwordLayout.findViewById(R.id.enter_txtLayout);
        mobTxtLayout.setHint(getString(R.string.password));
        passwordEdt = (EditText) mobTxtLayout.findViewById(R.id.input_edt);

        ImageView passwordIcon = (ImageView) passwordLayout.findViewById(R.id.icon_imv);
        passwordIcon.setImageDrawable(getResources().getDrawable(R.drawable.passwordicon));

    }


    /**
     * This method is used to validate password.
     * called when user click enter button.
     */
    @OnClick(R.id.enter_btn)
    public void validateInput(){
        boolean validated =true;

        passwordStr = passwordEdt.getText().toString();

        validated = validated&&Validations.isValidPassword(passwordStr);
        Validations.animatePasswordView(passwordStr, errorPasswordTxv);

        if(validated) {
            UserAPI.resetPasswordApi(this,Integer.parseInt(code),passwordStr);
        }
    }
}


