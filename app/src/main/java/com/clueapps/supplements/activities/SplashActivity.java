package com.clueapps.supplements.activities;

import android.animation.Animator;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.airbnb.lottie.LottieAnimationView;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Product.StoresActivity;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;

import java.util.Locale;


public class SplashActivity extends AppCompatActivity {
    SharedPref sharedPref  ;

    Bundle notificationBundle;
    ImageView logoImv;
    LottieAnimationView animationView;

    int type = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkLang();
        setContentView(R.layout.activity_splash);
        init();
        notificationData();
    }
    public void checkLang(){
        sharedPref = new SharedPref(this);
        Utility.setCurrentActivity(this);

        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();

        if ( sharedPref.getLang().equals("ar") )
            conf.setLocale(new Locale("ar"));
        else
            conf.setLocale(new Locale("en"));

        res.updateConfiguration(conf, dm);
    }
    public void init(){

        sharedPref  = new SharedPref(getApplicationContext());

        logoImv = (ImageView) findViewById(R.id.logo_imv);

        Animation animationimage = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        logoImv.startAnimation(animationimage);

        animationimage.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                if (sharedPref.getBoolean("first_time", true))
                    IntentClass.goToActivity(SplashActivity.this , ChangeLanguageActivity.class , notificationBundle );
                else
                    IntentClass.goToActivity(SplashActivity.this , HomeActivity.class , notificationBundle );

                finish();

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        /*
        animationView = (LottieAnimationView) findViewById(R.id.animation_view);
        animationView.playAnimation();
        animationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (sharedPref.getBoolean("first_time", true))
                    IntentClass.goToActivity(SplashActivity.this , ChangeLanguageActivity.class , null );
                else
                    IntentClass.goToActivity(SplashActivity.this , HomeActivity.class , null );

                finish();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        */
    }

    public void notificationData()
    {

        notificationBundle = new Bundle();

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                if (key.equals("type"))
                    type = getIntent().getExtras().getInt(key);

            }
        }
        notificationBundle.putInt("type",type);
    }

}
