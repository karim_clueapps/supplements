package com.clueapps.supplements.activities.User;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.API;
import com.clueapps.supplements.API.UserAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.Dialogs;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.RefreshToken;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Validations;
import com.clueapps.supplements.models.requests.ChangePasswordRequest;
import com.clueapps.supplements.models.responses.MessageResponse;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChangePasswordActivity extends AppCompatActivity {

    private EditText oldPassEdt,newPassEdt,confirmPassEdt;
    private TextView errorOldPassTxv, errorNewPassTxv, errorNotMatchPassTxv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ButterKnife.bind(this);
        initView();
    }
    public void initView(){
            CustomHeader.setToolbar(ChangePasswordActivity.this,getResources().getString(R.string.change_pass));


            LinearLayout oldPass = (LinearLayout)findViewById(R.id.old_pass_layout);
            TextInputLayout oldPassLayout = (TextInputLayout)oldPass.findViewById(R.id.enter_txtLayout);
            oldPassLayout.setHint(getString(R.string.current_password));

            LinearLayout newPass = (LinearLayout)findViewById(R.id.new_pass_layout);
            TextInputLayout newPassLayout = (TextInputLayout)newPass.findViewById(R.id.enter_txtLayout);
            newPassLayout.setHint(getString(R.string.new_password));


            LinearLayout confirmPass = (LinearLayout)findViewById(R.id.confirm_pass_layout);
            TextInputLayout confirmPassLayout = (TextInputLayout)confirmPass.findViewById(R.id.enter_txtLayout);
            confirmPassLayout.setHint(getString(R.string.confirm_password));


            oldPassEdt=(EditText)oldPassLayout.findViewById(R.id.input_edt);
            newPassEdt=(EditText)newPassLayout.findViewById(R.id.input_edt);
            confirmPassEdt=(EditText)confirmPassLayout.findViewById(R.id.input_edt);


            errorNewPassTxv =(TextView)findViewById(R.id.newPass_error_txv);
            errorOldPassTxv =(TextView)findViewById(R.id.oldPass_error_txv);
            errorNotMatchPassTxv =(TextView)findViewById(R.id.pass_not_match_error_txv);


    }


    /**
     * This method is used to validate change password data.
     * called when user click save button.
     **/
    @OnClick(R.id.save_btn)
    public void validateInput(){
        boolean validated = true;

        String oldPassword =  oldPassEdt.getText().toString().trim();
        String newPassword = newPassEdt.getText().toString().trim();
        String confPassword = confirmPassEdt.getText().toString().trim();

        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setOldPassword(oldPassword);
        changePasswordRequest.setPassword(newPassword);

        validated = validated&&Validations.isValidPassword(oldPassword);
        Validations.animatePasswordView(oldPassword,errorOldPassTxv);

        validated = validated&&Validations.isValidPassword(newPassword);
        Validations.animatePasswordView(newPassword,errorNewPassTxv);

        validated = validated&& newPassword.equals(confPassword);
        Validations.animatePasswordNotMatchView(newPassword,confPassword,errorNotMatchPassTxv);

        if (validated) {
            changePasswordRequest.setEmail(new SharedPref(this).getUserEmail());
            UserAPI.changePassword(this,changePasswordRequest);
        }
    }

}
