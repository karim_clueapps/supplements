package com.clueapps.supplements.activities.Cart;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Address.AddressesActivity;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.SharedPref;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.helpers.Validations;
import com.clueapps.supplements.models.Data.Address;
import com.clueapps.supplements.models.Data.Store;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShippingActivity extends AppCompatActivity {

    SharedPref sharedPref;
    String type = "Visa" ;
    Store store;
     public static Address address = new Address() ;
    String addressStr , addressId;
    public static Activity activity;
    public static  TextView addressTxv;
    RelativeLayout addressLayout;
    @BindView(R.id.address_error_txv) TextView errorAddressTxv;
    @BindView(R.id.cash_imv) ImageView cashImv;
    @BindView(R.id.visa_imv) ImageView visaImv;
    @BindView(R.id.switch1) SwitchCompat switchCompat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping);
        ButterKnife.bind(this);
        initData();
        initView();
        listeners();
        prepareView();
    }


    public void initData() {

        activity = this;

        Bundle bundle = getIntent().getBundleExtra("data");

        if ( bundle != null ){
            store = bundle.getParcelable("store");
        }
        sharedPref = new SharedPref(this);
        Utility.setCurrentActivity(this);
        CustomHeader.setToolbar(this,getString(R.string.shipping));

        if ( sharedPref.getBoolean("save_purchase",false)) {
            type = sharedPref.getString("payment").length() != 0 ? sharedPref.getString("payment") : "Visa";
            addressStr = sharedPref.getString("address");
            addressId = sharedPref.getString("addressId");
        }else{
            addressId = address.getId();
            addressStr = Utility.setAddress(address);
            address.setFullAddress(addressStr);
        }

        if ( addressId != null && addressId.length() != 0 ) {
            address.setId(addressId);
            address.setFullAddress(addressStr);
        }
    }
    public void initView() {

        RelativeLayout fNameLayout = (RelativeLayout) findViewById(R.id.fName_layout);
        TextView fNameTxv = (TextView) fNameLayout.findViewById(R.id.title_txv);
        EditText fNameEdt = (EditText) fNameLayout.findViewById(R.id.input_edt);

        fNameTxv.setText(getString(R.string.first_name));
        fNameEdt.setText(sharedPref.getFName());
        fNameEdt.setEnabled(false);


        RelativeLayout lNameLayout = (RelativeLayout) findViewById(R.id.lName_layout);
        TextView lNameTxv = (TextView) lNameLayout.findViewById(R.id.title_txv);
        EditText lNameEdt = (EditText) lNameLayout.findViewById(R.id.input_edt);

        lNameTxv.setText(getString(R.string.last_name));
        lNameEdt.setText(sharedPref.getLName());
        lNameEdt.setEnabled(false);


        RelativeLayout mobileLayout = (RelativeLayout) findViewById(R.id.mobile_layout);
        TextView mobileTxv = (TextView) mobileLayout.findViewById(R.id.title_txv);
        EditText mobileEdt = (EditText) mobileLayout.findViewById(R.id.input_edt);

        mobileTxv.setText(getString(R.string.mobile));
        mobileEdt.setText(sharedPref.getUserMob());
        mobileEdt.setEnabled(false);


        addressLayout = (RelativeLayout) findViewById(R.id.address_layout);
        addressTxv = (TextView) findViewById(R.id.address_txv);
        addressTxv.setTextColor(getResources().getColor(R.color.black));

    }

    public void listeners() {

        addressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("fromCart" , true);
                IntentClass.goToActivity(ShippingActivity.this , AddressesActivity.class , bundle );
            }
        });

        cashImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "Cash" ;
                cashImv.setImageResource(R.drawable.cashondeliveryselected);
                visaImv.setImageResource(R.drawable.visanotselected);
            }
        });

        visaImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "Visa" ;
                cashImv.setImageResource(R.drawable.cashondeliverynotselected);
                visaImv.setImageResource(R.drawable.visaselected);
            }
        });

    }

    public void prepareView(){
        if ( type.equals("Visa") ) {
            cashImv.setImageResource(R.drawable.cashondeliverynotselected);
            visaImv.setImageResource(R.drawable.visaselected);
        }
        else {
            cashImv.setImageResource(R.drawable.cashondeliveryselected);
            visaImv.setImageResource(R.drawable.visanotselected);
        }

        switchCompat.setChecked(sharedPref.getBoolean("save_purchase",false));

    }
    /**
     * This method is used to validate if address selected.
     * called when user click confirm button.
     **/
    @OnClick(R.id.confirm_btn)
    public void validateInput(){

        boolean validated = true;


        if ( address.getId() == null || address.getId().length() == 0 ) {
            Validations.animateStrView("", errorAddressTxv);
            validated = false ;
        }else{
            errorAddressTxv.setVisibility(View.GONE);
        }


        if(validated) {

            Bundle bundle = new Bundle();
            bundle.putParcelable("store", store);
            bundle.putString("payment" , type  );
            bundle.putString("address" ,  address.getFullAddress());
            bundle.putString("addressId" , address.getId());

            if ( switchCompat.isChecked()) {
                sharedPref.putString("payment" , type);
                sharedPref.putString("address" , address.getFullAddress());
                sharedPref.putString("addressId" , address.getId());
            }else{
                sharedPref.putString("payment" , "");
                sharedPref.putString("address" , "");
                sharedPref.putString("addressId" , "");
            }
            sharedPref.putBoolean("save_purchase",switchCompat.isChecked());
            IntentClass.goToActivity(ShippingActivity.this,CheckoutActivity.class,bundle);

        }
    }

    @Override
    protected void onResume() {

        if ( addressId != null && addressId.length() > 0 ) {
            addressStr = address.getFullAddress();
            addressTxv.setText(addressStr);
        }

        super.onResume();
    }
}
