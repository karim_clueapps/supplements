package com.clueapps.supplements.activities.Product;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.clueapps.supplements.API.OrderAPI;
import com.clueapps.supplements.API.ProductAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Order.OrdersActivity;
import com.clueapps.supplements.adapters.ProductsAdapter;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.PaginatorData;
import com.clueapps.supplements.models.Data.Product;

import java.util.ArrayList;

public class WishListActivity extends AppCompatActivity {

    public RecyclerView productsRecycler;
    public ProductsAdapter productsAdapter;
    public RelativeLayout progressRel,progressMoreDataRel;
    public LinearLayout noResultLayout;
    public ArrayList<Product> productArrayList =new ArrayList<>();
    ImageView backImv , cartImv;

    PaginatorData paginatorData = new PaginatorData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);

        initData();
        initView();
        listeners();

    }
    public void initData(){

    }
    public void initView() {

        paginatorData.refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        paginatorData.noResultLayout = (LinearLayout) findViewById(R.id.no_result_layout);
        paginatorData.recyclerView = (RecyclerView) findViewById(R.id.products_recycler);

        CustomHeader.setToolbarCart(this,getString(R.string.wishlist));

        productsRecycler = (RecyclerView) findViewById(R.id.products_recycler);

        productsAdapter =new ProductsAdapter(this, productArrayList,true);
        progressRel=(RelativeLayout)findViewById(R.id.progressRel);
        progressMoreDataRel =(RelativeLayout)findViewById(R.id.progressMoreDataRel);
        noResultLayout=(LinearLayout) findViewById(R.id.no_result_relative);
        backImv =(ImageView) findViewById(R.id.back_imv);
        cartImv =(ImageView) findViewById(R.id.cart_imv);

        productsRecycler.setHasFixedSize(true);

        productsRecycler.setAdapter(productsAdapter);

    }

    public void listeners() {
        paginatorData.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetProductsView();
                ProductAPI.getWishlist(WishListActivity.this, productsAdapter,productArrayList,paginatorData);
            }
        });

        final GridLayoutManager gridLayoutManager =  new GridLayoutManager(this,2);
        productsRecycler.setLayoutManager(gridLayoutManager);

        productsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = gridLayoutManager.getChildCount();
                int totalItemCount = gridLayoutManager.getItemCount();
                int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();

                if (!paginatorData.loading && ! paginatorData.empty) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= Utility.take) {
                         ProductAPI.getWishlist(WishListActivity.this, productsAdapter,productArrayList,paginatorData);
                    }
                }

            }
        });

    }
    public void resetProductsView() {
        productArrayList.clear();
        productsAdapter.notifyDataSetChanged();
        paginatorData.page = 1 ;
        paginatorData.loading = false ;
        paginatorData.empty = false ;
    }
    public void prepareView(){
        CustomHeader.changeCartNum(this);

        resetProductsView();
        ProductAPI.getWishlist(this, productsAdapter,productArrayList,paginatorData);

    }


    @Override
    protected void onResume() {
        CustomHeader.setToolbarCart(this,getString(R.string.wishlist));

        prepareView();
        super.onResume();
    }

}
