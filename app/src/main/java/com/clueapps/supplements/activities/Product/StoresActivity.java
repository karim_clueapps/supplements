package com.clueapps.supplements.activities.Product;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clueapps.supplements.API.ProductAPI;
import com.clueapps.supplements.R;
import com.clueapps.supplements.activities.Cart.CartActivity;
import com.clueapps.supplements.adapters.StoreAdapter;
import com.clueapps.supplements.helpers.CustomHeader;
import com.clueapps.supplements.helpers.IntentClass;
import com.clueapps.supplements.helpers.Utility;
import com.clueapps.supplements.models.Data.Category;
import com.clueapps.supplements.models.Data.PaginatorData;
import com.clueapps.supplements.models.Data.Store;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoresActivity extends AppCompatActivity {

    public RecyclerView storeRecycler;
    public StoreAdapter storeAdapter;
    public RelativeLayout progressRel,progressMoreDataRel;
    public LinearLayout noResultLayout;
    public ArrayList<Store> storeArrayList =new ArrayList<>();
    ImageView backImv , cartImv;
    ImageView imageImv ;
    Category category;
    TextView nameTxv ;

    PaginatorData paginatorData  = new PaginatorData();
    @BindView(R.id.collapsingToolbar) CollapsingToolbarLayout collapsingToolbar ;
    @BindView(R.id.searchIcon_imv) ImageView searchIconImv ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        ButterKnife.bind(this);

        iniData();
        initView();
        listeners();
    }

    public void iniData(){

        category = getIntent().getBundleExtra("data").getParcelable("category");

        storeArrayList.add(null);
    }
    public void initView() {

        collapsingToolbar.requestLayout();
        paginatorData.refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);

        storeRecycler = (RecyclerView) findViewById(R.id.store_recycler);
        nameTxv= (TextView) findViewById(R.id.name_txv);
        imageImv = (ImageView) findViewById(R.id.image_imv);
        storeAdapter =new StoreAdapter(this, storeArrayList,category);
        progressRel=(RelativeLayout)findViewById(R.id.progressRel);
        progressMoreDataRel =(RelativeLayout)findViewById(R.id.progressMoreDataRel);
        noResultLayout=(LinearLayout) findViewById(R.id.no_result_relative);
        backImv =(ImageView) findViewById(R.id.back_imv);
        cartImv =(ImageView) findViewById(R.id.cart_imv);

        storeRecycler.setHasFixedSize(true);
        storeRecycler.setAdapter(storeAdapter);


    }
    public void listeners() {


        searchIconImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(StoresActivity.this,SearchActivity.class,null);
            }
        });

        paginatorData.refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               resetStoresView();
                ProductAPI.getStores(StoresActivity.this, storeAdapter,category.getId(),storeArrayList,paginatorData);
            }
        });


        final GridLayoutManager gridLayoutManager =  new GridLayoutManager(this,3);
        storeRecycler.setLayoutManager(gridLayoutManager);

        storeRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = gridLayoutManager.getChildCount();
                int totalItemCount = gridLayoutManager.getItemCount();
                int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();

                if (!paginatorData.loading && ! paginatorData.empty) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= Utility.take) {
                        ProductAPI.getStores(StoresActivity.this, storeAdapter,category.getId(),storeArrayList,paginatorData);
                    }
                }

            }
        });

        backImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cartImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentClass.goToActivity(StoresActivity.this, CartActivity.class,null);
            }
        });
    }

    public void resetStoresView(){
        storeArrayList.clear();
        storeArrayList.add(null);
        storeAdapter.notifyDataSetChanged();
        paginatorData.page = 1 ;
        paginatorData.loading = false ;
        paginatorData.empty = false ;
    }
    public void prepareView(){
        CustomHeader.changeCartNum(this);


        collapsingToolbar.setTitle(category.getName());
        Utility.downloadImage(this,category.getImage(),R.drawable.blackplaceholder,imageImv);

        resetStoresView();
        ProductAPI.getStores(StoresActivity.this, storeAdapter,category.getId(),storeArrayList,paginatorData);

    }


    @Override
    protected void onResume() {
        prepareView();
        super.onResume();
    }
}
